import { useEffect, useRef, useState } from "react";
import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { Provider } from "react-redux";
// import "react-native-gesture-handler";
import { BottomSheetModalProvider } from "@gorhom/bottom-sheet";
import { PersistGate } from "redux-persist/integration/react";
import { LinearGradient } from "expo-linear-gradient";
import * as Device from "expo-device";
import * as Notifications from "expo-notifications";
import Constants from "expo-constants";
import { GestureHandlerRootView } from "react-native-gesture-handler";

// Router path
import Router from "./app/router";
import { Store, Persistor } from "./app/redux";
import { CHECK_CONNECTION, COLORS } from "./app/services";
import { CustomText } from "./app/components";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export default function App() {
  const [connection, setConnection] = useState(false);
  const [expoPushToken, setExpoPushToken] = useState("");
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();

  const checkConn = async () => {
    const connections = await CHECK_CONNECTION();
    console.log("jaringan? ", connections);
    if (!connections) {
      console.log("keluar");
      setConnection(true);
    }
  };

  useEffect(() => {
    checkConn();

    registerForPushNotificationsAsync().then((token) =>
      setExpoPushToken(token)
    );

    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        setNotification(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        // console.log(response);
      });

    async function fetchMyAPI() {
      await schedulePushNotificationClockIn();
    }

    fetchMyAPI();
    // await schedulePushNotificationClockOut();

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  if (connection) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <CustomText>There is no Connection in your device!</CustomText>
        <TouchableOpacity
          style={{ backgroundColor: "red", marginTop: 15, borderRadius: 5 }}
          onPress={() => checkConn()}
        >
          <LinearGradient
            colors={COLORS.gradient}
            style={{ padding: 10, borderRadius: 5 }}
          >
            <CustomText style={{ color: "white" }} regular>
              refresh here!
            </CustomText>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    );
  } else {
    return (
      <SafeAreaProvider>
        <GestureHandlerRootView style={{ flex: 1 }}>
          <BottomSheetModalProvider>
            <Provider store={Store}>
              <PersistGate
                loading={<CustomText>Loading...</CustomText>}
                persistor={Persistor}
              >
                <NavigationContainer>
                  <StatusBar
                    barStyle="light-content"
                    hidden={false}
                    translucent
                    backgroundColor="transparent"
                  />
                  <Router />
                </NavigationContainer>
              </PersistGate>
            </Provider>
          </BottomSheetModalProvider>
        </GestureHandlerRootView>
      </SafeAreaProvider>
    );
  }
}

async function schedulePushNotificationClockIn() {
  await Notifications.scheduleNotificationAsync({
    content: {
      title: "Absen pagi! 📬",
      body: "Sudah sampai kantor belum? Jangan lupa absen pagi ya!",
      data: { data: "goes here" },
      sound: "notification_alarm.wav",
    },
    trigger: {
      hour: 7,
      minute: 20,
      // seconds: 0,
      repeats: true,
      channelId: "default",
    },
  });
}

async function registerForPushNotificationsAsync() {
  let token;

  if (Platform.OS === "android") {
    Notifications.setNotificationChannelAsync("default", {
      name: "default",
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: "#FF231F7C",
      sound: "notification_alarm.wav",
    });
  }

  if (Device.isDevice) {
    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus !== "granted") {
      const { status } = await Notifications.requestPermissionsAsync({
        ios: {
          allowAlert: true,
          allowBadge: true,
          allowSound: true,
          allowAnnouncements: true,
        },
      });
      finalStatus = status;
    }
    if (finalStatus !== "granted") {
      alert("Failed to get push token for push notification!");
      return;
    }
    token = await Notifications.getExpoPushTokenAsync({
      projectId: Constants.expoConfig.extra.eas.projectId,
    });
    // console.log(token);
  } else {
    alert("Must use physical device for Push Notifications");
  }

  return token;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
