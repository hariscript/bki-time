import LogoColor from "./logo-color.png";
import LogoWhite from "./logo-white.png";
import BG from "./bg.jpg";
import LogoApp from "./icon-app.png";
import Placeholder from "./placeholder.jpeg";
import Empty from "./empty.jpg";

export { LogoColor, LogoWhite, BG, LogoApp, Placeholder, Empty };
