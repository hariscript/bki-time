import Absence from "./absence.png";
import ClockIn from "./clockIn.png";
import BT_Absence from "./BT_Absence.png";
import BT_After_Break from "./BT_After_Break.png";
import BT_Break from "./BT_Break.png";
import BT_Clockin from "./BT_Clockin.png";
import BT_Clockout from "./BT_Clockout.png";
import BT_E_Office from "./BT_E-Office.png";
import BT_Helpdesk from "./BT_Helpdesk.png";
import BT_Leave from "./BT_Leave.png";
import BT_Location_Management from "./BT_Location_management.png";
import BT_Management from "./BT_Management.png";
import BT_Overtime_Management from "./BT_Overtime_management.png";
import BT_Overtime from "./BT_Overtime.png";
import BT_Permission from "./BT_Permission.png";
import BT_Personal from "./BT_Personal.png";
import BT_Report_Absence from "./BT_Report_Absence.png";
import BT_Salary from "./BT_Salary.png";
import BT_Absence_Test from "./BT_Absence_Test.png";
import User from "./user.png";
import Pagi from "./pagi.png";
import Siang from "./siang.png";
import Sore from "./sore.png";

export {
  Absence,
  ClockIn,
  BT_Absence,
  BT_After_Break,
  BT_Break,
  BT_Clockin,
  BT_Clockout,
  BT_E_Office,
  BT_Helpdesk,
  BT_Leave,
  BT_Location_Management,
  BT_Management,
  BT_Overtime,
  BT_Overtime_Management,
  BT_Permission,
  BT_Personal,
  BT_Report_Absence,
  BT_Salary,
  BT_Absence_Test,
  User,
  Pagi,
  Siang,
  Sore,
};
