import apisauce from "apisauce";

const create = (baseuri) => {
  // const dispatch = useDispatch();

  const api = apisauce.create({
    baseURL:
      baseuri == "auth"
        ? "https://auth.bki.co.id/v1/"
        : baseuri == "hrms"
        ? "https://hrms.bki.co.id/web/api/v2/client/"
        : "https://api-bkitime.bki.co.id/v1/",
    headers: {
      "Cache-Control": "no-cache",
      "Content-Type": "application/json",
    },
    timeout: 30000,
  });

  // inisialisasi
  const GET = api.get;
  const POST = api.post;
  const DELETE = api.delete;
  const PUT = api.put;

  // end point
  // Auth (Login & Logout)
  const login = (data) => POST("login", data);
  const refresh = (data) => POST("refresh", data);
  const longlat = (data) =>
    GET(
      `Get_Division?token=WP4PFLLTUWS2430R7D6T02Q86JX3IV4VO6168SCBM8JF1J6Y3E&uid=RS1PRkZJQ0U&codeDivisi=${data.code}`
    );

  return {
    login,
    refresh,
    POST,
    DELETE,
    GET,
    PUT,
    longlat,
  };
};

export default { create };
