import { CREATE_UPDATE_TOKEN, GET_TOKEN } from "./sqlite";
import { CALL_API } from "./apihandler";
import { CHECK_CONNECTION } from "./network";
import { GET_SENTENCE, GET_UNIQUE_ID } from "./general";

export {
  CREATE_UPDATE_TOKEN,
  GET_TOKEN,
  CALL_API,
  CHECK_CONNECTION,
  GET_SENTENCE,
  GET_UNIQUE_ID
};
