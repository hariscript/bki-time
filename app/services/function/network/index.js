import NetInfo from '@react-native-community/netinfo';

const CHECK_CONNECTION = async () => {
  const jaringan = await NetInfo.fetch().then((state) => {
    console.log("Connection type", state.type);
    console.log("Is connected?", state.isConnected);
    return state.isConnected;
  });
  return jaringan;
};

export { CHECK_CONNECTION };
