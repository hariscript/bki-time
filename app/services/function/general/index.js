import * as SecureStore from "expo-secure-store";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";

const GET_UNIQUE_ID = async () => {
  let uuid = uuidv4();
  let fetchUUID = await SecureStore.getItemAsync("secure_deviceid");
  console.log(fetchUUID ? "kamu sudah pernah absen" : "user baru");
  //if user has already signed up prior
  if (fetchUUID) {
    uuid = fetchUUID;
  }
  await SecureStore.setItemAsync("secure_deviceid", uuid);
  return uuid;
};

const GET_SENTENCE = (data) => {
  const random = Math.floor(Math.random() * data.length);
  const get_sentence = data[random];

  return get_sentence;
};

export { GET_UNIQUE_ID, GET_SENTENCE };
