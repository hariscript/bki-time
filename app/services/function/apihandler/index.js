import base64 from "react-native-base64";
import sha512 from "js-sha512";
import * as SecureStore from "expo-secure-store";
import "react-native-get-random-values";
import { v4 as uuidv4 } from "uuid";
import AsyncStorage from "@react-native-async-storage/async-storage";

// BUILD IN CODE
import Api from "../../api";
import { CREATE_UPDATE_TOKEN, GET_TOKEN } from "../sqlite";

const CALL_API = async (uri, payload, type, nup, navigation) => {
  let response;

  const keys = await GET_TOKEN(nup);
  // const key_login = await AsyncStorage.getItem("key_login");

  try {
    if (type === "POST") {
      const changePayload = {
        ...payload,
        key: keys,
      };

      response = await Api.create("Api").POST(uri, changePayload, {
        headers: {
          "Content-Type": "application/json",
          // "Access-Control-Request-Headers": "*"
        },
      });
    } else if (type === "UPLOAD") {
      payload.append("key", keys);
      response = await Api.create("Api").POST(uri, payload, {
        headers: {
          "Content-Type": "multipart/form-data",
          // "Access-Control-Request-Headers": "*"
        },
      });
    } else {
      response = await Api.create("Api").POST(
        uri,
        {
          ...payload,
          key: keys,
        },
        {
          headers: {
            "Content-Type": "application/json",
            // "Access-Control-Request-Headers": "*"
          },
        }
      );
    }

    if (response.status === 200 || response.status === "200") {
      console.log("Berhasil get API");
      const finalResponse = response.data;

      return { finalResponse, keys };
    } else if (response.data.status === "refresh") {
      const test = await refreshTokenApi_v2(
        uri,
        payload,
        type,
        nup,
        navigation
      ).then(async (res) => {
        let refreshResponse;

        if (type === "POST") {
          const changePayload = {
            ...payload,
            key: res,
          };

          refreshResponse = await Api.create("Api").POST(uri, changePayload, {
            headers: {
              "Content-Type": "application/json",
              // "Access-Control-Request-Headers": "*"
            },
          });
        } else if (type === "UPLOAD") {
          payload.append("key", res);
          refreshResponse = await Api.create("Api").POST(uri, payload, {
            headers: {
              "Content-Type": "multipart/form-data",
              // "Access-Control-Request-Headers": "*"
            },
          });
        } else {
          refreshResponse = await Api.create("Api").POST(
            uri,
            {
              ...payload,
              key: res,
            },
            {
              headers: {
                "Content-Type": "application/json",
                // "Access-Control-Request-Headers": "*"
              },
            }
          );
        }

        if (
          refreshResponse.status === 200 ||
          refreshResponse.status === "200"
        ) {
          console.log("Berhasil get API");
          const finalResponse = refreshResponse.data;

          return { finalResponse, keys: res };
        }
      });

      return test;
    } else {
      // const finalResponse = {
      //   data: [],
      // };

      // if (response.data.message) {
      //   alert(response.data.message);
      //   console.log(response.data.message);
      // } else {
      //   alert(response.originalError);
      //   console.log(response.originalError);
      // }
      // return { finalResponse, keys };

      navigation.replace("Login");
    }
  } catch (error) {
    navigation.replace("Login");
    console.error("An error occurred:", error);
  }
};

const refreshTokenApi_v2 = async (uri, payload, type, nup, navigation) => {
  const keys = await GET_TOKEN(nup);

  var base64_key = base64.encode(keys);
  var sha512_key = sha512(base64_key);

  var payloadToken = {
    key: keys,
    refresh: sha512_key,
  };

  const response = await Api.create("auth").refresh(payloadToken);
  if (response.data.status === "success") {
    await CREATE_UPDATE_TOKEN(nup, response.data.key);
    // await AsyncStorage.setItem("key_login", response.data.key);

    return response.data.key;
  } else if (response.data.status === "invalid") {
    // const finalResponse = "error";
    // return { finalResponse };

    navigation.replace("Login");
  } else {
    // const finalResponse = "error";
    // return { finalResponse };

    navigation.replace("Login");
  }
};

export { CALL_API };
