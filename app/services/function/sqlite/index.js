import * as SQLite from "expo-sqlite";

function openDatabase() {
  if (Platform.OS === "web") {
    return {
      transaction: () => {
        return {
          executeSql: () => {}
        };
      }
    };
  }

  const db = SQLite.openDatabase("keys.db");
  return db;
}

let db = openDatabase();

const CREATE_UPDATE_TOKEN = async (nup, key) => {
  try {
    const result = await new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          `SELECT * FROM token WHERE nup = ?`,
          [nup],
          (tx, results) => {
            const rows = results.rows;
            if (rows.length > 0) {
              tx.executeSql(
                `UPDATE token SET keys = ? WHERE nup = ?`,
                [key, nup],
                () => resolve({ updated: true }),
                (error) => reject(error)
              );
            } else {
              tx.executeSql(
                `INSERT INTO token (nup, keys) VALUES (?, ?)`,
                [nup, key],
                () => resolve({ inserted: true }),
                (error) => reject(error)
              );
            }
          },
          (error) => {
            reject(error);
          }
        );
      });
    });
    return result;
  } catch (error) {
    console.log(`Error checking/updating token: ${JSON.stringify(error)}`);
    return null;
  }
};

const GET_TOKEN = async (nup) => {
  try {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        tx.executeSql(
          "SELECT keys FROM token WHERE nup = ?",
          [nup],
          (_, result) => {
            const keys = result.rows._array.map((row) => row.keys);
            resolve(keys[0]);
          },
          (_, error) => reject(`Error: ${error}`)
        );
      });
    });
  } catch (error) {
    console.log(`Error retrieving key from table token: ${error}`);
    return null;
  }
};

export { CREATE_UPDATE_TOKEN, GET_TOKEN };
