const COLORS = {
  gradient: ["#007FFF", "#026AD2", "#055DB3"],
  boldText: "#707070",
  placeholder: "#D7D7D7",
  greenCurrency: "#1AA87C",
  blue: "#007FFF",
  lightBlue: "#1EACFF",

  success: "#1AA87C",
  waring: "#ECB143",
  danger: "#BF1A1A",
};

export { COLORS };
