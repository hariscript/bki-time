import {
  BT_Absence,
  BT_After_Break,
  BT_Break,
  BT_Clockin,
  BT_Clockout,
  BT_E_Office,
  BT_Helpdesk,
  BT_Leave,
  BT_Location_Management,
  BT_Management,
  BT_Overtime,
  BT_Overtime_Management,
  BT_Permission,
  BT_Personal,
  BT_Report_Absence,
  BT_Salary
} from "../../../assets";
import { BT_Absence_Test } from "../../../assets/icons";

const MENU_HOME = [
  {
    nama: "Absence",
    navigation: "Absence",
    gambar: BT_Absence
  },
  {
    nama: "Personal",
    navigation: "Personal",
    gambar: BT_Personal
  },
  {
    nama: "Eoffice",
    navigation: "Eoffice",
    gambar: BT_E_Office
  },
  {
    nama: "Hepldesk",
    navigation: "Helpdesk",
    gambar: BT_Helpdesk
  },
  {
    nama: "Management",
    navigation: "Management",
    gambar: BT_Management
  }
];

const MENU_ABSENCE = [
  {
    nama: "Clock-In",
    navigation: "Clock In",
    gambar: BT_Clockin
  },
  {
    nama: "Break",
    navigation: "Break",
    gambar: BT_Break
  },
  {
    nama: "After Break",
    navigation: "After Break",
    gambar: BT_After_Break
  },
  {
    nama: "Clock-Out",
    navigation: "Clock Out",
    gambar: BT_Clockout
  },
  {
    nama: "Permission",
    navigation: "List Permission",
    gambar: BT_Permission
  },
  {
    nama: "Report Absence",
    navigation: "Report",
    gambar: BT_Report_Absence
  }
];

const MENU_MANAGEMENT = [
  {
    nama: "Absensi",
    navigation: "ManageAbsence",
    gambar: BT_Absence
  },
  {
    nama: "Lembur",
    navigation: "Lembur",
    gambar: BT_Overtime_Management
  },
  {
    nama: "Approval",
    navigation: "Approval",
    gambar: BT_Location_Management
  }
];

const MENU_PERSONAL = [
  {
    nama: "Gaji",
    navigation: "Salary",
    gambar: BT_Salary
  },
  {
    nama: "Cuti",
    navigation: "Cuti",
    gambar: BT_Leave
  },
  {
    nama: "Lembur",
    navigation: "Lembur",
    gambar: BT_Overtime
  }
];

const MENU_EOFFICE = [
  {
    nama: "Inbox",
    navigation: "ListEoffice",
    gambar: BT_Salary,
    count: 0
  },
  {
    nama: "Tembusan",
    navigation: "Tembusan",
    gambar: BT_Salary,
    count: 0
  },
  {
    nama: "Approval",
    navigation: "Approval E-Office",
    gambar: BT_Salary,
    count: 0
  },
  {
    nama: "Sent Item",
    navigation: "Sent Item",
    gambar: BT_Salary,
    count: 0
  },
  {
    nama: "Disposisi",
    navigation: "Disposisi",
    gambar: BT_Salary,
    count: 0
  },
  {
    nama: "Disposisi Keluar",
    navigation: "Disposisi Keluar",
    gambar: BT_Salary,
    count: 0
  }
];

const MONTHS = [
  { id: 1, name: "Januari" },
  { id: 2, name: "Februari" },
  { id: 3, name: "Maret" },
  { id: 4, name: "April" },
  { id: 5, name: "Mei" },
  { id: 6, name: "Juni" },
  { id: 7, name: "Juli" },
  { id: 8, name: "Agustus" },
  { id: 9, name: "September" },
  { id: 10, name: "Oktober" },
  { id: 11, name: "November" },
  { id: 12, name: "Desember" }
];

const JENIS_IZIN = [
  "Izin Terlambat",
  "Izin Tidak Masuk",
  "Izin Pulang Cepat",
  "Izin Sakit"
];

const ABSENCE_TYPE = ["Clock In", "Break", "After Break", "Clock Out"];

const LOKASI_PELAKSANAAN = [
  {
    id: 1,
    name: "Dalam Kota"
  },
  {
    id: 2,
    name: "Luar Kota"
  }
];

const DUMMY_FLATLIST = [
  {
    id: 1,
    nama: "Haha"
  },
  {
    id: 2,
    nama: "Hihi"
  },
  {
    id: 3,
    nama: "Hehe"
  },
  {
    id: 4,
    nama: "Huhu"
  },
  {
    id: 5,
    nama: "Hoho"
  }
];

const LAST_THREE_YEARS = () => {
  const currentYear = new Date().getFullYear();
  const threeYearsAgo = currentYear - 3;
  let years = [];

  for (let i = currentYear; i > threeYearsAgo; i--) {
    const temp = {
      id: i,
      name: i
    };
    years.push(temp);
  }

  return years;
};

const OUTSIDE_RADIUS_SENTENCES = [
  "Eh, kamu masih di luar kantor, nih! Segera menuju ke kantor untuk memulai hari produktifmu. Absen akan menunggu!",
  "Sepertinya kamu sudah hampir sampai! Tetapi untuk absen, kita perlu kamu di dalam kantor. Semangat menuju kantor!",
  "Wow, sepertinya kamu punya petualangan pagi ini! Absen belum bisa dimulai karena kamu masih di luar radius kantor. Lanjutkan perjalanan!",
  "Hore, kamu dalam perjalanan ke kantor! Saat kamu sampai, jangan lupa absen ya. Semangat!",
  "Ayo, segera menuju kantor! Absenmu menunggu di sana. 😊",
  "Kamu masih di luar jangkauan kantor, jadi belum bisa absen. Cepat ke kantor untuk memulai hari!",
  "Kamu hampir mencapai kantor! Begitu kamu masuk, jangan lupa untuk absen. Selamat datang!"
];

const SUCCESS_CLOCK_IN_SENTENCES = [
  "Hore! Kamu sudah berhasil melakukan absen masuk kantor. Selamat datang, hari ini akan menjadi luar biasa!",
  "Absen masuk kantor sukses! Kamu adalah poin terang yang memulai hari ini dengan baik.",
  "Absen kamu sudah tercatat dengan baik. Sekarang, mari mulai produktif di kantor!",
  "Great job! Kamu sudah ada di kantor dan absen berhasil. Mari buat hari ini menjadi yang terbaik!",
  "Selamat, kamu sudah masuk ke kantor dan berhasil absen. Waktunya beraksi dan mencapai tujuanmu!",
  "Yeay, kamu berhasil masuk kantor! Sekarang, mari bersama-sama menjalani hari yang penuh prestasi.",
  "Sukses! Kamu sudah absen masuk kantor dengan baik. Ayo, mari bersama-sama menjalani hari yang produktif!"
];

const SUCCESS_BREAK_SENTENCES = [
  "Selamat! Kamu telah berhasil melakukan absen untuk istirahatmu. Waktunya nikmati waktu istirahatmu dengan sepenuh hati.",
  "Absen untuk istirahatmu sukses! Ini saatnya untuk merilekskan diri sejenak dan mengisi energi.",
  "Great job! Kamu telah absen untuk istirahat. Gunakan waktu ini sebaik mungkin untuk menyegarkan pikiranmu.",
  "Yeay, kamu berhasil absen untuk istirahat! Sekarang, mari nikmati waktu istirahat dengan tenang.",
  "Selamat, istirahatmu sudah dicatat. Ini kesempatan untuk mereset diri dan kembali dengan semangat baru.",
  "Istirahatmu sudah diatur dengan baik. Saatnya untuk melepaskan beban sejenak dan merilekskan diri."
];

const SUCCESS_AFTER_BREAK_SENTENCES = [
  "Selamat! Kamu telah kembali setelah istirahat dan berhasil melakukan absen. Ayo, kita lanjutkan dengan semangat!",
  "Absen setelah istirahatmu sukses! Sekarang, mari kita lanjutkan dengan semangat penuh.",
  "Great job! Kamu sudah kembali dari istirahat dan berhasil melakukan absen. Waktu untuk bekerja kembali!",
  "Yeay, kamu berhasil absen setelah istirahat! Sekarang, mari kita fokus dan lanjutkan pekerjaan dengan semangat.",
  "Selamat, absen setelah istirahatmu sudah dicatat dengan baik. Saatnya untuk kembali bekerja dengan penuh energi.",
  "Kamu telah sukses absen setelah istirahat. Mari kita lanjutkan dengan semangat tinggi dan tekad yang kuat."
];

const SUCCESS_CLOCK_OUT_SENTENCES = [
  "Hore! Kamu telah berhasil melakukan absen pulang kerja. Selamat beristirahat dan nikmati malammu!",
  "Absen pulang kerjamu sukses! Ini waktunya untuk bersantai dan menikmati waktu bersama keluarga atau melakukan hal-hal yang kamu cintai.",
  "Great job! Kamu sudah absen setelah seharian kerja. Sekarang, nikmati waktu luangmu dengan tenang.",
  "Yeay, kamu berhasil absen pulang kerja! Sekarang, mari lepaskan beban kerja dan nikmati waktu malammu.",
  "Selamat, absen pulang kerjamu sudah dicatat. Saatnya untuk merilekskan diri dan menikmati malam yang tenang."
];

export {
  MENU_HOME,
  MENU_ABSENCE,
  MENU_MANAGEMENT,
  MENU_PERSONAL,
  MENU_EOFFICE,
  MONTHS,
  DUMMY_FLATLIST,
  JENIS_IZIN,
  LAST_THREE_YEARS,
  LOKASI_PELAKSANAAN,
  ABSENCE_TYPE,
  OUTSIDE_RADIUS_SENTENCES,
  SUCCESS_CLOCK_IN_SENTENCES,
  SUCCESS_BREAK_SENTENCES,
  SUCCESS_AFTER_BREAK_SENTENCES,
  SUCCESS_CLOCK_OUT_SENTENCES
};
