const ENDPOINTS = {
  GetListNews: "news/newslist",
  GetLongLatOffice: "Mcabang/GetCabang",
  PostTrx: "trx/PostTrx",

  // FAQ
  GetFaq: "faq/GetFaq",

  // Izin
  GetIjin: "ijin/GetIjin",

  // Report
  ReportAbsenNup: "report/reportAbsenNup",
  ReportAbsenAll: "report/reportAbsenAll",
  PostIjin: "ijin/PostIjin",

  // E-Office
  GetMessage: "eoffice/GetMessage",
  GetCountUnreadsurat: "eoffice/GetcountUnreadsurat",
  GetTembusan: "eoffice/Getembusan",
  GetCountUnreadTembusan: "eoffice/GetcountUnreadTembusan",
  GetListApproval: "eoffice/GeListApproval",
  GetCountApproval: "eoffice/GecountApproval",
  GetMetadataSurat: "eoffice/GetMetadataSurat",
  GetSentItem: "eoffice/GetSentItem",
  GetCountUnreaddisposisi: "eoffice/GetcountUnreaddisposisi",
  GetListDisposisi: "eoffice/GeListDisposisi",
  GetListDisposisiKeluar: "eoffice/GeListDisposisiKeluar",

  // Management
  GetDaysAbsen: "management/GetDaysAbsen",
  GetlatitudeDaysByNup: "management/getlatitudeDaysByNup",

  // Profile
  GetProfile: "profile/GetProfile",
  InsertDeviceId: "profile/InsertDeviceId"
};

export { ENDPOINTS };
