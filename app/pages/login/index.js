import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Platform,
  StyleSheet,
  StatusBar,
  Alert,
  Image,
  ActivityIndicator,
  BackHandler,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import { FontAwesome, Feather } from "@expo/vector-icons";
import moment from "moment";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { useSelector, useDispatch } from "react-redux";
import sha512 from "js-sha512";

import {
  API,
  CREATE_UPDATE_TOKEN,
  COLORS,
  GET_UNIQUE_ID,
  ENDPOINTS,
} from "../../services";
import { LogoWhite } from "../../assets";
import {
  SET_JABATAN,
  SET_LOGGED,
  SET_NAME,
  SET_NUP,
  SET_UNIT_KERJA,
} from "../../redux";
import { CustomText } from "../../components";

const SignInScreen = ({ navigation }) => {
  const { appID } = useSelector((state) => state.LoginReducer);
  const [data, setData] = useState({
    nup: "",
    password: "",
    check_textInputChange: false,
    secureTextEntry: true,
    isValidUser: true,
    isValidPassword: true,
  });
  const [isLoading, setisLoading] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const textInputChange = (val) => {
    if (val.trim().length > 4) {
      setData({
        ...data,
        nup: val,
        check_textInputChange: true,
        isValidUser: true,
      });
    } else {
      setData({
        ...data,
        nup: val,
        check_textInputChange: false,
        isValidUser: false,
      });
    }
  };

  const handlePasswordChange = (val) => {
    var isi = sha512(val);
    if (val.trim().length >= 8) {
      setData({
        ...data,
        password: isi,
        isValidPassword: true,
      });
    } else {
      setData({
        ...data,
        password: isi,
        isValidPassword: false,
      });
    }
  };

  const getGreetingTime = (m) => {
    var g = null; //return g

    if (!m || !m.isValid()) {
      return;
    } //if we can't find a valid or filled moment, we return.

    var split_afternoon = 12; //24hr time to split the afternoon
    var split_evening = 17; //24hr time to split the evening
    var currentHour = parseFloat(m.format("HH"));

    if (currentHour >= split_afternoon && currentHour <= split_evening) {
      g = "Afternoon";
    } else if (currentHour >= split_evening) {
      g = "Evening";
    } else {
      g = "Morning";
    }

    return g;
  };

  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const sendLogin = (nup, password) => {
    setisLoading(true);

    try {
      if (nup != "" && password != "") {
        API.create("auth")
          .login({ email: nup, sandi: password, app: appID })
          .then(async (response) => {
            const deviceId = await GET_UNIQUE_ID();
            // alert("deviceID yg tersimpan => ", deviceId);

            if (response.data.status == "inactive") {
              Alert.alert(JSON.stringify(response.data.message));
            } else if (response.data.status == "mismatch") {
              Alert.alert("Info", JSON.stringify(response.data.message));
            } else {
              const getDeviceId = await API.create("Api").POST(
                ENDPOINTS.GetProfile,
                {
                  key: response.data.key,
                },
                {
                  headers: {
                    "Content-Type": "application/json",
                    // "Access-Control-Request-Headers": "*"
                  },
                }
              );

              // alert("Get device ID => ", JSON.stringify(getDeviceId.data[0]));

              // if (getDeviceId.data.length > 0) {
              //   if (getDeviceId.data[0].device_id) {
              //     if (getDeviceId.data[0].device_id == deviceId) {
                    dispatch(SET_LOGGED(true));
                    dispatch(SET_NAME(response.data.nama));
                    dispatch(SET_NUP(response.data.nup));
                    dispatch(SET_JABATAN(response.data.jabatan));
                    dispatch(SET_UNIT_KERJA(response.data.kodeDepartemen));

                    setTimeout(async () => {
                      const result = await CREATE_UPDATE_TOKEN(
                        response.data.nup,
                        response.data.key
                      );

                      if (result !== null && result.updated) {
                        console.log(`Token updated`);

                        navigation.replace("bottomTab");
                      } else if (result !== null && result.inserted) {
                        console.log(`New token inserted`);

                        navigation.replace("bottomTab");
                      } else {
                        console.log(`Error checking/updating token`);
                        alert(`Error checking/updating token`);
                      }
                    }, 500);
              //     } else {
              //       alert("Uppss, kamu mau coba login di hp orang yaaa?");
              //     }
              //   } else {
              //     // navigation.replace("bottomTab");
              //     const insertDeviceId = await API.create("Api").POST(
              //       ENDPOINTS.InsertDeviceId,
              //       {
              //         key: response.data.key,
              //         deviceId,
              //       },
              //       {
              //         headers: {
              //           "Content-Type": "application/json",
              //           // "Access-Control-Request-Headers": "*"
              //         },
              //       }
              //     );

              //     // alert("insert baru => ", JSON.stringify(insertDeviceId));

              //     if (insertDeviceId.data.status == "Succes") {
              //       dispatch(SET_LOGGED(true));
              //       dispatch(SET_NAME(response.data.nama));
              //       dispatch(SET_NUP(response.data.nup));
              //       dispatch(SET_JABATAN(response.data.jabatan));
              //       dispatch(SET_UNIT_KERJA(response.data.kodeDepartemen));

              //       setTimeout(async () => {
              //         const result = await CREATE_UPDATE_TOKEN(
              //           response.data.nup,
              //           response.data.key
              //         );

              //         if (result !== null && result.updated) {
              //           console.log(`Token updated`);

              //           navigation.replace("bottomTab");
              //         } else if (result !== null && result.inserted) {
              //           console.log(`New token inserted`);

              //           navigation.replace("bottomTab");
              //         } else {
              //           console.log(`Error checking/updating token`);
              //           alert(`Error checking/updating token`);
              //         }
              //       }, 500);
              //     } else {
              //       alert("Uppss, ada kesalahan dalam insert device id");
              //     }
              //   }
              // }
            }
          });
      } else {
        Alert.alert("Info", "Please enter your nup/password first");
      }
    } catch (error) {
      Alert.alert("Error", error);
    }

    setTimeout(() => {
      setisLoading(false);
    }, 500);
  };

  // if (isLoading) {
  //   return (
  //     <ActivityIndicator
  //       color={"blue"}
  //       size={"large"}
  //       style={{
  //         // flex: 1,
  //         height: "100%",
  //         width: "100%",
  //         position: "absolute",
  //         justifyContent: "center",
  //         alignItems: "center",
  //       }}
  //     />
  //   );
  // }

  return (
    <View style={styles.container}>
      <LinearGradient style={styles.header} colors={COLORS.gradient}>
        <Image
          source={LogoWhite}
          resizeMode="contain"
          style={{ width: 65, height: 65 }}
        />
        <View>
          <CustomText style={styles.text_header}>Welcome!</CustomText>
          <CustomText regular style={{ color: "white" }}>
            Good {getGreetingTime(moment())}, please login first to access our
            management system.
          </CustomText>
        </View>
      </LinearGradient>
      <View style={{ backgroundColor: "#055DB3", flex: 3 }}>
        <Animatable.View
          animation="fadeInUpBig"
          style={[
            styles.footer,
            {
              backgroundColor: "white",
            },
          ]}
        >
          <CustomText regular style={[styles.text_footer, { color: "black" }]}>
            Nup
          </CustomText>
          <View style={styles.action}>
            <FontAwesome name="user-o" color={"#192f6a"} size={20} />
            <TextInput
              placeholder="Type nup here"
              placeholderTextColor="#666666"
              style={styles.textInput}
              autoCapitalize="none"
              onChangeText={(val) => textInputChange(val)}
            />
            {data.check_textInputChange ? (
              <Animatable.View animation="bounceIn">
                <Feather name="check-circle" color="green" size={20} />
              </Animatable.View>
            ) : null}
          </View>
          {data.isValidUser ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <CustomText regular style={styles.errorMsg}>
                Username must be 5 or more than 5 characters long.
              </CustomText>
            </Animatable.View>
          )}

          <CustomText
            regular
            style={[styles.text_footer, { color: "black", marginTop: 35 }]}
          >
            Password
          </CustomText>
          <View style={styles.action}>
            <Feather name="lock" color={"#192f6a"} size={20} />
            <TextInput
              placeholder="Your Password"
              placeholderTextColor="#666666"
              secureTextEntry={data.secureTextEntry ? true : false}
              style={[styles.textInput, { color: "black" }]}
              autoCapitalize="none"
              onChangeText={(val) => handlePasswordChange(val)}
            />
            <TouchableOpacity onPress={updateSecureTextEntry}>
              {data.secureTextEntry ? (
                <Feather name="eye-off" color="grey" size={20} />
              ) : (
                <Feather name="eye" color="#192f6a" size={20} />
              )}
            </TouchableOpacity>
          </View>
          {data.isValidPassword ? null : (
            <Animatable.View animation="fadeInLeft" duration={500}>
              <CustomText regular style={styles.errorMsg}>
                Password must be 8 characters long.
              </CustomText>
            </Animatable.View>
          )}

          <TouchableOpacity>
            <CustomText regular style={{ color: "#192f6a", marginTop: 15 }}>
              Forgot password?
            </CustomText>
          </TouchableOpacity>
          <View style={styles.button}>
            <TouchableOpacity
              style={styles.signIn}
              onPress={() => {
                sendLogin(data.nup, data.password);
              }}
            >
              <LinearGradient colors={COLORS.gradient} style={styles.signIn}>
                {isLoading ? (
                  <ActivityIndicator
                    color={"white"}
                    size={"large"}
                    style={{
                      // flex: 1,
                      height: "100%",
                      width: "100%",
                      position: "absolute",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  />
                ) : (
                  <CustomText
                    style={[
                      styles.textSign,
                      {
                        color: "#fff",
                      },
                    ]}
                    bold
                  >
                    Sign In
                  </CustomText>
                )}
              </LinearGradient>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={[
                styles.signIn,
                {
                  borderColor: "#192f6a",
                  borderWidth: 1,
                  marginTop: 15,
                },
              ]}
            >
              <CustomText style={[styles.textSign, { color: "#192f6a" }]}>
                Back
              </CustomText>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </View>
    </View>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4c669f",
  },
  header: {
    flex: 1,
    justifyContent: "space-between",
    marginTop: getStatusBarHeight(),
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});
