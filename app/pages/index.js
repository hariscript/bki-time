import Home from "./home";
import ScreenContainer from "./screenContainer";
import Faq from "./faq";
import AuthLoading from "./authLoading";
import WelcomeAuth from "./welcomeAuth";
import Login from "./login";
import Maps from "./maps";
import Profile from "./profile";
import Absence from "./absence";
import {
  ClockIn,
  AfterBreak,
  Break,
  ClockOut,
  DetailReport,
  Summary,
  ListPermission,
  EntryPermission,
  DetailPermission
} from "./menuAbsence";
import Camera from "./camera";
import Management from "./management";
import {
  DetailOvertime,
  FormOnLeave,
  OnLeave,
  Overtime,
  Salary
} from "./menuPersonal";
import Personal from "./personal";
import Eoffice from "./eoffice";
import {
  ListEoffice,
  DetailEoffice,
  Tembusan,
  ApprovalEoffice,
  Disposisi,
  DisposisiKeluar,
  SentItem
} from "./menuEoffice";
import Helpdesk from "./helpdesk";
import { AddHelpdesk, DetailHelpdesk, AnswerHelpdesk } from "./menuHelpdesk";
import { ManageAbsence, ManageDetail, ManageSummary } from "./menuManagement";
import ComingSoon from "./comingSoon";

export {
  Home,
  ScreenContainer,
  Faq,
  AuthLoading,
  WelcomeAuth,
  Login,
  Maps,
  Profile,
  Absence,
  ClockIn,
  Break,
  AfterBreak,
  ClockOut,
  Camera,
  Management,
  Personal,
  DetailReport,
  Summary,
  ListPermission,
  EntryPermission,
  DetailPermission,
  Salary,
  OnLeave,
  FormOnLeave,
  Overtime,
  DetailOvertime,
  Eoffice,
  ListEoffice,
  DetailEoffice,
  Helpdesk,
  AddHelpdesk,
  DetailHelpdesk,
  AnswerHelpdesk,
  ManageAbsence,
  ManageDetail,
  ManageSummary,
  ComingSoon,
  Tembusan,
  ApprovalEoffice,
  Disposisi,
  DisposisiKeluar,
  SentItem
};
