import { LinearGradient } from "expo-linear-gradient";
import moment from "moment";
import React, { useEffect, useRef, useState } from "react";
import {
  Alert,
  Animated,
  BackHandler,
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from "react-native";
import { View } from "react-native-animatable";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { useDispatch, useSelector } from "react-redux";
import { FontAwesome } from "@expo/vector-icons";

import { Absence, BG, LogoApp, LogoWhite, Placeholder } from "../../assets";
import {
  ButtonMenu,
  CustomText,
  LoadingIndicator,
  LoadingNews,
  ModalUnderConstraction,
} from "../../components";
import { API, CALL_API, COLORS, ENDPOINTS, MENU_HOME } from "../../services";
import { useFocusEffect } from "@react-navigation/native";
import { SET_LONG_LAT_OFFICE } from "../../redux";

const Home = ({ navigation }) => {
  const {
    nama,
    nup,
    unit_kerja,
    clock_in,
    clock_out,
    trigger_color,
    time_worked,
  } = useSelector((state) => state.ProfileReducer);
  const dispatch = useDispatch();

  const [visibleHeader, setvisibleHeader] = useState(true);
  const [loading, setLoading] = useState(false);
  const [btnToTop, setBtnToTop] = useState(false);
  const [listNews, setListNews] = useState([]);
  const [modalUC, setModalUC] = useState(false);
  const [hours, setHours] = useState(null);
  const [minutes, setMinutes] = useState(null);
  const [timeWorked, setTimeWorked] = useState(null);
  const ref = useRef();

  const onFabPress = () => {
    ref.current?.scrollTo({
      y: 0,
      animated: true,
    });
  };

  useFocusEffect(
    React.useCallback(() => {
      // Do something when the screen is focused
      BackHandler.addEventListener("hardwareBackPress", backPressed);
      dataNews();
      countWorkingHour();

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
        BackHandler.removeEventListener("hardwareBackPress", backPressed);
      };
    }, [])
  );

  const countWorkingHour = () => {
    const now = new Date();
    const diffTime = now.getTime() - new Date(clock_in).getTime();
    const hoursWorked = Math.floor(diffTime / (1000 * 60 * 60)); // hours
    const minutesWorked = Math.floor(
      (diffTime % (1000 * 60 * 60)) / (1000 * 60)
    ); // minutes
    const formattedHours = String(hoursWorked).padStart(2, "0");
    const formattedMinutes = String(minutesWorked).padStart(2, "0");

    if (
      moment(clock_in).format("DD MMMM YYYY") !=
        moment(new Date()).format("DD MMMM YYYY") &&
      moment(clock_out).format("DD MMMM YYYY") !=
        moment(new Date()).format("DD MMMM YYYY")
    ) {
      setTimeWorked(`--`);
    } else if (
      moment(clock_in).format("DD MMMM YYYY") ==
        moment(new Date()).format("DD MMMM YYYY") &&
      moment(clock_out).format("DD MMMM YYYY") !=
        moment(new Date()).format("DD MMMM YYYY")
    ) {
      setTimeWorked(`${formattedHours}:${formattedMinutes}`);
    } else {
      setTimeWorked(time_worked);
    }
  };

  const backPressed = () => {
    if (Platform.OS === "ios") {
      // Minimize the app on iOS
      return true;
    } else {
      Alert.alert(
        "Exit App",
        "Exiting the application?",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => BackHandler.exitApp(),
          },
        ],
        {
          cancelable: false,
        }
      );
      return true;
    }
  };

  const dataNews = async () => {
    try {
      setLoading(true);
      const response = await CALL_API(
        ENDPOINTS.GetListNews,
        null,
        "GET",
        nup,
        navigation
      );

      setListNews(response.finalResponse);
      setLoading(false);

      setTimeout(() => {
        getLongLatOffice();
      }, 1000);
    } catch (error) {
      console.log(error);
    }
  };

  const getLongLatOffice = async () => {
    try {
      const response = await API.create("hrms").longlat({
        code: unit_kerja,
      });

      const longLat = {
        latitude_center: response.data.data[0].latitude,
        longitude_center: response.data.data[0].longitude,
      };

      setLoading(false);

      dispatch(SET_LONG_LAT_OFFICE(longLat));
    } catch (error) {
      console.log(error);
    }
  };

  const renderItems = () => {
    const items = [];

    for (let i = 0; i < MENU_HOME.length; i += 4) {
      const rowItems = MENU_HOME.slice(i, i + 4);
      items.push(
        // Second Option

        // <View
        //   key={`row_${i}`}
        //   style={{
        //     flexDirection: "row",
        //     // backgroundColor: "red",
        //     marginHorizontal: 15,
        //     justifyContent: "space-between"
        //   }}
        // >
        <View key={`row_${i}`} style={{ flexDirection: "row" }}>
          {rowItems.map((item, index) => (
            <View key={index}>
              <ButtonMenu
                onPress={() => {
                  if (item.nama == "Personal" || item.nama == "Hepldesk") {
                    setModalUC(true);
                  } else {
                    navigation.navigate(item.navigation);
                  }
                }}
                text={item.nama}
                style={styles.btnMenu}
                styleText={{ textAlign: "center" }}
                gambar={item.gambar}
              />
            </View>
          ))}
        </View>
      );
    }

    return items;
  };

  const renderItem = ({ item, index }) => {
    return (
      <View key={index} style={styles.card}>
        <Image
          source={
            item.gambar !== "" && item.gambar !== null
              ? {
                  uri: `https://dev-apibkitime.bki.co.id/foto/${item.gambar}`,
                }
              : Placeholder
          }
          style={styles.images}
          resizeMode={"cover"}
          borderTopLeftRadius={10}
          borderTopRightRadius={10}
        />
        <View style={styles.contentCard}>
          <View>
            <CustomText style={{ fontSize: 16 }} bold>
              {item.title === null ? "Breaking news" : item.title}
            </CustomText>
            <CustomText style={{ fontSize: 12, marginTop: 5 }} regular>
              {moment(item.created_at).format("DD MMM YYYY")}
            </CustomText>
          </View>
          <View style={styles.containerReadmore}>
            <LinearGradient colors={COLORS.gradient} style={styles.readmore}>
              <CustomText style={{ fontSize: 10, color: "white" }} bold>
                Readmore...
              </CustomText>
            </LinearGradient>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <ModalUnderConstraction
        visible={modalUC}
        type={"failed"}
        text={"New feature still in progress!"}
        onRequestClose={() => {
          setModalUC(!modalUC);
        }}
        onPress={() => {
          setModalUC(false);
        }}
        onPressExit={() => {
          setModalUC(false);
        }}
      />

      <LinearGradient style={styles.header} colors={COLORS.gradient}>
        <Image source={LogoWhite} style={{ height: 40 }} resizeMode="contain" />
      </LinearGradient>

      <ScrollView
        ref={ref}
        showsVerticalScrollIndicator={false}
        onScroll={(event) => {
          if (
            event.nativeEvent.contentOffset.y > 0 ||
            event.nativeEvent.contentOffset.y < 0
          ) {
            setvisibleHeader(false);
            setBtnToTop(true);
          } else {
            setvisibleHeader(true);
            setBtnToTop(false);
          }
        }}
        scrollEventThrottle={1}
      >
        <View style={{ backgroundColor: "white", paddingVertical: 20 }}>
          <View style={styles.textHeader}>
            <CustomText style={{ fontSize: 18 }} regular>
              Halo, {""}
            </CustomText>
            <CustomText
              style={{ fontSize: 16, textTransform: "capitalize" }}
              bold
            >
              {nama}
            </CustomText>
          </View>

          <ImageBackground
            source={BG}
            borderRadius={10}
            style={[
              styles.cardReminder,
              { marginHorizontal: 20, marginBottom: 30 },
            ]}
          >
            <View style={{ paddingVertical: 15 }}>
              <CustomText regular>Jangan lupa absen yaa ☺️</CustomText>

              <View style={{ flexDirection: "row", marginTop: 15 }}>
                <View>
                  <CustomText regular style={{ fontSize: 12, color: "grey" }}>
                    Clock-in
                  </CustomText>
                  <CustomText regular style={{ fontSize: 14, marginTop: 5 }}>
                    {clock_in &&
                    moment(clock_in).format("DD MMMM YYYY") ==
                      moment(new Date()).format("DD MMMM YYYY")
                      ? moment(clock_in).format("hh:mm")
                      : "--"}
                  </CustomText>
                </View>

                <View style={styles.containerSparator}>
                  <View style={styles.sparator} />
                  <View style={styles.sparator} />
                  <View style={styles.sparator} />
                </View>

                <View>
                  <CustomText regular style={{ fontSize: 12, color: "grey" }}>
                    Clock-out
                  </CustomText>
                  <CustomText regular style={{ fontSize: 14, marginTop: 5 }}>
                    {clock_out &&
                    moment(clock_out).format("DD MMMM YYYY") ==
                      moment(new Date()).format("DD MMMM YYYY")
                      ? moment(clock_out).format("hh:mm")
                      : "--"}
                  </CustomText>
                </View>
              </View>
            </View>
            <View
              style={{
                height: "100%",
                justifyContent: "center",
                alignItems: "center",
                marginLeft: 25,
                marginRight: 15,
              }}
            >
              <View
                style={{
                  borderWidth: 0.5,
                  height: "100%",
                  borderColor: "white",
                }}
              />
            </View>

            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <CustomText
                style={{
                  fontSize: 30,
                  color: trigger_color,
                }}
              >
                {timeWorked}
              </CustomText>
              <CustomText style={{ color: "grey", fontSize: 12 }}>
                Working Hour
              </CustomText>
            </View>
          </ImageBackground>

          {renderItems()}
        </View>

        <View style={{ marginBottom: 20, marginLeft: 20 }}>
          <CustomText style={{ fontSize: 18, marginRight: 20 }} bold>
            Informasi BKI Terkini
          </CustomText>

          {listNews.length > 0 && !loading ? (
            <FlatList
              horizontal
              showsHorizontalScrollIndicator={false}
              data={listNews}
              renderItem={renderItem}
            />
          ) : (
            <View style={{ flexDirection: "row" }}>
              <LoadingNews style={{ marginRight: 20 }} />
              <LoadingNews />
            </View>
          )}
        </View>
      </ScrollView>

      {btnToTop && (
        <TouchableOpacity
          style={styles.fab}
          onPress={() => {
            onFabPress();
          }}
        >
          <View style={styles.contentFab}>
            <FontAwesome name="angle-double-up" size={30} color="white" />
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop:
      Platform.OS === "ios" ? getStatusBarHeight() + 15 : getStatusBarHeight(),
    paddingBottom: 30,
    // backgroundColor: "red"
  },
  btnMenu: {
    width: Dimensions.get("window").width / 4,
    // width: Dimensions.get("window").width / 4 - 10,
    textAlign: "center",
    marginBottom: 10,
  },
  cardReminder: {
    marginTop: 15,
    paddingHorizontal: 15,
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    flexDirection: "row",
    height: 100,
  },
  card: {
    marginTop: 15,
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    // flexDirection: "row",
    elevation: 5,
    marginBottom: 20,
    width: 310,
    marginRight: 20,
    marginLeft: 5,
  },
  readmore: {
    // backgroundColor: "blue",
    paddingVertical: 3,
    width: "40%",
    borderRadius: 10,
    marginTop: 5,
    alignItems: "center",
  },
  contentCardLeft: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start",
  },
  contentCardSquare: {
    // backgroundColor: "green",
    width: 90,
    height: 90,
    borderRadius: 10,
    justifyContent: "center",
  },
  fab: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: "#055DB3",
    position: "absolute",
    bottom: 20,
    right: 20,
    justifyContent: "center",
    opacity: 0.9,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
  },
  contentFab: {
    justifyContent: "center",
    alignItems: "center",
  },
  images: {
    width: "100%",
    height: 100,
    alignSelf: "center",
  },
  textHeader: {
    marginBottom: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  sparator: {
    borderWidth: 0.5,
    height: 10,
    borderColor: "grey",
  },
  containerSparator: {
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 30,
  },
  containerReadmore: {
    justifyContent: "flex-end",
    flexDirection: "row",
  },
  contentCard: {
    padding: 15,
    justifyContent: "space-between",
    height: 110,
  },
});
