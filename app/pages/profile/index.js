import React, { useEffect, useState } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  BackHandler,
  Alert,
} from "react-native";
import {
  Entypo,
  MaterialIcons,
  Ionicons,
  MaterialCommunityIcons,
  FontAwesome,
} from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import { useDispatch, useSelector } from "react-redux";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { COLORS } from "../../services";
import { SET_LOGGED } from "../../redux";
import { CustomText } from "../../components";
import { User } from "../../assets";

const Profile = ({ navigation }) => {
  const { nama, jabatan, photo } = useSelector((state) => state.ProfileReducer);

  const dispatch = useDispatch();
  const backPressed = () => {
    Alert.alert(
      "Exit App",
      "Exiting the application?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => BackHandler.exitApp(),
        },
      ],
      {
        cancelable: false,
      }
    );
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  return (
    <View style={styles.container}>
      <LinearGradient colors={COLORS.gradient} style={styles.header}>
        <View style={styles.contentHeader}>
          <View style={{ flex: 1 }}></View>
          <Text style={styles.textHeader}>Profile Page</Text>
          <Ionicons
            name="notifications"
            size={25}
            color="white"
            style={{ flex: 1, textAlign: "right" }}
          />
        </View>

        <View style={styles.componentHeader}>
          <View style={{ flex: 0.7 }}>
            <Image
              source={
                photo
                  ? {
                      uri:
                        "https://api.bki.co.id/api-sikom/config/foto_trx/" +
                        photo,
                    }
                  : User
              }
              style={styles.avatar}
            />
          </View>
          <View style={styles.containerContent}>
            <CustomText style={styles.nama}>{nama}</CustomText>
            <CustomText style={styles.titleJob} regular>
              {jabatan}
            </CustomText>
          </View>
        </View>
      </LinearGradient>

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.viewAttendance}>
          <CustomText style={styles.textAttendance}>
            Attendance This Month
          </CustomText>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={styles.rectangleAttendance}>
              <View style={styles.rectangleIconCheck}>
                <FontAwesome name="check-square" size={24} color="#38ba45" />
              </View>

              <CustomText style={styles.titleAttendance} regular>
                Arrived on time
              </CustomText>
              <CustomText style={styles.countAttendance}>20</CustomText>
            </View>
            <View style={styles.rectangleAttendance}>
              <View style={styles.rectangleIconWarn}>
                <Ionicons name="md-warning" size={24} color="#ffdd00" />
              </View>

              <CustomText style={styles.titleAttendance} regular>
                Arrived late
              </CustomText>
              <CustomText style={styles.countAttendance}>5</CustomText>
            </View>
            <View style={styles.rectangleAttendance}>
              <View style={styles.rectangleIconCross}>
                <Entypo name="squared-cross" size={24} color="#d10011" />
              </View>

              <CustomText style={styles.titleAttendance} regular>
                Leave
              </CustomText>
              <CustomText style={styles.countAttendance}>5</CustomText>
            </View>
          </ScrollView>
        </View>

        <View style={{ marginTop: 15 }}>
          <TouchableOpacity
            style={styles.viewMenu}
            activeOpacity={0.7}
            onPress={() => navigation.navigate("Coming Soon")}
          >
            <View style={styles.iconNameMenu}>
              <MaterialCommunityIcons
                name="account-arrow-right-outline"
                size={30}
                color="#ED5B26"
              />
              <CustomText style={{ marginLeft: 15 }} regular>
                Personal Information
              </CustomText>
            </View>
            <MaterialIcons
              name="keyboard-arrow-right"
              size={30}
              color="black"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewMenu}
            activeOpacity={0.7}
            onPress={() => navigation.navigate("Coming Soon")}
          >
            <View style={styles.iconNameMenu}>
              <MaterialIcons name="family-restroom" size={30} color="#ED5B26" />
              <CustomText style={{ marginLeft: 15 }} regular>
                Family Information
              </CustomText>
            </View>
            <MaterialIcons
              name="keyboard-arrow-right"
              size={30}
              color="black"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewMenu}
            activeOpacity={0.7}
            onPress={() => navigation.navigate("Coming Soon")}
          >
            <View style={styles.iconNameMenu}>
              <MaterialIcons name="history" size={30} color="#ED5B26" />
              <CustomText style={{ marginLeft: 15 }} regular>
                History
              </CustomText>
            </View>
            <MaterialIcons
              name="keyboard-arrow-right"
              size={30}
              color="black"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.viewMenu}
            activeOpacity={0.7}
            onPress={() => navigation.navigate("Coming Soon")}
          >
            <View style={styles.iconNameMenu}>
              <Ionicons name="notifications" size={30} color="#ED5B26" />
              <CustomText style={{ marginLeft: 15 }} regular>
                Notifications
              </CustomText>
            </View>
            <MaterialIcons
              name="keyboard-arrow-right"
              size={30}
              color="black"
            />
          </TouchableOpacity>
        </View>

        <View style={{ marginTop: 20, marginBottom: 10 }}>
          <TouchableOpacity
            style={styles.viewMenu}
            activeOpacity={0.7}
            onPress={() => {
              navigation.replace("Login");
              dispatch(SET_LOGGED(false));
            }}
          >
            <View style={styles.iconNameMenu}>
              <MaterialIcons name="logout" size={30} color="#ED5B26" />
              <CustomText style={{ marginLeft: 15 }} regular>
                Logout
              </CustomText>
            </View>
            <MaterialIcons
              name="keyboard-arrow-right"
              size={30}
              color="black"
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Profile;

const styles = StyleSheet.create({
  nama: {
    fontSize: 15,
    textAlign: "center",
    marginBottom: 5,
  },
  container: {
    flex: 1,
    // backgroundColor: "#f6f6f6",
  },
  textHeader: {
    flex: 1,
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
  header: {
    paddingHorizontal: 10,
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    paddingTop: getStatusBarHeight(),
  },
  contentHeader: {
    height: 60,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  componentHeader: {
    flexDirection: "row",
    padding: 20,
    backgroundColor: "white",
    marginVertical: 20,
    marginHorizontal: 10,
    borderRadius: 25,
  },
  avatar: {
    height: 100,
    width: 100,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: "#4c669f",
  },
  titleJob: {
    backgroundColor: "#d1e6ff",
    paddingVertical: 5,
    textAlign: "center",
    borderRadius: 10,
    color: "grey",
    fontWeight: "bold",
    width: "100%",
  },
  viewAttendance: {
    marginTop: 15,
    paddingHorizontal: 10,
  },
  textAttendance: {
    fontSize: 15,
    fontWeight: "bold",
  },
  rectangleAttendance: {
    borderRadius: 15,
    backgroundColor: "white",
    padding: 15,
    marginTop: 10,
    borderWidth: 0.5,
    borderColor: "#bdbdbd",
    marginRight: 15,
    width: 140,
    height: 150,
  },
  rectangleIconCheck: {
    height: 50,
    width: 50,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#38ba45",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#bfffc6",
  },
  titleAttendance: {
    marginTop: 10,
    color: "#4d4d4d",
    fontWeight: "bold",
  },
  countAttendance: {
    fontSize: 20,
    fontWeight: "bold",
    marginTop: 5,
  },
  rectangleIconWarn: {
    height: 50,
    width: 50,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#ffdd00",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#feffcf",
  },
  rectangleIconCross: {
    height: 50,
    width: 50,
    borderRadius: 15,
    borderWidth: 1,
    borderColor: "#d10011",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ffb8be",
  },
  viewMenu: {
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    padding: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: "grey",
    alignItems: "center",
  },
  containerContent: {
    flex: 1.3,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: 5,
  },
  iconNameMenu: { flexDirection: "row", alignItems: "center" },
});
