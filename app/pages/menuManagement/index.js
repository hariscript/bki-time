import ManageAbsence from "./manageAbsence";
import ManageDetail from "./manageAbsence/detail";
import ManageSummary from "./manageAbsence/summary";

export { ManageAbsence, ManageDetail, ManageSummary };
