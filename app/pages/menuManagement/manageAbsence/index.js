import React, { useCallback, useRef, useState } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { AntDesign } from "@expo/vector-icons";
import moment from "moment";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";

import { CustomText, Loading } from "../../../components";
import { COLORS, CALL_API, ENDPOINTS } from "../../../services";
import { ClockIn } from "../../../assets";

const ManageAbsence = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      dataAbsenPersonil();
      return () => {};
    }, [])
  );

  const dataAbsenPersonil = async () => {
    setLoading(true);

    const payload = {
      tanggal: new Date(),
    };
    const response = await CALL_API(
      ENDPOINTS.GetDaysAbsen,
      payload,
      "POST",
      nup,
      navigation
    );

    setData(response.finalResponse.data);
    setLoading(false);
  };

  const renderItemContent = useCallback(
    ({ item }) => (
      <TouchableOpacity
        style={styles.itemContainerAbsence}
        onPress={() =>
          navigation.navigate("Detail Absence", {
            nup_personil: item.nup,
          })
        }
      >
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            // backgroundColor: "green"
          }}
        >
          <Image
            source={ClockIn}
            style={{ width: 60, height: 60, backgroundColor: "red" }}
            resizeMode={"contain"}
            borderRadius={30}
          />
        </View>
        <View style={{ flex: 3.2, paddingVertical: 10 }}>
          <CustomText
            style={{
              fontSize: 14,
              color: COLORS.boldText,
              marginBottom: 5,
              textTransform: "capitalize",
            }}
          >
            {item.nama} ({item.nup})
          </CustomText>
          <View style={{ flexDirection: "row", marginBottom: 5 }}>
            <CustomText style={{ fontSize: 11, width: "23%" }} regular>
              Clock In
            </CustomText>
            <CustomText style={{ fontSize: 11, width: "2%" }} regular>
              :
            </CustomText>
            <CustomText style={{ fontSize: 11, width: "75%" }} regular>
              {item.clock_in_time
                ? moment(item.clock_in_time).format("DD MMMM YYYY, hh:mm:ss")
                : "-"}
            </CustomText>
          </View>
          <View style={{ flexDirection: "row" }}>
            <CustomText style={{ fontSize: 11, width: "23%" }} regular>
              Clock Out
            </CustomText>
            <CustomText style={{ fontSize: 11, width: "2%" }} regular>
              :
            </CustomText>
            <CustomText style={{ fontSize: 11, width: "75%" }} regular>
              {item.clock_out_time
                ? moment(item.clock_out_time).format("DD MMMM YYYY, hh:mm:ss")
                : "-"}
            </CustomText>
          </View>
        </View>

        <View
          style={{
            flex: 0.4,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: COLORS.blue,
            borderBottomRightRadius: 10,
            borderTopRightRadius: 10,
          }}
        >
          <AntDesign name="right" size={24} color="white" />
        </View>
      </TouchableOpacity>
    ),
    []
  );

  return (
    <View style={styles.container}>
      {/* <View> */}
      <View style={styles.containerHeader}>
        <CustomText style={{ fontSize: 17 }}>Daftar Karyawan</CustomText>
      </View>

      <View style={styles.sparator} />

      {loading ? (
        <Loading />
      ) : (
        <FlatList
          data={data}
          renderItem={renderItemContent}
          keyExtractor={(item) => Number(item.nup)}
        />
      )}
      {/* </View> */}
    </View>
  );
};

export default ManageAbsence;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  sparator: {
    borderWidth: 0.3,
    borderColor: "#707070",
    marginHorizontal: 20,
    marginBottom: 15,
  },
  itemContainerAbsence: {
    marginVertical: 6,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: "#eee",
    flexDirection: "row",
    borderRadius: 10,
  },
  containerHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginVertical: 20,
  },
});
