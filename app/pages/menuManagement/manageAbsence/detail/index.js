import React, { useCallback, useRef, useState } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { BottomSheetFlatList, BottomSheetModal } from "@gorhom/bottom-sheet";
import { AntDesign, Entypo } from "@expo/vector-icons";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";

import { ButtomSheetModal, CustomText } from "../../../../components";
import {
  DUMMY_FLATLIST,
  MONTHS,
  COLORS,
  CALL_API,
  ENDPOINTS,
} from "../../../../services";
import { ClockIn } from "../../../../assets";

const ManageDetail = ({ route, navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);
  const { nup_personil } = route.params;

  const [open, setOpen] = useState(false);
  const [monthSelected, setMonthSelected] = useState("");
  const [data, setData] = useState([]);
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date());
  const [tempDate, settempDate] = useState(new Date());
  const [loading, setLoading] = useState(false);

  const bottmSheetRef = useRef(null);
  const snapPoints = ["50%"];

  const handlePresentModal = () => {
    bottmSheetRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  useFocusEffect(
    React.useCallback(() => {
      dataAbsenPersonil();
      return () => {};
    }, [date])
  );

  const dataAbsenPersonil = async () => {
    setLoading(true);

    const payload = {
      tanggal: date,
      nup_personil: nup_personil,
    };

    const response = await CALL_API(
      ENDPOINTS.GetlatitudeDaysByNup,
      payload,
      "POST",
      nup,
      navigation
    );

    setData(response.finalResponse.data);
    setLoading(false);
  };

  const renderItem = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setMonthSelected(item.name);
          bottmSheetRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const setShowing = () => {
    if (Platform.OS === "ios") {
      handlePresentModal();
    } else {
      setShow(true);
    }
  };

  const renderItemContent = useCallback(
    ({ item }) => (
      <TouchableOpacity style={styles.itemContainerAbsence}>
        <View
          style={{
            flex: 0.7,
            justifyContent: "center",
            alignItems: "center",
            // backgroundColor: "green"
          }}
        >
          <Image
            source={ClockIn}
            style={{ width: 50, height: 50, backgroundColor: "red" }}
            resizeMode={"contain"}
          />
        </View>
        <View style={{ flex: 2.3 }}>
          <CustomText style={{ fontSize: 16 }}>{item.nama}</CustomText>
          <View style={{ flexDirection: "row" }}>
            <CustomText style={{ fontSize: 12, width: "17%" }} regular>
              Waktu
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "3%" }} regular>
              :
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "80%" }} regular>
              {moment(item.datetime_trx).format("DD MMMM YYYY, hh:mm:ss")}
            </CustomText>
          </View>
          <View style={{ flexDirection: "row" }}>
            <CustomText style={{ fontSize: 12, width: "17%" }} regular>
              Status
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "3%" }} regular>
              :
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "80%" }} regular>
              {item.status_position}
            </CustomText>
          </View>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  return (
    <View style={styles.container}>
      {show && (
        <DateTimePicker
          value={date}
          mode="date"
          is24Hour={true}
          display="default"
          onChange={(event, selectedDate) => {
            const currentDate = selectedDate || date;

            setDate(currentDate);
            setShow(false);
          }}
        />
      )}
      <CustomText
        style={{
          marginTop: 15,
          fontSize: 16,
          color: COLORS.boldText,
          marginLeft: 20,
        }}
      >
        Daftar Absensi Karyawan (unit_kerja)
      </CustomText>
      <View style={{ opacity: open ? 0.3 : null }}>
        <TouchableOpacity style={styles.card} onPress={() => setShowing()}>
          <CustomText regular style={{ color: date ? "black" : "grey" }}>
            {moment(date).format("DD MMMM YYYY")}
          </CustomText>
          <AntDesign
            name="caretdown"
            size={10}
            color="black"
            style={{
              justifyContent: "center",
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>

        <View style={styles.sparator} />

        <FlatList
          data={data}
          renderItem={renderItemContent}
          keyExtractor={(item) => item.id}
        />
      </View>

      <ButtomSheetModal
        bottomSheetRef={bottmSheetRef}
        title={"Pilih Tanggal"}
        onDismiss={() => setOpen(false)}
        onPressClose={() => {
          bottmSheetRef.current?.close();
          setOpen(false);
        }}
        type={"date"}
        date={date}
        onChangeDate={(event, selectedDate) => {
          const currentDate = selectedDate || date;
          settempDate(currentDate);
        }}
        onPressOk={() => {
          setDate(tempDate);
          bottmSheetRef.current?.close();
        }}
      />
    </View>
  );
};

export default ManageDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    margin: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20,
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center",
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
  },
  itemContainerAbsence: {
    paddingVertical: 10,
    marginVertical: 6,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: "#eee",
    flexDirection: "row",
  },
});
