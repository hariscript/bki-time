import React, { useState, useRef, useCallback } from "react";
import {
  Animated,
  Dimensions,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { AntDesign, Entypo } from "@expo/vector-icons";
import {
  BottomSheetModal,
  BottomSheetModalProvider,
  BottomSheetFlatList,
} from "@gorhom/bottom-sheet";
import { useFocusEffect } from "@react-navigation/native";
import { useSelector } from "react-redux";
import DateTimePicker from "@react-native-community/datetimepicker";

import {
  BarCharts,
  ButtomSheetModal,
  CustomText,
  MapsWebViewMultiple,
} from "../../../../components";
import { MONTHS, COLORS, ENDPOINTS, CALL_API } from "../../../../services";
import moment from "moment";

const ManageSummary = ({ route, navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);
  const { latitude_center, longitude_center } = useSelector(
    (state) => state.OfficeReducer
  );
  const { nup_personil } = route.params;

  const [monthSelected, setMonthSelected] = useState("");
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(new Date());
  const [tempDate, settempDate] = useState(new Date());

  const bottmSheetRef = useRef(null);
  const snapPoints = ["50%"];

  const handlePresentModal = () => {
    bottmSheetRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  useFocusEffect(
    React.useCallback(() => {
      dataAbsenPersonil();
      return () => {};
    }, [date])
  );

  const dataAbsenPersonil = async () => {
    setLoading(true);

    let temp = [];

    const payload = {
      tanggal: date,
      nup_personil: nup_personil,
    };

    const response = await CALL_API(
      ENDPOINTS.GetlatitudeDaysByNup,
      payload,
      "POST",
      nup,
      navigation
    );

    if (response.finalResponse.data.length > 0) {
      for (let i = 0; i < response.finalResponse.data.length; i++) {
        const element = {
          nama: response.finalResponse.data[i].nama,
          lat: response.finalResponse.data[i].latitude,
          long: response.finalResponse.data[i].longitude,
          gambar: response.finalResponse.data[i].gambar,
        };

        temp.push(element);
      }
    }

    setData(temp);
    setLoading(false);
  };

  const renderItem = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setMonthSelected(item.name);
          bottmSheetRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const setShowing = () => {
    if (Platform.OS === "ios") {
      handlePresentModal();
    } else {
      setShow(true);
    }
  };

  return (
    <View style={[styles.container]}>
      {show && (
        <DateTimePicker
          value={date}
          mode="date"
          is24Hour={true}
          display="default"
          onChange={(event, selectedDate) => {
            const currentDate = selectedDate || date;

            setDate(currentDate);
            setShow(false);
          }}
        />
      )}
      <CustomText
        style={{
          marginTop: 15,
          fontSize: 16,
          color: COLORS.boldText,
          marginLeft: 20,
        }}
      >
        Daftar Absensi Karyawan (unit_kerja)
      </CustomText>

      <View style={{ opacity: open ? 0.3 : null }}>
        <TouchableOpacity style={styles.card} onPress={() => setShowing()}>
          <CustomText regular style={{ color: date ? "black" : "grey" }}>
            {moment(date).format("DD MMMM YYYY")}
          </CustomText>
          <AntDesign
            name="caretdown"
            size={10}
            color="black"
            style={{
              justifyContent: "center",
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>

        <View style={styles.sparator} />
      </View>

      <MapsWebViewMultiple
        user_loc={data}
        office_loc={[latitude_center, longitude_center]}
        style={{ zIndex: 0 }}
      />

      <ButtomSheetModal
        bottomSheetRef={bottmSheetRef}
        title={"Pilih Tanggal"}
        onDismiss={() => setOpen(false)}
        onPressClose={() => {
          bottmSheetRef.current?.close();
          setOpen(false);
        }}
        type={"date"}
        date={date}
        onChangeDate={(event, selectedDate) => {
          const currentDate = selectedDate || date;
          settempDate(currentDate);
        }}
        onPressOk={() => {
          setDate(tempDate);
          bottmSheetRef.current?.close();
        }}
      />
    </View>
  );
};

export default ManageSummary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    margin: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  sparator: {
    height: 0.5,
    backgroundColor: "#bcbcbc",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20,
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center",
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
  },
});
