import { useFocusEffect } from "@react-navigation/native";
import { LinearGradient } from "expo-linear-gradient";
import React, { useState } from "react";
import {
  Alert,
  BackHandler,
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
} from "react-native";
import { View } from "react-native-animatable";
import { getStatusBarHeight } from "react-native-status-bar-height";
import { useSelector } from "react-redux";
import { MaterialIcons } from "@expo/vector-icons";

import { CustomText } from "../../components";
import { CALL_API, COLORS, ENDPOINTS } from "../../services";

const Faq = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [countArray, setcountArray] = useState(0);
  const [temp, settemp] = useState([]);

  useFocusEffect(
    React.useCallback(() => {
      // Do something when the screen is focused
      BackHandler.addEventListener("hardwareBackPress", backPressed);
      dataFaq();

      return () => {
        // Do something when the screen is unfocused
        // Useful for cleanup functions
        BackHandler.removeEventListener("hardwareBackPress", backPressed);
      };
    }, [])
  );

  const backPressed = () => {
    if (Platform.OS === "ios") {
      // Minimize the app on iOS
      return true;
    } else {
      Alert.alert(
        "Exit App",
        "Exiting the application?",
        [
          {
            text: "Cancel",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => BackHandler.exitApp(),
          },
        ],
        {
          cancelable: false,
        }
      );
      return true;
    }
  };

  const dataFaq = async () => {
    setLoading(true);
    const response = await CALL_API(
      ENDPOINTS.GetFaq,
      null,
      "POST",
      nup,
      navigation
    );

    setData(response.finalResponse);
    setcountArray(response.finalResponse.length);
    setLoading(false);
  };

  const collapseFaq = (id) => {
    if (temp.includes(id)) {
      settemp(temp.filter((item) => item !== id));
    } else {
      settemp([...temp, id]);
    }
  };

  return (
    <View style={styles.container}>
      <LinearGradient style={styles.header} colors={COLORS.gradient}>
        <View style={styles.contentHeader}>
          <Text style={styles.textHeader}>FAQs</Text>
        </View>
      </LinearGradient>

      {/* <CustomText style={styles.textFaq}>FAQ</CustomText> */}

      <FlatList
        data={data}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => {
          return (
            <View
              style={[
                styles.seperatorAnswer,
                {
                  marginBottom: index == countArray - 1 ? 10 : 0,
                },
              ]}
              key={index}
            >
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.headerQuestion}
                onPress={() => collapseFaq(item.id)}
              >
                <View style={styles.viewPointDesc}>
                  <View style={styles.point} />
                  <CustomText
                    style={{ marginHorizontal: 10, color: "black" }}
                    regular
                  >
                    {item.pertanyaan}
                  </CustomText>
                </View>
                <View style={styles.arrowIcon}>
                  <MaterialIcons
                    name={
                      temp.includes(item.id)
                        ? "keyboard-arrow-up"
                        : "keyboard-arrow-down"
                    }
                    size={24}
                    color="black"
                  />
                </View>
              </TouchableOpacity>
              {temp.includes(item.id) && (
                <View style={styles.answerView}>
                  <CustomText regular style={styles.answer}>
                    {item.jawaban}
                  </CustomText>
                </View>
              )}
            </View>
          );
        }}
      />
    </View>
  );
};

export default Faq;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    // marginTop: Platform.OS === 'ios' ? getStatusBarHeight() : 0,
    // height: 50,
    justifyContent: "center",
    paddingHorizontal: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: "grey",
    paddingTop: getStatusBarHeight(),
    paddingBottom: 15,
  },
  contentHeader: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  textHeader: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
  },
  textFaq: {
    marginVertical: 10,
    marginHorizontal: 10,
  },
  headerQuestion: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
    padding: 10,
  },
  answerView: {
    marginHorizontal: 30,
    marginVertical: 10,
  },
  answer: {
    color: "#a3a3a3",
  },
  seperatorAnswer: {
    borderBottomWidth: 0.5,
  },
  viewPointDesc: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1.7,
  },
  point: {
    width: 5,
    height: 5,
    borderRadius: 5 / 2,
    borderWidth: 1,
    backgroundColor: "black",
  },
  arrowIcon: {
    flex: 0.3,
    justifyContent: "center",
    alignItems: "center",
  },
});
