import { LinearGradient } from "expo-linear-gradient";
import AnimatedLottieView from "lottie-react-native";
import React, { useEffect } from "react";
import {
  BackHandler,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { COLORS } from "../../services";

const ComingSoon = ({ navigation }) => {
  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);
  return (
    <View style={styles.container}>
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <AnimatedLottieView
          source={require("../../assets/lottie/coming-soon.json")}
          loop
          autoPlay
          style={{ width: 250, height: 250 }}
        />

        <Text style={{ fontSize: 30, fontWeight: "bold" }}>Coming soon!</Text>
        <TouchableOpacity
          style={{ backgroundColor: "red", marginTop: 15, borderRadius: 5 }}
          onPress={() => backPressed()}
        >
          <LinearGradient
            colors={COLORS.gradient}
            style={{ padding: 10, borderRadius: 5 }}
          >
            <Text style={{ color: "white" }}>back</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default ComingSoon;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
