import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CustomText, CustomTextInput } from "../../../components";
import { COLORS } from "../../../services";
import { Entypo } from "@expo/vector-icons";
import OptionsMenu from "react-native-options-menu";

const Overtime = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        {/* view header card */}
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: 5
          }}
        >
          <CustomText style={{ fontSize: 16, color: COLORS.boldText }}>
            {"{ Nama Lembur }"}
          </CustomText>
          <OptionsMenu
            customButton={
              <Entypo name="dots-three-horizontal" size={24} color="black" />
            }
            destructiveIndex={1}
            options={["View", "Cancel"]}
            actions={[
              () => navigation.navigate("Detail Lembur"),
              () => alert("Cancel")
            ]}
          />
        </View>

        <View style={{ flexDirection: "row" }}>
          <CustomText regular style={[styles.cardText, { width: "25%" }]}>
            Tgl Lembur
          </CustomText>
          <CustomText regular style={[styles.cardText, { width: "3%" }]}>
            :
          </CustomText>
          <CustomText regular style={styles.cardText}>
            (tgl_lembur)
          </CustomText>
        </View>

        <View style={{ flexDirection: "row" }}>
          <CustomText regular style={[styles.cardText, { width: "25%" }]}>
            No. Lembur
          </CustomText>
          <CustomText regular style={[styles.cardText, { width: "3%" }]}>
            :
          </CustomText>
          <CustomText regular style={styles.cardText}>
            (no_lembur)
          </CustomText>
        </View>

        <View style={{ flexDirection: "row" }}>
          <CustomText regular style={[styles.cardText, { width: "25%" }]}>
            Permintaan Oleh
          </CustomText>
          <CustomText regular style={[styles.cardText, { width: "3%" }]}>
            :
          </CustomText>
          <CustomText regular style={styles.cardText}>
            (NUP) - (Nama Pegawai)
          </CustomText>
        </View>

        <View style={{ flexDirection: "row" }}>
          <CustomText regular style={[styles.cardText, { width: "25%" }]}>
            Status
          </CustomText>
          <CustomText regular style={[styles.cardText, { width: "3%" }]}>
            :
          </CustomText>
          <View style={styles.viewStatus}>
            <CustomText style={styles.done}>Sudah Dilaksanakan</CustomText>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Overtime;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FAFAFA"
  },
  card: {
    backgroundColor: "#fff",
    marginHorizontal: 20,
    marginTop: 20,
    marginBottom: 10,
    padding: 17,
    // height: 200,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5
  },
  cardText: {
    fontSize: 12,
    marginBottom: 5,
    color: COLORS.boldText
  },
  viewStatus: {
    marginHorizontal: 5,
    backgroundColor: "#1AA87C",
    borderRadius: 9,
    paddingVertical: 4,
    paddingHorizontal: 20
  },
  done: {
    fontSize: 8,
    color: "#fff"
  },
  being_done: {
    marginTop: 3,
    fontSize: 8,
    marginHorizontal: 5,
    backgroundColor: "#BF1A1A",
    color: "#fff",
    borderRadius: 9,
    paddingVertical: 4,
    paddingHorizontal: 21
  },
  not_done: {
    marginTop: 3,
    fontSize: 8,
    marginHorizontal: 5,
    backgroundColor: "#ECB143",
    color: "#fff",
    borderRadius: 9,
    paddingVertical: 4,
    paddingHorizontal: 22
  }
});
