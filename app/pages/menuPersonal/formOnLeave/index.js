import React, { useCallback, useRef, useState } from "react";
import {
  Keyboard,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { AntDesign, FontAwesome } from "@expo/vector-icons";
import { BottomSheetModal, BottomSheetFlatList } from "@gorhom/bottom-sheet";
import DateTimePicker from "@react-native-community/datetimepicker";

import {
  ButtomSheetModal,
  CustomText,
  CustomTextInput
} from "../../../components";
import {
  COLORS,
  DUMMY_FLATLIST,
  JENIS_IZIN,
  LAST_THREE_YEARS,
  LOKASI_PELAKSANAAN
} from "../../../services";
import moment from "moment";

const FormOnLeave = () => {
  const [tempDate, settempDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [show, setShow] = useState(false);
  const [type, setType] = useState("");
  const [dataList, setDataList] = useState([]);
  const [trigger, setTrigger] = useState(null);
  const [renderClick, setRenderClick] = useState(null);
  const [titleModal, setTitleModal] = useState(null);
  const [year, setYear] = useState(null);
  const [jenisSelected, setJenisSelected] = useState("");
  const [date, setDate] = useState(new Date());
  const [location, setLocation] = useState(null);
  const [reason, setReason] = useState(null);
  const [plt, setPlt] = useState(null);
  const [approve, setApprove] = useState(null);

  const bottomSheetListRef = useRef(null);

  const handlePresentModal = (e) => {
    bottomSheetListRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  const setShowing = () => {
    if (Platform.OS === "ios") {
      setType("date");
      handlePresentModal();
    } else {
      setShow(true);
    }
  };

  const renderItem = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setYear(item.name);
          bottomSheetListRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const renderItemLokasi = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setLocation(item.name);
          bottomSheetListRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const renderItemEmployee = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setPlt(item.nama);
          bottomSheetListRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.nama}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const renderItemEmployeePC = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setApprove(item.nama);
          bottomSheetListRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.nama}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <ScrollView>
        <View style={[styles.container, { opacity: open ? 0.3 : null }]}>
          <View style={{ marginVertical: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Pilih Tahun Cuti
            </CustomText>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.card}
              onPress={() => {
                setTrigger(0);
                setType("list");
                setRenderClick("TC");
                setTitleModal("Pilih Tahun Cuti");
                setDataList(LAST_THREE_YEARS());
                handlePresentModal();
              }}
            >
              <CustomText regular style={{ color: year ? "black" : "grey" }}>
                {year ? year : "-- Pilih Tahun Cuti --"}
              </CustomText>
              <AntDesign
                name="caretdown"
                size={10}
                color="black"
                style={{
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ marginBottom: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Jenis Cuti
            </CustomText>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.card}
              onPress={() => {
                setTrigger(1);
                setType("list");
                setDataList(JENIS_IZIN);
                handlePresentModal();
                setTitleModal("Pilih Jenis Cuti");
              }}
            >
              <CustomText
                regular
                style={{ color: jenisSelected ? "black" : "grey" }}
              >
                {jenisSelected ? jenisSelected : "-- Pilih Jenis Cuti --"}
              </CustomText>
              <AntDesign
                name="caretdown"
                size={10}
                color="black"
                style={{
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ marginBottom: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Tanggal Pelaksanaan
            </CustomText>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.card}
              onPress={() => {
                setShowing();
                setTitleModal("Pilih Tanggal Cuti");
              }}
            >
              <CustomText
                regular
                style={{ color: jenisSelected ? "black" : "grey" }}
              >
                {moment(date).format("DD MMMM YYYY")}
              </CustomText>
              <FontAwesome
                name="calendar"
                size={18}
                color="black"
                style={{
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ marginBottom: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Lokasi Pelaksanaan
            </CustomText>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.card}
              onPress={() => {
                setTrigger(0);
                setType("list");
                setRenderClick("LP");
                setTitleModal("Pilih Lokasi Pelaksanaan");
                setDataList(LOKASI_PELAKSANAAN);
                handlePresentModal();
              }}
            >
              <CustomText
                regular
                style={{ color: location ? "black" : "grey" }}
              >
                {location ? location : "-- Pilih Lokasi Pelaksanaan --"}
              </CustomText>
              <AntDesign
                name="caretdown"
                size={10}
                color="black"
                style={{
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ marginBottom: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Alasan
            </CustomText>
            <CustomTextInput
              editable={true}
              maxLength={200}
              multiline
              numberOfLines={6}
              onChangeText={(text) => setReason(text)}
              value={reason}
              placeholder={"Alasan cuti..."}
              textAlignVertical={"top"}
              keyboardType={"done"}
              style={[styles.card, { height: 50 }]}
            />
          </View>

          <View style={{ marginBottom: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Pelaksana Tugas Harian (PLT)
            </CustomText>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.card}
              onPress={() => {
                setTrigger(0);
                setType("list");
                setRenderClick("PLT");
                setTitleModal("Pilih Pelaksanaan Tugas Harian");
                setDataList(DUMMY_FLATLIST);
                handlePresentModal();
              }}
            >
              <CustomText regular style={{ color: plt ? "black" : "grey" }}>
                {plt ? plt : "-- Pilih Pelaksana Tugas Harian --"}
              </CustomText>
              <AntDesign
                name="caretdown"
                size={10}
                color="black"
                style={{
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>

          <View style={{ marginBottom: 20 }}>
            <CustomText regular style={{ fontSize: 15 }}>
              Persetujuan Cuti
            </CustomText>
            <TouchableOpacity
              activeOpacity={0.7}
              style={styles.card}
              onPress={() => {
                setTrigger(0);
                setType("list");
                setRenderClick("PC");
                setTitleModal("Pilih Persetujuan Cuti");
                setDataList(DUMMY_FLATLIST);
                handlePresentModal();
              }}
            >
              <CustomText regular style={{ color: approve ? "black" : "grey" }}>
                {approve ? approve : "-- Pilih Persetujuan Cuti --"}
              </CustomText>
              <AntDesign
                name="caretdown"
                size={10}
                color="black"
                style={{
                  justifyContent: "center",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </View>

          {jenisSelected && reason &&  (
            <TouchableOpacity style={styles.btnSubmit}>
              <CustomText style={{ color: "white", fontSize: 15 }}>
                Submit
              </CustomText>
            </TouchableOpacity>
          )}

          {show && (
            <DateTimePicker
              value={date}
              mode="date"
              is24Hour={true}
              display="default"
              onChange={(event, selectedDate) => {
                const currentDate = selectedDate || date;
                setDate(currentDate);
                setShow(false);
              }}
            />
          )}

          <ButtomSheetModal
            bottomSheetRef={bottomSheetListRef}
            title={titleModal}
            data={dataList}
            onDismiss={() => setOpen(false)}
            onPressClose={() => {
              bottomSheetListRef.current?.close();
              setOpen(false);
              setType("");
            }}
            onPressRender={(item) => {
              setJenisSelected(item);
              bottomSheetListRef.current?.close();
            }}
            date={date}
            type={type}
            onChangeDate={(event, selectedDate) => {
              const currentDate = selectedDate || date;
              settempDate(currentDate);
            }}
            onPressOk={() => {
              setDate(tempDate);
              bottomSheetListRef.current?.close();
            }}
            trigger={trigger}
            renderItems={
              renderClick === "TC"
                ? renderItem
                : renderClick === "LP"
                ? renderItemLokasi
                : renderClick === "PLT"
                ? renderItemEmployee
                : renderItemEmployeePC
            }
          />
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
};

export default FormOnLeave;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingHorizontal: 20
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee"
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20
  },
  uploadIconView: {
    height: 100,
    width: 100,
    borderWidth: 0.2,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderColor: "grey"
  },

  uploadIconViews: {
    height: 150,
    width: 200,
    borderWidth: 0.5,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10
  },
  btnSubmit: {
    backgroundColor: COLORS.blue,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20
  }
});
