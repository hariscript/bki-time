import { earthRadius } from "geolib";
import React, { useState } from "react";
import { Alert, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CustomText } from "../../../components";
import { COLORS } from "../../../services";
import { Entypo, Ionicons } from "@expo/vector-icons";
import { FlatList } from "react-native";
import OptionsMenu from "react-native-options-menu";

const OnLeave = ({ navigation }) => {
  const [data, setData] = useState([
    {
      id: 1,
      nama: "rafsan"
    },
    {
      id: 2,
      nama: "riko noob"
    },
    {
      id: 3,
      nama: "haris noob"
    }
  ]);

  const onFabPress = () => {
    navigation.navigate("Form Cuti");
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={data}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => {
          return (
            <View style={[styles.card, index === 0 && { marginTop: 20 }]}>
              {/* view header card */}
              <View style={styles.cardHeader}>
                <CustomText style={{ fontSize: 16, color: COLORS.boldText }}>
                  {"{Jenis Cuti} - {Tahun Cuti}"}
                </CustomText>

                <OptionsMenu
                  customButton={
                    <Entypo
                      name="dots-three-horizontal"
                      size={24}
                      color="black"
                    />
                  }
                  destructiveIndex={1}
                  options={["Edit", "View", "Cancel"]}
                  actions={[
                    () => navigation.navigate("Form Cuti"),
                    () => alert("Delete"),
                    () => alert("Cancel")
                  ]}
                />
              </View>
              <View style={{ flexDirection: "row" }}>
                <CustomText regular style={[styles.cardText, { width: "23%" }]}>
                  No. Cuti
                </CustomText>
                <CustomText regular style={[styles.cardText, { width: "3%" }]}>
                  :
                </CustomText>
                <CustomText regular style={styles.cardText}>
                  {"{no_cuti}"}
                </CustomText>
              </View>

              <View style={{ flexDirection: "row" }}>
                <CustomText regular style={[styles.cardText, { width: "23%" }]}>
                  PLT
                </CustomText>
                <CustomText regular style={[styles.cardText, { width: "3%" }]}>
                  :
                </CustomText>
                <CustomText regular style={styles.cardText}>
                  {item.nama}
                </CustomText>
              </View>

              <View style={{ flexDirection: "row" }}>
                <CustomText regular style={[styles.cardText, { width: "23%" }]}>
                  Jumlah Hari
                </CustomText>
                <CustomText regular style={[styles.cardText, { width: "3%" }]}>
                  :
                </CustomText>
                <CustomText regular style={styles.cardText}>
                  {"{total_cuti}"}
                </CustomText>
              </View>

              <View style={{ flexDirection: "row" }}>
                <CustomText regular style={[styles.cardText, { width: "23%" }]}>
                  Lokasi Cuti
                </CustomText>
                <CustomText regular style={[styles.cardText, { width: "3%" }]}>
                  :
                </CustomText>
                <CustomText regular style={styles.cardText}>
                  Luar Kota / Dalam Kota
                </CustomText>
              </View>

              <View style={{ flexDirection: "row" }}>
                <CustomText regular style={[styles.cardText, { width: "23%" }]}>
                  Alasan Cuti
                </CustomText>
                <CustomText regular style={[styles.cardText, { width: "3%" }]}>
                  :
                </CustomText>
                <CustomText regular style={styles.cardText}>
                  {"{alasan_cuti}"}
                </CustomText>
              </View>

              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <CustomText regular style={[styles.cardText, { width: "23%" }]}>
                  Status
                </CustomText>
                <CustomText regular style={[styles.cardText, { width: "3%" }]}>
                  :
                </CustomText>
                <View
                  style={[
                    styles.contentStatus,
                    {
                      backgroundColor:
                        index === 0
                          ? "#1AA87C"
                          : index === 1
                          ? "#BF1A1A"
                          : "#ECB143"
                    }
                  ]}
                >
                  <CustomText style={styles.status}>
                    {index === 0
                      ? "Disetujui"
                      : index === 1
                      ? "Tidak Disetujui"
                      : "Menunggu Persetujuan"}
                  </CustomText>
                </View>
              </View>
            </View>
          );
        }}
      />

      <TouchableOpacity
        style={styles.fab}
        onPress={() => {
          onFabPress();
        }}
      >
        {/* <View style={styles.contentFab}> */}
        <Ionicons
          name="add"
          size={40}
          color="white"
          style={{ marginLeft: 2 }}
        />
        {/* </View> */}
      </TouchableOpacity>
    </View>
  );
};

export default OnLeave;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FAFAFA"
  },
  section: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#fff",
    padding: 20,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    elevation: 4,
    marginBottom: 10
  },
  button: {
    backgroundColor: COLORS.lightBlue,
    paddingVertical: 8,
    paddingHorizontal: 15,
    borderRadius: 11
  },
  card: {
    backgroundColor: "#fff",
    marginHorizontal: 10,
    marginBottom: 15,
    padding: 17,
    // height: 200,
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5
  },
  cardHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5
  },
  cardText: {
    fontSize: 12,
    marginBottom: 5,
    color: COLORS.boldText
  },
  status: {
    fontSize: 7,
    marginHorizontal: 5,
    color: "#fff"
  },
  contentStatus: {
    borderRadius: 9,
    paddingVertical: 5,
    paddingHorizontal: 15,
    marginHorizontal: 5
  },
  fab: {
    width: 60,
    height: 60,
    borderRadius: 40,
    backgroundColor: "#055DB3",
    position: "absolute",
    bottom: 100,
    right: 20,
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
    justifyContent: "center",
    alignItems: "center"
  },
  contentFab: {}
});
