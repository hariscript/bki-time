import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CustomText, CustomTextInput } from "../../../components";
import { COLORS } from "../../../services";
import { Entypo } from "@expo/vector-icons";
import { FlatList } from "react-native";

const DetailOvertime = ({ navigation }) => {
  const [data, setData] = useState([
    {
      id: 1
    },
    {
      id: 2
    },
    {
      id: 3
    },
    {
      id: 4
    }
  ]);
  return (
    <View style={styles.container}>
      <CustomText
        style={{
          marginTop: 20,
          marginLeft: 20,
          fontSize: 20,
          color: COLORS.boldText,
          marginBottom: 20
        }}
      >
        Detail Lembur (nama_lembur)
      </CustomText>

      {/* FlatList */}
      <FlatList
        data={data}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => {
          return (
            <View style={styles.card}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginBottom: 5
                }}
              >
                <CustomText style={{ fontSize: 18, color: COLORS.boldText }}>
                  (Nama Lembur)-(tgl_lembur)
                </CustomText>
                <Entypo name="dots-three-horizontal" size={15} color="black" />
              </View>

              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <CustomText regular>Status : </CustomText>
                <CustomText style={styles.done}>Sudah dilaksanakan</CustomText>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

export default DetailOvertime;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  },
  card: {
    backgroundColor: "white",
    // borderWidth: 1,
    marginHorizontal: 20,
    marginBottom: 15,
    padding: 15,
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5
  },
  done: {
    color: "white",
    backgroundColor: COLORS.greenCurrency,
    paddingVertical: 3,
    paddingHorizontal: 15,
    fontSize: 8,
    borderRadius: 10
  },
  being_done: {
    color: "white",
    backgroundColor: "#BF1A1A",
    paddingVertical: 3,
    paddingHorizontal: 15,
    fontSize: 8,
    borderRadius: 10
  },
  not_done: {
    color: "white",
    backgroundColor: "#ECB143",
    paddingVertical: 3,
    paddingHorizontal: 15,
    fontSize: 8,
    borderRadius: 10
  }
});
