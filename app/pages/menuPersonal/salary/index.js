import React, { useCallback, useRef, useState } from "react";
import { ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { AntDesign, Ionicons, Entypo } from "@expo/vector-icons";

import { ButtomSheetModal, CustomText } from "../../../components";
import { useEffect } from "react";
import { COLORS, MONTHS } from "../../../services";
import { BottomSheetFlatList, BottomSheetModal } from "@gorhom/bottom-sheet";

const Salary = () => {
  const [month, setMonth] = useState("");
  const [year, setYear] = useState("");
  const [open, setOpen] = useState(false);
  const [type, setType] = useState("");
  const [YEAR, setYEAR] = useState([]);
  const [trigger, setTrigger] = useState(null);

  const bottomSheetListRef = useRef(null);
  const bottomSheetRef = useRef(null);
  const snapPoints = ["88%"];

  useEffect(() => {
    getLastThreeYears();
    return () => {};
  }, []);

  const getLastThreeYears = () => {
    const currentYear = new Date().getFullYear();
    const years = Array.from({ length: 3 }, (v, i) => currentYear - i);
    setYEAR(years);
  };

  const handlePresentModal = () => {
    bottomSheetListRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  const renderItem = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          setMonth(item.name);
          bottomSheetListRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const handlePresentModalSubmit = () => {
    bottomSheetRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  return (
    <View style={styles.container}>
      <View style={{ padding: 20, backgroundColor: "white" }}>
        <CustomText style={{ fontSize: 15, color: COLORS.boldText }}>
          Pilih Periode Gaji
        </CustomText>
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[styles.card, { marginRight: 5 }]}
            onPress={() => {
              setTrigger(0);
              setType("month");
              handlePresentModal();
            }}
          >
            <CustomText regular style={{ color: month ? "black" : "grey" }}>
              {month ? month : "--Pilih Bulan-- "}
            </CustomText>
            <AntDesign
              name="caretdown"
              size={10}
              color="black"
              style={{
                justifyContent: "center",
                alignSelf: "center"
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={0.7}
            style={[styles.card, { marginLeft: 5 }]}
            onPress={() => {
              setTrigger(1);
              setType("year");
              handlePresentModal();
            }}
          >
            <CustomText regular style={{ color: year ? "black" : "grey" }}>
              {year ? year : "--Pilih Tahun-- "}
            </CustomText>
            <AntDesign
              name="caretdown"
              size={10}
              color="black"
              style={{
                justifyContent: "center",
                alignSelf: "center"
              }}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={styles.btnSubmit}
          onPress={handlePresentModalSubmit}
        >
          <CustomText style={{ color: "white", fontSize: 15 }}>
            Submit
          </CustomText>
        </TouchableOpacity>
      </View>

      <ButtomSheetModal
        bottomSheetRef={bottomSheetListRef}
        title={type === "month" ? "Pilih Bulan" : "Pilih Tahun"}
        data={type === "month" ? MONTHS : YEAR}
        onDismiss={() => setOpen(false)}
        onPressClose={() => {
          bottomSheetListRef.current?.close();
          setOpen(false);
          setType("");
        }}
        type={"list"}
        renderItems={type === "month" ? renderItem : null}
        onPressRender={(item) => {
          setYear(item);
          bottomSheetListRef.current?.close();
        }}
        trigger={trigger}
      />

      <BottomSheetModal
        ref={bottomSheetRef}
        snapPoints={snapPoints}
        backgroundStyle={styles.bottomSheetBackground}
        onDismiss={() => setOpen(false)}
      >
        <View style={{ paddingHorizontal: 15 }}>
          <ScrollView style={{ marginHorizontal: 20, marginTop: 10 }}>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between"
              }}
            >
              <CustomText style={{ fontSize: 15, color: COLORS.boldText }}>
                Gaji Bersih
              </CustomText>
              <View
                style={{
                  paddingVertical: 5,
                  paddingHorizontal: 8,
                  backgroundColor: "#1AA87C",
                  borderRadius: 18,
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Ionicons name="ios-print-outline" size={15} color="white" />
                <CustomText
                  style={{ color: "white", fontSize: 9, marginLeft: 5 }}
                >
                  Cetak Slip gaji
                </CustomText>
              </View>
            </View>

            <View style={styles.sparator} />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Gaji Bersih 1
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Gaji Bersih 2
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Gaji Bersih 3
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>

            <View style={{ marginTop: 15 }}>
              <CustomText style={{ fontSize: 15, color: COLORS.boldText }}>
                Penghasilan Lain
              </CustomText>
            </View>

            <View style={styles.sparator} />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Penghasilan Lain 1
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Penghasilan Lain 2
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>

            {/* Potongan */}
            <View style={{ marginTop: 15 }}>
              <CustomText style={{ fontSize: 15, color: COLORS.boldText }}>
                Potongan
              </CustomText>
            </View>

            <View style={styles.sparator} />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Potongan 1
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Potongan 2
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.boldText, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>

            <View style={styles.sparator} />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 10
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.greenCurrency, fontSize: 12 }}
              >
                Total Penghasilan {"\n"}
                (Gaji Bersih + Penghasilan Lain)
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.greenCurrency, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText
                regular
                style={{ color: COLORS.danger, fontSize: 12 }}
              >
                Total Potongan
              </CustomText>
              <CustomText
                regular
                style={{ color: COLORS.danger, fontSize: 12 }}
              >
                Rp. 10.000.000
              </CustomText>
            </View>

            <View style={styles.sparator} />

            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginBottom: 5
              }}
            >
              <CustomText style={{ color: COLORS.blue, fontSize: 12 }}>
                Total NET
              </CustomText>
              <CustomText style={{ color: COLORS.blue, fontSize: 12 }}>
                Rp. 10.000.000
              </CustomText>
            </View>
          </ScrollView>
        </View>
      </BottomSheetModal>
    </View>
  );
};

export default Salary;
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20
  },
  btnSubmit: {
    backgroundColor: COLORS.blue,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 20
  },
  sparator: {
    borderWidth: 1,
    // height: 10,
    borderColor: "#ECEFF1",
    marginVertical: 10
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    // marginBottom: 15,
    alignItems: "center"
  }
});
