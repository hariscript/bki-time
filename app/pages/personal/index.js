import React, { useEffect } from "react";
import { BackHandler, Dimensions, StyleSheet, View } from "react-native";
import { ButtonMenu, CustomText } from "../../components";
import { MENU_PERSONAL } from "../../services";

const Personal = ({ navigation }) => {
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const renderItems = () => {
    const items = [];

    for (let i = 0; i < MENU_PERSONAL.length; i += 4) {
      const rowItems = MENU_PERSONAL.slice(i, i + 4);
      items.push(
        <View key={`row_${i}`} style={{ flexDirection: "row" }}>
          {rowItems.map((item, index) => (
            <View key={index}>
              <ButtonMenu
                onPress={() => navigation.navigate(item.navigation)}
                text={item.nama}
                style={{
                  width: Dimensions.get("window").width / 4,
                  textAlign: "center",
                  marginBottom: 10
                }}
                styleText={{ textAlign: "center" }}
                gambar={item.gambar}
              />
            </View>
          ))}
        </View>
      );
    }

    return items;
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ backgroundColor: "white", paddingVertical: 20 }}>
        <View>{renderItems()}</View>
      </View>
    </View>
  );
};

export default Personal;

const styles = StyleSheet.create({
  btnMenu: {
    // width: Dimensions.get("window").width,
    flexDirection: "row",
    justifyContent: "space-between"
  }
});
