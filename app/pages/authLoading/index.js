import React from "react";
import { ActivityIndicator } from "react-native";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";
import * as SQLite from "expo-sqlite";
import ScreenContainer from "../screenContainer";
import * as Font from "expo-font";
import AsyncStorage from "@react-native-async-storage/async-storage";

const db = SQLite.openDatabase("keys.db");

const AuthLoading = ({ navigation }) => {
  const [fontLoaded, setFontLoaded] = React.useState(false);
  const { isLogged } = useSelector((state) => state.LoginReducer);

  useFocusEffect(
    React.useCallback(() => {
      isTableExists()
        .then((exists) => {
          if (exists) {
            console.log("Tabel tokens sudah ada dalam database.");
          } else {
            console.log("Tabel tokens belum ada dalam database.");

            createTableSQLite();
          }
        })
        .catch((error) => {
          console.error("Terjadi kesalahan saat memeriksa tabel:", error);
        });
      loadFont();
      // console.log("check login => ", isLogged);
      getAsyncStorage();

      return () => {};
    }, [])
  );

  const loadFont = async () => {
    try {
      await Font.loadAsync({
        "Montserrat-Regular": require("../../assets/fonts/Montserrat-Regular.ttf"),
        "Montserrat-Bold": require("../../assets/fonts/Montserrat-Bold.ttf")
      });
      setFontLoaded(true);
    } catch (error) {
      console.error("Error loading fonts:", error);
    }
  };

  const getAsyncStorage = async () => {
    const isLogin = await AsyncStorage.getItem("isLogin");

    navigateTo(isLogin);
  };

  const navigateTo = (isLogin) => {
    setTimeout(() => {
      isLogged || isLogin == "true"
        ? navigation.replace("bottomTab")
        : navigation.replace("WelcomeAuth");
    }, 2000);
  };

  const createTableSQLite = () => {
    db.transaction((tx) => {
      tx.executeSql(
        "create table if not exists token (id integer primary key not null, nup int, keys text);",
        [],
        (sqlTxn = SQLite.SQLTransaction, res = SQLite.SQLResultSet) => {
          console.log("table created successfully");
        },
        (error) => {
          console.log("error on creating table " + error.message);
        }
      );
    });
  };

  const isTableExists = () => {
    return new Promise((resolve, reject) => {
      db.transaction((tx) => {
        // Cek apakah tabel 'tokens' sudah ada dalam database.
        tx.executeSql(
          "SELECT name FROM sqlite_master WHERE type='table' AND name='token';",
          [],
          (_, result) => {
            if (result.rows.length > 0) {
              // Tabel 'tokens' sudah ada dalam database.
              resolve(true);
            } else {
              // Tabel 'tokens' belum ada dalam database.
              resolve(false);
            }
          },
          (_, error) => {
            // Terjadi kesalahan saat mengeksekusi perintah SQL.
            reject(error);
          }
        );
      });
    });
  };

  return (
    <ScreenContainer>
      <ActivityIndicator
        color={"blue"}
        size={"large"}
        style={{
          position: "absolute",
          justifyContent: "center",
          alignItems: "center"
        }}
      />
    </ScreenContainer>
  );
};

export default AuthLoading;
