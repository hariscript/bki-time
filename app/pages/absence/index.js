import React, { useEffect, useState } from "react";
import { Alert, BackHandler, Dimensions, StyleSheet, View } from "react-native";
import {
  ButtonMenu,
  CustomText,
  ModalUnderConstraction,
} from "../../components";
import { MENU_ABSENCE } from "../../services";
import { useSelector } from "react-redux";
import moment from "moment";

const Absence = ({ navigation }) => {
  const { clock_in, clock_out } = useSelector((state) => state.ProfileReducer);
  const [modalUC, setModalUC] = useState(false);

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const renderItems = () => {
    const items = [];

    for (let i = 0; i < MENU_ABSENCE.length; i += 4) {
      const rowItems = MENU_ABSENCE.slice(i, i + 4);
      items.push(
        <View key={`row_${i}`} style={{ flexDirection: "row" }}>
          {rowItems.map((item, index) => (
            <View key={index}>
              <ButtonMenu
                onPress={() => {
                  if (
                    clock_in &&
                    moment(clock_in).format("DD MMMM YYYY") ==
                      moment(new Date()).format("DD MMMM YYYY") &&
                    item.nama === "Clock-In"
                  ) {
                    Alert.alert(
                      "Info",
                      "Anda sudah absen Clock In hari ini, apakah Anda ingin mengulang?",
                      [
                        {
                          text: "Cancel",
                          onPress: () => console.log("Cancel Pressed"),
                          style: "cancel",
                        },
                        {
                          text: "OK",
                          onPress: () => navigation.navigate(item.navigation, { from: null }),
                        },
                      ],
                      {
                        cancelable: false,
                      }
                    );
                  } else if (
                    clock_out &&
                    moment(clock_out).format("DD MMMM YYYY") ==
                      moment(new Date()).format("DD MMMM YYYY") &&
                    item.nama === "Clock-Out"
                  ) {
                    Alert.alert(
                      "Info",
                      "Anda sudah absen Clock Out hari ini, apakah Anda ingin mengulang?",
                      [
                        {
                          text: "Cancel",
                          onPress: () => console.log("Cancel Pressed"),
                          style: "cancel",
                        },
                        {
                          text: "OK",
                          onPress: () => navigation.navigate(item.navigation, { from: null }),
                        },
                      ],
                      {
                        cancelable: false,
                      }
                    );
                  } else if (item.nama == "Permission") {
                    setModalUC(true);
                  } else {
                    navigation.navigate(item.navigation, { from: null });
                  }
                }}
                text={item.nama}
                style={{
                  width: Dimensions.get("window").width / 4,
                  textAlign: "center",
                  marginBottom: 10,
                }}
                styleText={{ textAlign: "center" }}
                gambar={item.gambar}
              />
            </View>
          ))}
        </View>
      );
    }

    return items;
  };

  return (
    <View style={{ flex: 1 }}>
      <ModalUnderConstraction
        visible={modalUC}
        type={"failed"}
        text={"New feature still in progress!"}
        onRequestClose={() => {
          setModalUC(!modalUC);
        }}
        onPress={() => {
          setModalUC(false);
        }}
        onPressExit={() => {
          setModalUC(false);
        }}
      />
      <View style={{ backgroundColor: "white", paddingVertical: 20 }}>
        <View>{renderItems()}</View>
      </View>
    </View>
  );
};

export default Absence;

const styles = StyleSheet.create({
  btnMenu: {
    // width: Dimensions.get("window").width,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
