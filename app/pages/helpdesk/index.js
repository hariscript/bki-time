import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CustomText, CustomTextInput } from "../../components";
import { COLORS } from "../../services";
import { FlatList } from "react-native";

const Helpdesk = ({ navigation }) => {
  const [data, setData] = useState([
    {
      id: 1
    },
    {
      id: 2
    },
    {
      id: 3
    },
    {
      id: 4
    }
  ]);
  return (
    <View style={styles.container}>
      <View
        style={{
          marginBottom: 10,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          paddingHorizontal: 20,
          paddingVertical: 15,
          backgroundColor: "white"
        }}
      >
        <CustomText
          style={{
            fontSize: 20,
            color: COLORS.boldText
          }}
        >
          Daftar Tiket
        </CustomText>
        <TouchableOpacity
          style={styles.add}
          onPress={() => navigation.navigate("AddHelpdesk")}
        >
          <CustomText
            style={{ textAlign: "center", color: "#fff", fontSize: 15 }}
          >
            + Tambah
          </CustomText>
        </TouchableOpacity>
      </View>

      {/* FlatList */}
      <FlatList
        data={data}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => {
          return (
            <View style={styles.card}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  marginBottom: 5
                }}
              >
                <CustomText style={{ fontSize: 16, color: COLORS.boldText }}>
                  No. Tiket: xxx/ICT/HD/12/22
                </CustomText>
              </View>
              <View>
                <CustomText regular style={{ fontSize: 13, marginBottom: 5 }}>
                  Deskripsi dari permintaan yang telah diajukan
                </CustomText>
                <CustomText regular style={{ fontSize: 13, marginBottom: 5 }}>
                  Line 2
                </CustomText>
              </View>
              <View
                style={{
                  marginTop: 5,
                  height: 2,
                  backgroundColor: "#EFEFEF",
                  flexDirection: "row"
                }}
              ></View>
              <View
                style={{
                  alignItems: "center",
                  flexDirection: "row",
                  justifyContent: "space-around"
                }}
              >
                <CustomText
                  style={{
                    color: COLORS.lightBlue,
                    marginTop: 5,
                    fontSize: 11
                  }}
                >
                  Tgl. Permintaan: dd/mm/yyyy | Status:
                </CustomText>
                <CustomText
                  style={{
                    color: COLORS.greenCurrency,
                    marginTop: 5,
                    fontSize: 11
                  }}
                >
                  Open
                </CustomText>
                <TouchableOpacity
                  onPress={() => navigation.navigate("DetailHelpdesk")}
                >
                  <CustomText style={styles.done}>Detail</CustomText>
                </TouchableOpacity>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

export default Helpdesk;

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  card: {
    backgroundColor: "white",
    // borderWidth: 1,
    marginHorizontal: 20,
    marginBottom: 15,
    padding: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10
  },
  done: {
    color: "white",
    backgroundColor: COLORS.lightBlue,
    paddingVertical: 5,
    paddingHorizontal: 18,
    fontSize: 10,
    borderRadius: 12,
    marginTop: 10
  },
  add: {
    color: "white",
    backgroundColor: COLORS.lightBlue,
    paddingVertical: 7,
    paddingHorizontal: 18,
    fontSize: 10,
    borderRadius: 15
  }
  // button: {
  //   backgroundColor: COLORS.lightBlue,
  //   paddingVertical: 8,
  //   paddingHorizontal: 15,
  //   borderRadius: 11,
  // },
});
