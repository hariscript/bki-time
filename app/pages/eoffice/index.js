import React, { useState } from "react";
import { BackHandler, Dimensions, StyleSheet, View } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { useSelector } from "react-redux";

import {
  ButtonMenu,
  ButtonMenuEoffice,
  CustomText,
  LoadingIndicator,
} from "../../components";
import { CALL_API, ENDPOINTS, MENU_EOFFICE } from "../../services";

const Eoffice = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [data, setData] = useState(MENU_EOFFICE);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      GetCountInbox();

      BackHandler.addEventListener("hardwareBackPress", backPressed);
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", backPressed);
      };
    }, [])
  );

  const GetCountInbox = async () => {
    setLoading(true);
    const response = await CALL_API(
      ENDPOINTS.GetCountUnreadsurat,
      null,
      "POST",
      nup,
      navigation
    );

    data[0].count = response.finalResponse.data;
    GetCountTembusan();
  };

  const GetCountTembusan = async () => {
    const response = await CALL_API(
      ENDPOINTS.GetCountUnreadTembusan,
      null,
      "POST",
      nup,
      navigation
    );

    data[1].count = response.finalResponse.data;
    GetCountApproval();
  };

  const GetCountApproval = async () => {
    const response = await CALL_API(
      ENDPOINTS.GetCountApproval,
      null,
      "POST",
      nup,
      navigation
    );

    data[2].count = response.finalResponse.data;
    GetCountDisposisi();
  };

  const GetCountDisposisi = async () => {
    const response = await CALL_API(
      ENDPOINTS.GetCountUnreaddisposisi,
      null,
      "POST",
      nup,
      navigation
    );

    data[4].count = response.finalResponse.data;
    setLoading(false);
  };

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const renderItems = () => {
    const items = [];

    for (let i = 0; i < data.length; i += 4) {
      const rowItems = data.slice(i, i + 4);
      items.push(
        <View key={`row_${i}`} style={{ flexDirection: "row" }}>
          {rowItems.map((item, index) => (
            <View key={index}>
              <ButtonMenuEoffice
                onPress={() => navigation.navigate(item.navigation)}
                text={item.nama}
                style={{
                  width: Dimensions.get("window").width / 4,
                  textAlign: "center",
                  marginBottom: 10,
                }}
                styleText={{ textAlign: "center" }}
                gambar={item.gambar}
                count={item.count}
                loading={loading}
              />
            </View>
          ))}
        </View>
      );
    }

    return items;
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ backgroundColor: "white", paddingVertical: 20 }}>
        <View>{renderItems()}</View>
      </View>
    </View>
  );
};

export default Eoffice;

const styles = StyleSheet.create({
  btnMenu: {
    // width: Dimensions.get("window").width,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
