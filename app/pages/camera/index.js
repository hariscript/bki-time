import React, { useEffect, useRef, useState } from "react";
import {
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  StyleSheet,
  TouchableOpacity,
  View,
  useWindowDimensions,
} from "react-native";
import { Camera } from "expo-camera";
import { MaterialIcons } from "@expo/vector-icons";
import * as ImageManipulator from "expo-image-manipulator";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { useDispatch, useSelector } from "react-redux";
import moments from "moment-timezone";
import moment from "moment";
import * as geolib from "geolib";
import * as Location from "expo-location";
import * as FaceDetector from "expo-face-detector";
import { BlurView } from "expo-blur";

import { CustomText, ModalSuccessFailed } from "../../components";
import {
  CALL_API,
  ENDPOINTS,
  GET_SENTENCE,
  SUCCESS_AFTER_BREAK_SENTENCES,
  SUCCESS_BREAK_SENTENCES,
  SUCCESS_CLOCK_IN_SENTENCES,
  SUCCESS_CLOCK_OUT_SENTENCES,
} from "../../services";
import {
  SET_CLOCK_IN,
  SET_CLOCK_OUT,
  SET_TIME_WORKED,
  SET_TRIGGER_COLOR,
} from "../../redux";
import { CommonActions, StackActions } from "@react-navigation/native";
import axios, * as others from "axios";

const CameraScreen = ({ route, navigation }) => {
  const { nup, clock_in, clock_out, nama } = useSelector(
    (state) => state.ProfileReducer
  );
  const { appID } = useSelector((state) => state.LoginReducer);
  const {
    latitude_center,
    longitude_center,
    longitude1,
    latitude1,
    longitude2,
    latitude2,
    longitude3,
    latitude3,
    longitude4,
    latitude4,
  } = useSelector((state) => state.OfficeReducer);

  const insets = useSafeAreaInsets();
  const camRef = useRef(null);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const [flash, setflash] = useState(Camera.Constants.FlashMode.off);
  const [abbrLocation, setabbrLocation] = useState("");
  const [modalSuccess, setmodalSuccess] = useState(false);
  const [modalFailed, setmodalFailed] = useState(false);
  const [disable, setdisable] = useState(false);
  const [position, setposition] = useState("");
  const [location, setlocation] = useState({
    latitude: 0,
    longitude: 0,
  });
  const [detectFace, setDetectFace] = useState(false);
  const [modalFailedSmile, setModalFailedSmile] = useState(false);
  const [modalNoFace, setModalNoFace] = useState(false);
  const dispatch = useDispatch();
  /* 2. Get the param */

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    const region = moments.tz.guess();
    setabbrLocation(moments.tz(region).zoneAbbr());

    PermissionCamera();
    GetLocationAsync();
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  const PermissionCamera = async () => {
    const { status } = await Camera.requestCameraPermissionsAsync();
    if (status !== "granted") {
      Alert.alert(
        "Camera not detected",
        "You haven't granted permission to use your Camera.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }]
      );
    }
  };

  const goBack = (item, onSelect) => {
    setdisable(false);
    onSelect(item);
    navigation.goBack();
  };

  const GetLocationAsync = async () => {
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      Alert.alert(
        "User location not detected",
        "You haven't granted permission to detect your location.",
        [{ text: "OK", onPress: () => console.log("OK Pressed") }]
      );
    }
    let locations = await Location.getCurrentPositionAsync({
      enableHighAccuracy: true,
    });

    setlocation({
      latitude: locations.coords.latitude,
      longitude: locations.coords.longitude,
    });

    currentPosition(locations.coords.latitude, locations.coords.longitude);
    // _goToMyPosition(location.coords.latitude, location.coords.longitude)
  };

  const currentPosition = (lat, long) => {
    // let homeRadius = geolib.isPointWithinRadius(
    //   { latitude: lat, longitude: long },
    //   { latitude: latitude_rumah, longitude: logitude_rumah },
    //   100
    // );

    let officeRadius = geolib.isPointWithinRadius(
      { latitude: lat, longitude: long },
      { latitude: latitude_center, longitude: longitude_center },
      100
    );

    // let officePolygon = geolib.isPointInPolygon(
    //   { latitude: lat, longitude: long },
    //   [
    //     { latitude: latitude1, longitude: longitude1 },
    //     { latitude: latitude2, longitude: longitude2 },
    //     { latitude: latitude3, longitude: longitude3 },
    //     { latitude: latitude4, longitude: longitude4 },
    //   ]
    // );

    if (officeRadius) {
      setposition("KDK");
    }
    // else if (homeRadius) {
    //   setposition("KDR");
    // } else {
    //   setposition("unknown");
    // }
  };

  const takePicture = async (val) => {
    const { location, types, onSelect } = route.params;

    if (types == "Izin") {
      if (camRef) {
        const options = { quality: 0.5, base64: true };

        try {
          const data = await camRef.current.takePictureAsync({
            quality: 0.5,
            skipProcessing: false,
          });
          const manipulate = await ImageManipulator.manipulateAsync(
            data.uri,
            [{ resize: { width: 300, height: 400 } }],
            { format: ImageManipulator.SaveFormat.JPEG }
          );
          goBack(manipulate.uri, onSelect);
        } catch (error) {
          console.log("error");
          console.log(error);
        }
      }
    } else {
      if (camRef) {
        const options = { quality: 0.5, base64: true };

        try {
          if (val) {
            const data = await camRef.current.takePictureAsync({
              quality: 0.5,
              skipProcessing: false,
            });
            const manipulate = await ImageManipulator.manipulateAsync(
              data.uri,
              [{ resize: { width: 300, height: 400 } }],
              { compress: 0.7, format: ImageManipulator.SaveFormat.JPEG }
            );

            let localUri =
              Platform.OS === "android"
                ? manipulate.uri
                : manipulate.uri.replace("file://", "");
            let filename = manipulate.uri.split("/").pop();
            let match = /\.(\w+)$/.exec(filename);
            let type = match ? `image/${match[1]}` : `image`;
            var gambar = {
              uri: localUri,
              name: filename,
              type: type,
            };
            let utc =
              abbrLocation == "WIB" ? 7 : abbrLocation == "WITA" ? 8 : 9;

            // const payloads = new FormData();

            // // payloads.append("key", key);
            // payloads.append("nup_personil", nup);
            // payloads.append("app", appID);
            // payloads.append("latitude", location.latitude);
            // payloads.append("longitude", location.longitude);
            // payloads.append("status_clock", types);
            // payloads.append("status_position", position);
            // payloads.append("gambar", gambar);
            // payloads.append(
            //   "utc",
            //   // abbrLocation == "WIB" ? 7 : abbrLocation == "WITA" ? 8 : 9
            //   utc
            // );

            // const response = await CALL_API(
            //   ENDPOINTS.PostTrx,
            //   payloads,
            //   "UPLOAD",
            //   nup,
            //   navigation
            // );

            // if (response.finalResponse.status == "Succes") {
            //   if (types == 1) {
            //     dispatch(SET_CLOCK_IN(new Date()));
            //     dispatch(SET_TRIGGER_COLOR("black"));
            //   }

            //   if (types == 4) {
            //     dispatch(SET_CLOCK_OUT(new Date()));

            //     if (
            //       moment(clock_in).format("DD MMMM YYYY") ==
            //       moment(new Date()).format("DD MMMM YYYY")
            //     ) {
            //       const now = new Date();
            //       const diffTime = now.getTime() - new Date(clock_in).getTime();
            //       const hoursWorked = Math.floor(diffTime / (1000 * 60 * 60)); // hours
            //       const minutesWorked = Math.floor(
            //         (diffTime % (1000 * 60 * 60)) / (1000 * 60)
            //       ); // minutes
            //       const formattedHours = String(hoursWorked).padStart(2, "0");
            //       const formattedMinutes = String(minutesWorked).padStart(
            //         2,
            //         "0"
            //       );

            //       if (
            //         hoursWorked >= 9 &&
            //         moment(clock_out).format("DD MMMM YYYY") ==
            //           moment(new Date()).format("DD MMMM YYYY")
            //       ) {
            //         dispatch(SET_TRIGGER_COLOR("green"));
            //       } else if (
            //         hoursWorked < 9 &&
            //         moment(clock_out).format("DD MMMM YYYY") ==
            //           moment(new Date()).format("DD MMMM YYYY")
            //       ) {
            //         dispatch(SET_TRIGGER_COLOR("red"));
            //       }

            //       dispatch(
            //         SET_TIME_WORKED(`${formattedHours}:${formattedMinutes}`)
            //       );
            //     }
            //   }

            //   setmodalSuccess(true);
            // } else {
            //   setmodalFailed(true);
            // }

            const fd = new FormData();

            fd.append("nup", nup);
            fd.append("nama", nama);
            fd.append("latitude", location.latitude);
            fd.append("longitude", location.longitude);
            fd.append("server", "10.0.1.77");
            fd.append("database", "db_appsenc");
            fd.append("userDb", "chemonk");
            fd.append("passDb", "T4m1y42904");
            fd.append("status_clock", types);
            fd.append("status_position", "KDK");

            const res = await axios.post(
              "https://api.bki.co.id/api-sikom/config/trx.php",
              fd,
              {
                headers: {
                  "Content-Type": "multipart/form-data",
                },
              }
            );
            // .then((res) => {
            //   console.log(res.data);
            // });

            // console.log(JSON.stringify(res.data));
            if (types == 1) {
              dispatch(SET_CLOCK_IN(new Date()));
              dispatch(SET_TRIGGER_COLOR("black"));
            }

            if (types == 4) {
              dispatch(SET_CLOCK_OUT(new Date()));

              if (
                moment(clock_in).format("DD MMMM YYYY") ==
                moment(new Date()).format("DD MMMM YYYY")
              ) {
                const now = new Date();
                const diffTime = now.getTime() - new Date(clock_in).getTime();
                const hoursWorked = Math.floor(diffTime / (1000 * 60 * 60)); // hours
                const minutesWorked = Math.floor(
                  (diffTime % (1000 * 60 * 60)) / (1000 * 60)
                ); // minutes
                const formattedHours = String(hoursWorked).padStart(2, "0");
                const formattedMinutes = String(minutesWorked).padStart(2, "0");

                if (
                  hoursWorked >= 9 &&
                  moment(clock_out).format("DD MMMM YYYY") ==
                    moment(new Date()).format("DD MMMM YYYY")
                ) {
                  dispatch(SET_TRIGGER_COLOR("green"));
                } else if (
                  hoursWorked < 9 &&
                  moment(clock_out).format("DD MMMM YYYY") ==
                    moment(new Date()).format("DD MMMM YYYY")
                ) {
                  dispatch(SET_TRIGGER_COLOR("red"));
                }

                dispatch(
                  SET_TIME_WORKED(`${formattedHours}:${formattedMinutes}`)
                );
              }
            }
            setmodalSuccess(true);
          } else {
            setModalFailedSmile(true);
          }
          setdisable(false);
        } catch (error) {
          setmodalFailed(true);
          console.log(error);
        }
      }
    }
  };

  const handleFacesDetected = ({ faces }) => {
    try {
      if (faces.length > 0) {
        const smilingProbability = faces[0].smilingProbability;
        if (smilingProbability > 0.7) {
          takePicture(true);
        } else {
          // trigger alert failed no smile
          takePicture(true);
        }
      } else {
        setModalNoFace(true);
      }

      setDetectFace(false);
    } catch (error) {
      console.log("Ada error nih => ", error);
    }
  };

  return (
    <View style={styles.container}>
      <ModalSuccessFailed
        visible={modalSuccess}
        type={"success"}
        text={
          route.params.types == 1
            ? GET_SENTENCE(SUCCESS_CLOCK_IN_SENTENCES)
            : route.params.types == 2
            ? GET_SENTENCE(SUCCESS_BREAK_SENTENCES)
            : route.params.types == 3
            ? GET_SENTENCE(SUCCESS_AFTER_BREAK_SENTENCES)
            : GET_SENTENCE(SUCCESS_CLOCK_OUT_SENTENCES)
        }
        onRequestClose={() => {
          setmodalSuccess(!modalSuccess);
        }}
        onPress={() => {
          setmodalSuccess(false);
          if (route.params.from) {
            navigation.goBack();
          } else {
            navigation.navigate("Absence");
          }
        }}
        onPressExit={() => {
          setmodalSuccess(false);
          if (route.params.from) {
            navigation.goBack();
          } else {
            navigation.navigate("Absence");
          }
        }}
      />

      {/* Modal ketika ada error atau gangguan signal */}
      <ModalSuccessFailed
        visible={modalFailed}
        type={"failed"}
        text={
          "Your Absence couldn't be sent out, check your signal and try again."
        }
        onRequestClose={() => {
          setmodalFailed(!modalFailed);
        }}
        onPress={() => {
          setmodalFailed(false);
          setdisable(false);
        }}
        onPressExit={() => {
          setmodalFailed(false);
          setdisable(false);
        }}
      />

      {/* Modal ketika tidak tersenyum */}
      <ModalSuccessFailed
        visible={modalFailedSmile}
        type={"failed"}
        text={"Oops, sepertinya kamu belum tersenyum. Senyum dulu dong! 😊"}
        onRequestClose={() => {
          setModalFailedSmile(!modalFailedSmile);
        }}
        onPress={() => {
          setModalFailedSmile(false);
          setdisable(false);
        }}
        onPressExit={() => {
          setModalFailedSmile(false);
          setdisable(false);
        }}
      />

      {/* Modal ketika ga ada muka di camera */}
      <ModalSuccessFailed
        visible={modalNoFace}
        type={"failed"}
        text={
          "Uppss, ada yang ngumpet nih. Muncul dong. Aku kan mau liat muka kamu! ☺️"
        }
        onRequestClose={() => {
          setModalNoFace(!modalNoFace);
        }}
        onPress={() => {
          setModalNoFace(!modalNoFace);
          setdisable(false);
        }}
        onPressExit={() => {
          setModalNoFace(!modalNoFace);
          setdisable(false);
        }}
      />

      <Camera
        style={{
          height: Math.round((useWindowDimensions().width * 16) / 9),
          width: "100%",
        }}
        type={route.params.types == "Izin" ? type : Camera.Constants.Type.front}
        ref={camRef}
        flashMode={flash}
        autoFocus={Camera.Constants.AutoFocus.on}
        ratio="16:9"
        focusDepth={1}
        onFacesDetected={
          detectFace && Platform.OS == "android" && handleFacesDetected
        }
        faceDetectorSettings={{
          mode: FaceDetector.FaceDetectorMode.fast,
          detectLandmarks: FaceDetector.FaceDetectorLandmarks.all,
          runClassifications: FaceDetector.FaceDetectorClassifications.all,
          minDetectionInterval: 800,
          tracking: true,
        }}
      >
        <View
          style={[
            styles.btnCameraContainer,
            {
              bottom: insets.bottom + 0,
            },
          ]}
        >
          <View style={{ alignItems: "center" }}>
            <BlurView intensity={90} tint="dark" style={styles.blurContainer}>
              <CustomText style={{ color: "white" }}>
                Senyum ketika ambil gambar 😊
              </CustomText>
            </BlurView>
            {route.params.types == "Izin" && (
              <TouchableOpacity
                onPress={() => {
                  setType(
                    type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back
                  );
                }}
              >
                <MaterialIcons name="flip-camera-ios" size={35} color="white" />
              </TouchableOpacity>
            )}

            <TouchableOpacity
              style={[
                styles.circle,
                disable == false && {
                  backgroundColor: "white",
                },
              ]}
              disabled={disable}
              onPress={() => {
                if (route.params.types == "Izin") {
                  takePicture();
                } else {
                  if (Platform.OS === "ios") {
                    takePicture(true);
                  } else {
                    setDetectFace(true);
                  }
                  setdisable(true);
                }
              }}
            >
              {disable && (
                <ActivityIndicator
                  color={"green"}
                  size={"large"}
                  style={{
                    // flex: 1,
                    height: "100%",
                    width: "100%",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                />
              )}
            </TouchableOpacity>

            {route.params.types == "Izin" && (
              <TouchableOpacity
                onPress={() => {
                  setflash(
                    flash === Camera.Constants.FlashMode.off
                      ? Camera.Constants.FlashMode.on
                      : flash === Camera.Constants.FlashMode.on
                      ? Camera.Constants.FlashMode.auto
                      : Camera.Constants.FlashMode.off
                  );
                }}
              >
                <MaterialIcons
                  name={
                    flash === Camera.Constants.FlashMode.off
                      ? "flash-off"
                      : flash === Camera.Constants.FlashMode.on
                      ? "flash-on"
                      : "flash-auto"
                  }
                  size={35}
                  color="white"
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </Camera>
    </View>
  );
};

export default CameraScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  takePhoto: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#121212",
    margin: 20,
    borderRadius: 10,
    height: 50,
  },
  circle: {
    height: 80,
    width: 80,
    borderRadius: 50,
    borderWidth: 5,
    borderColor: "rgba(158, 150, 150, .5)",
    justifyContent: "center",
    alignItems: "center",
  },
  btnCameraContainer: {
    flex: 1,
    position: "absolute",
    flexDirection: "row",
    width: "100%",
    padding: 20,
    justifyContent: "space-around",
    alignItems: "center",
  },
  blurContainer: {
    flex: 1,
    padding: 20,
    margin: 16,
    textAlign: "center",
    justifyContent: "center",
    overflow: "hidden",
    borderRadius: 20,
  },
});
