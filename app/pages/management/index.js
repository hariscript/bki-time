import React, { useEffect, useState } from "react";
import { BackHandler, Dimensions, StyleSheet, View } from "react-native";
import { ButtonMenu, CustomText, ModalUnderConstraction } from "../../components";
import { MENU_MANAGEMENT } from "../../services";

const Management = ({ navigation }) => {
  const [modalUC, setModalUC] = useState(false);
  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const renderItems = () => {
    const items = [];

    for (let i = 0; i < MENU_MANAGEMENT.length; i += 4) {
      const rowItems = MENU_MANAGEMENT.slice(i, i + 4);
      items.push(
        <View key={`row_${i}`} style={{ flexDirection: "row" }}>
          {rowItems.map((item, index) => (
            <View key={index}>
              <ButtonMenu
                onPress={() => {
                  if (item.nama == "Lembur" || item.nama == "Approval") {
                    setModalUC(true);
                  } else {
                    navigation.navigate(item.navigation);
                  }
                }}
                text={item.nama}
                style={{
                  width: Dimensions.get("window").width / 4,
                  textAlign: "center",
                  marginBottom: 10,
                }}
                styleText={{ textAlign: "center" }}
                gambar={item.gambar}
              />
            </View>
          ))}
        </View>
      );
    }

    return items;
  };

  return (
    <View style={{ flex: 1 }}>
      <ModalUnderConstraction
        visible={modalUC}
        type={"failed"}
        text={"New feature still in progress!"}
        onRequestClose={() => {
          setModalUC(!modalUC);
        }}
        onPress={() => {
          setModalUC(false);
        }}
        onPressExit={() => {
          setModalUC(false);
        }}
      />
      <View style={{ backgroundColor: "white", paddingVertical: 20 }}>
        <View>{renderItems()}</View>
      </View>
    </View>
  );
};

export default Management;

const styles = StyleSheet.create({
  btnMenu: {
    // width: Dimensions.get("window").width,
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
