import React, { useEffect, useState } from "react";
import {
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { FontAwesome, Feather } from "@expo/vector-icons";
import * as Location from "expo-location";
import { LinearGradient } from "expo-linear-gradient";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import { COLORS } from "../../../services";
import { LoadingIndicator, MapsWebView } from "../../../components";

const { width, height } = Dimensions.get("window");

const Break = ({ route, navigation }) => {
  const insets = useSafeAreaInsets();
  // const map_ref = useRef();
  const [location, setlocation] = useState({
    latitude: 0,
    longitude: 0,
  });
  const [loading, setLoading] = useState(false);

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    // set button refresh on header right
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={() => onFabPress()}>
          <Feather name="refresh-cw" size={24} color="white" />
        </TouchableOpacity>
      ),
    });

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        Alert.alert(
          "User location not detected",
          "You haven't granted permission to detect your location.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }]
        );
      }

      let locations = await Location.getCurrentPositionAsync({
        enableHighAccuracy: true,
      });

      setlocation({
        latitude: locations.coords.latitude,
        longitude: locations.coords.longitude,
      });
    })();
  }, []);

  const _goToMyPosition = (lat, lon) => {
    map_ref.current.injectJavaScript(`
      map.setView([${lat}, ${lon}], 16)
      L.marker([${lat}, ${lon}]).addTo(map)
    `);
  };

  const onFabPress = async () => {
    setLoading(true);

    let locations = await Location.getCurrentPositionAsync({
      enableHighAccuracy: true,
    });

    setlocation({
      latitude: locations.coords.latitude,
      longitude: locations.coords.longitude,
    });

    setLoading(false);
  };

  if ((location.latitude === 0 && location.longitude === 0) || loading) {
    return <LoadingIndicator />;
  }

  return (
    <View style={styles.container}>
      <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
        <MapsWebView
          // ref={map_ref}
          longitude={location.longitude}
          latitude={location.latitude}
          style={{ zIndex: 0 }}
        />
      </View>
      <TouchableOpacity
        style={styles.touchAble}
        onPress={() => {
          if (route.params.from) {
            navigation.navigate("Camera", {
              location,
              types: 2,
              from: route.params.from,
            });
          } else {
            navigation.navigate("Camera", {
              location,
              types: 2,
              from: null,
            });
          }
        }}
        activeOpacity={0.7}
      >
        <LinearGradient
          colors={COLORS.gradient}
          style={[
            styles.linearGradientView,
            Platform.OS === "ios" && { paddingBottom: insets.bottom },
          ]}
        >
          <FontAwesome name="camera" size={24} color="white" />
          <Text
            style={{
              color: "white",
              fontSize: 15,
              fontWeight: "bold",
              marginLeft: 15,
            }}
          >
            Take a photo
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

export default Break;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
  },
  touchAble: {
    backgroundColor: "transparent",
    zIndex: 1,
  },
  linearGradientView: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    // width: "100%",
    padding: 15,
  },
});
