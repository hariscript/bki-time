import React, { useState, useRef, useCallback, useEffect } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { AntDesign, Entypo } from "@expo/vector-icons";
import {
  BottomSheetModal,
  BottomSheetModalProvider,
  BottomSheetFlatList,
} from "@gorhom/bottom-sheet";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";

import { BarCharts, CustomText, Loading } from "../../../../components";
import { CALL_API, ENDPOINTS, MONTHS } from "../../../../services";

const Summary = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [monthSelected, setMonthSelected] = useState("");
  const [open, setOpen] = useState(false);
  const [bulan, setBulan] = useState(new Date().getMonth());
  const [tahun, setTahun] = useState(new Date().getFullYear());
  const [clockIn, setClockIn] = useState(0);
  const [clockOut, setClockOut] = useState(0);
  const [terlambat, setTerlambat] = useState(0);
  const [izinTidakMasuk, setIzinTidakMasuk] = useState(0);
  const [izinSakit, setIzinSakit] = useState(0);
  const [izinTerlambat, setIzinTerlambat] = useState(0);
  const [izinPulangCepat, setIzinPulangCepat] = useState(0);
  const [mangkir, setMangkir] = useState(0);
  const [loading, setLoading] = useState(false);

  const bottmSheetRef = useRef(null);
  const snapPoints = ["50%"];

  useFocusEffect(
    React.useCallback(() => {
      reportAbsen();
      return () => {};
    }, [])
  );

  const reportAbsen = async (month) => {
    setLoading(true);

    const payload = {
      bulan: month ? month : bulan + 1,
      tahun,
    };

    const response = await CALL_API(
      ENDPOINTS.ReportAbsenNup,
      payload,
      "POST",
      nup,
      navigation
    );

    setClockIn(response.finalResponse.data[0].clock_in);
    setClockOut(response.finalResponse.data[0].clock_out);
    setTerlambat(response.finalResponse.data[0].terlambat);
    setIzinTidakMasuk(response.finalResponse.data[0].tidak_masuk);
    setIzinSakit(response.finalResponse.data[0].sakit);
    setIzinPulangCepat(response.finalResponse.data[0].pulang_cepat);

    setLoading(false);
  };

  const handlePresentModal = () => {
    bottmSheetRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  const renderItem = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          reportAbsen(item.id);
          setMonthSelected(item.name);
          bottmSheetRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={[styles.container]}>
      <View style={{ opacity: open ? 0.3 : null }}>
        <TouchableOpacity style={styles.card} onPress={handlePresentModal}>
          <CustomText
            regular
            style={{ color: monthSelected ? "black" : "grey" }}
          >
            {monthSelected ? monthSelected : "-- Pilih Bulan --"}
          </CustomText>
          <AntDesign
            name="caretdown"
            size={10}
            color="black"
            style={{
              justifyContent: "center",
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>

        <View style={styles.sparator} />

        {!loading && (
          <>
            <BarCharts
              value={clockIn}
              colors="#22f002"
              title={"Absen Masuk"}
              horizontal
            />
            <BarCharts
              value={clockOut}
              colors="#b0e0e6"
              title={"Absen Pulang"}
              horizontal
            />
            <BarCharts
              value={terlambat}
              colors="red"
              title={"Terlambat"}
              horizontal
            />
            <BarCharts
              value={izinTidakMasuk}
              colors="#f8e4ca"
              title={"Izin Tidak Masuk"}
              horizontal
            />
            <BarCharts
              value={izinSakit}
              colors="#f8e4ca"
              title={"Izin Sakit"}
              horizontal
            />
            <BarCharts
              value={izinTerlambat}
              colors="grey"
              title={"Izin Terlambat"}
              horizontal
            />
            <BarCharts
              value={mangkir}
              colors="#2a363b"
              title={"Mangkir"}
              horizontal
            />
            <BarCharts
              value={izinPulangCepat}
              colors="#db7d10"
              title={"Pulang Cepat"}
              horizontal
            />
          </>
        )}
      </View>

      <BottomSheetModal
        ref={bottmSheetRef}
        // index={1}
        snapPoints={snapPoints}
        backgroundStyle={styles.bottomSheetBackground}
        onDismiss={() => setOpen(false)}
      >
        <View style={{ flex: 1, paddingHorizontal: 15 }}>
          <View style={styles.bottomSheetHeader}>
            <TouchableOpacity
              onPress={() => {
                bottmSheetRef.current?.close();
                setOpen(false);
              }}
              style={{ marginRight: 10 }}
            >
              <Entypo name="cross" size={20} color="grey" />
            </TouchableOpacity>
            <CustomText style={{ fontSize: 15 }}>Pilih Bulan</CustomText>
          </View>

          <View style={styles.sparator} />

          <BottomSheetFlatList
            data={MONTHS}
            keyExtractor={(i) => i.id}
            renderItem={renderItem}
            // contentContainerStyle={styles.contentContainer}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </BottomSheetModal>
    </View>
  );
};

export default Summary;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    margin: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  sparator: {
    height: 0.5,
    backgroundColor: "#bcbcbc",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20,
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center",
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
  },
});
