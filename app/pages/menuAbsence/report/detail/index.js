import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  FlatList,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { BottomSheetFlatList, BottomSheetModal } from "@gorhom/bottom-sheet";
import { AntDesign, Entypo } from "@expo/vector-icons";
import moment from "moment";
import { useSelector } from "react-redux";
import { useFocusEffect } from "@react-navigation/native";
import axios, * as others from "axios";

import { CustomText, Empty, Loading } from "../../../../components";
import {
  ABSENCE_TYPE,
  CALL_API,
  ENDPOINTS,
  MONTHS,
} from "../../../../services";
import { ClockIn, Pagi, Siang, Sore } from "../../../../assets";

const Detail = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [open, setOpen] = useState(false);
  const [monthSelected, setMonthSelected] = useState("");
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [bulan, setBulan] = useState(new Date().getMonth());
  const [tahun, setTahun] = useState(new Date().getFullYear());

  const bottmSheetRef = useRef(null);
  const snapPoints = ["50%"];

  useFocusEffect(
    React.useCallback(() => {
      reportAbsen();
      return () => {};
    }, [])
  );

  const reportAbsen = async (month) => {
    setLoading(true);

    const payload = {
      bulan: month ? month : bulan + 1,
      tahun,
    };

    // const response = await CALL_API(
    //   ENDPOINTS.ReportAbsenAll,
    //   payload,
    //   "POST",
    //   nup,
    //   navigation
    // );

    // setData(response.finalResponse.data);

    await axios
      .get(
        "https://api.bki.co.id/api-sikom/config/get_trx.php",
        {
          params: {
            nup: nup,
            server: "10.0.1.77",
            database: "db_appsenc",
            userDb: "chemonk",
            passDb: "T4m1y42904",
          },
        },
        []
      )
      .then((res) => {
        setData(res.data);

        // this.setState({
        //   data: res.data,
        //   refreshing: false,
        // });
      })
      .catch((err) => {
        console.log(err);
      });

    setLoading(false);
  };

  const handlePresentModal = () => {
    bottmSheetRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  const renderItem = useCallback(
    ({ item, index }) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          reportAbsen(item.id);
          setMonthSelected(item.name);
          bottmSheetRef.current?.close();
        }}
      >
        <View style={styles.itemContainer}>
          <CustomText regular>{item.name}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  const renderItemContent = useCallback(
    ({ item, index }) => (
      <TouchableOpacity style={styles.itemContainerAbsence}>
        <View
          style={{
            flex: 0.7,
            justifyContent: "center",
            alignItems: "center",
            // backgroundColor: "green"
          }}
        >
          <Image
            source={
              // Number(item.status_clock) == 1
              //   ? Pagi
              //   : Number(item.status_clock) == 4
              //   ? Sore
              //   : Siang
              item.status_clock == "Clock In"
                ? Pagi
                : item.status_clock == "Clock Out"
                ? Sore
                : Siang
            }
            style={{ width: 50, height: 50 }}
            resizeMode={"contain"}
          />
        </View>
        <View style={{ flex: 2.3 }}>
          <CustomText style={{ fontSize: 15 }}>
            {/* {ABSENCE_TYPE[Number(item.status_clock) - 1]} */}
            {item.status_clock}
          </CustomText>
          <View style={{ flexDirection: "row" }}>
            <CustomText style={{ fontSize: 12, width: "17%" }} regular>
              Waktu
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "3%" }} regular>
              :
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "80%" }} regular>
              {`${item.tgl}, ${item.jam}`}
            </CustomText>
          </View>
          <View style={{ flexDirection: "row" }}>
            <CustomText style={{ fontSize: 12, width: "17%" }} regular>
              Status
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "3%" }} regular>
              :
            </CustomText>
            <CustomText style={{ fontSize: 12, width: "80%" }} regular>
              {item.status_position}
            </CustomText>
          </View>
        </View>
      </TouchableOpacity>
    ),
    []
  );

  if (loading) {
    return <Loading />;
  }

  return (
    <View style={styles.container}>
      <View>
        <TouchableOpacity style={styles.card} onPress={handlePresentModal}>
          <CustomText
            regular
            style={{ color: monthSelected ? "black" : "grey" }}
          >
            {monthSelected ? monthSelected : "-- Pilih Bulan --"}
          </CustomText>
          <AntDesign
            name="caretdown"
            size={10}
            color="black"
            style={{
              justifyContent: "center",
              alignSelf: "center",
            }}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.sparator} />

      <View style={{ opacity: open ? 0.3 : null }}>
        {!loading && (
          <FlatList
            data={data}
            renderItem={renderItemContent}
            keyExtractor={(item) => item.id}
          />
        )}
      </View>

      {!loading && data.length < 1 && <Empty />}

      <BottomSheetModal
        ref={bottmSheetRef}
        // index={1}
        snapPoints={snapPoints}
        backgroundStyle={styles.bottomSheetBackground}
        onDismiss={() => setOpen(false)}
      >
        <View style={{ flex: 1, paddingHorizontal: 15 }}>
          <View style={styles.bottomSheetHeader}>
            <TouchableOpacity
              onPress={() => {
                bottmSheetRef.current?.close();
                setOpen(false);
              }}
              style={{ marginRight: 10 }}
            >
              <Entypo name="cross" size={20} color="grey" />
            </TouchableOpacity>
            <CustomText style={{ fontSize: 15 }}>Pilih Bulan</CustomText>
          </View>

          <View style={styles.sparator} />

          <BottomSheetFlatList
            data={MONTHS}
            keyExtractor={(i) => i.id}
            renderItem={renderItem}
            // contentContainerStyle={styles.contentContainer}
            showsVerticalScrollIndicator={false}
          />
        </View>
      </BottomSheetModal>
    </View>
  );
};

export default Detail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    margin: 20,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20,
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center",
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
  },
  itemContainerAbsence: {
    paddingVertical: 10,
    marginVertical: 6,
    marginHorizontal: 20,
    borderWidth: 1,
    borderColor: "#eee",
    flexDirection: "row",
  },
});
