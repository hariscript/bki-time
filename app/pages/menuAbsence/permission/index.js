import ListPermission from "./list";
import EntryPermission from "./entry";
import DetailPermission from "./detail";

export { ListPermission, EntryPermission, DetailPermission };
