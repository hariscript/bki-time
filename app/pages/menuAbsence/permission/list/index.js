import React, { useEffect, useState } from "react";
import { FlatList, StyleSheet, TouchableOpacity, View } from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import moment from "moment";
import { useSelector } from "react-redux";

import { CustomText, Loading } from "../../../../components";
import { CALL_API, COLORS, ENDPOINTS, JENIS_IZIN } from "../../../../services";

const List = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      dataIzin();
      return () => {};
    }, [])
  );

  const dataIzin = async () => {
    setLoading(true);
    const response = await CALL_API(
      ENDPOINTS.GetIjin,
      null,
      "POST  ",
      nup,
      navigation
    );

    setData(response.finalResponse);
    setLoading(false);
  };

  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        style={styles.card}
        onPress={() => navigation.navigate("Detail Permission")}
      >
        <CustomText>{JENIS_IZIN[Number(item.ijin) - 1]}</CustomText>
        <View style={{ flexDirection: "row", marginTop: 10 }}>
          <CustomText style={{ fontSize: 12, width: "17%" }} regular>
            Waktu
          </CustomText>
          <CustomText style={{ fontSize: 12, width: "3%" }} regular>
            :
          </CustomText>
          <CustomText style={{ fontSize: 12, width: "80%" }} regular>
            {moment(item.created_at).format("DD MMMM YYYY")}
          </CustomText>
        </View>
        <View style={styles.containerContent}>
          <CustomText style={{ fontSize: 12, width: "17%" }} regular>
            Status
          </CustomText>
          <CustomText style={{ fontSize: 12, width: "3%" }} regular>
            :
          </CustomText>
          <View
            style={[
              styles.contentStatus,
              {
                backgroundColor:
                  item.status == 1
                    ? COLORS.waring
                    : item.status == 2
                    ? COLORS.success
                    : COLORS.danger,
              },
            ]}
          >
            {/* <CustomText style={styles.txtStatus} regular>
              {t === "S"
                ? "Disetujui"
                : t === "T"
                ? "Tidak Disetujui"
                : "Menunggu Persetujuan"}
            </CustomText> */}
            <CustomText style={styles.txtStatus} regular>
              {item.status == 1
                ? "Menunggu Persetujuan"
                : item.status == 2
                ? "Disetujui"
                : "Tidak Disetujui"}
            </CustomText>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerHeader}>
        <CustomText style={{ fontSize: 17 }}>Daftar Izin</CustomText>

        <TouchableOpacity
          style={styles.btnBuatIzin}
          onPress={() => navigation.navigate("Entry Permission")}
        >
          <CustomText style={styles.txtIzin}>Buat Izin</CustomText>
        </TouchableOpacity>
      </View>

      <View style={styles.sparator} />

      {loading && <Loading />}

      {!loading && (
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderItem}
        />
      )}
    </View>
  );
};

export default List;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  sparator: {
    borderWidth: 0.3,
    borderColor: "#707070",
    marginHorizontal: 20,
    // marginBottom: 15
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 2,
    padding: 15,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  containerHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginVertical: 20,
  },
  btnBuatIzin: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    backgroundColor: COLORS.blue,
    borderRadius: 10,
  },
  txtIzin: {
    color: "white",
    fontSize: 12,
  },
  containerContent: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 5,
    width: "100%",
  },
  contentStatusAndArrow: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "80%",
  },
  contentStatus: {
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 10,
  },
  txtStatus: {
    fontSize: 12,
    color: "white",
  },
  containerArrow: {
    backgroundColor: "#1EACFF",
    borderRadius: 12,
  },
});
