import React, { useCallback, useRef, useState } from "react";
import {
  Image,
  Keyboard,
  Platform,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { AntDesign, FontAwesome } from "@expo/vector-icons";
import DateTimePicker from "@react-native-community/datetimepicker";
import moment from "moment";
import { useSelector } from "react-redux";

import {
  ButtomSheetModal,
  CustomText,
  CustomTextInput,
  ModalSuccessFailed,
} from "../../../../components";
import { CALL_API, COLORS, ENDPOINTS, JENIS_IZIN } from "../../../../services";

const Entry = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [jenisSelected, setJenisSelected] = useState("");
  const [date, setDate] = useState(new Date());
  const [tempDate, settempDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [show, setShow] = useState(false);
  const [type, setType] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [image, setImage] = useState("");
  const [modalSuccess, setModalSuccess] = useState(false);
  const [modalFailed, setModalFailed] = useState(false);
  const [loading, setLoading] = useState(false);

  const bottomSheetListRef = useRef(null);

  const PostIjin = async () => {
    setLoading(true);

    let localUri =
      Platform.OS === "android" ? image : image.replace("file://", "");
    let filename = localUri.split("/").pop();
    let match = /\.(\w+)$/.exec(filename);
    let type = match ? `image/${match[1]}` : `image`;
    var gambar = {
      uri: localUri,
      name: filename,
      type: type,
    };

    const payloads = new FormData();

    payloads.append("ijin", JENIS_IZIN.indexOf(jenisSelected) + 1);
    payloads.append("keterangan", keterangan);
    payloads.append("gambar", gambar);

    const response = await CALL_API(
      ENDPOINTS.PostIjin,
      payloads,
      "UPLOAD",
      nup,
      navigation
    );

    if (response.finalResponse.status === "Succes") {
      setModalSuccess(true);
    } else {
      setModalFailed(true);
    }

    setLoading(false);
  };

  const onSelect = (data) => {
    setImage(data);
  };

  const handlePresentModal = (e) => {
    bottomSheetListRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  const setShowing = () => {
    if (Platform.OS === "ios") {
      setType("date");
      handlePresentModal();
    } else {
      setShow(true);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={[styles.container, { opacity: open ? 0.3 : null }]}>
        <ModalSuccessFailed
          visible={modalSuccess}
          type={"success"}
          text={"Your permission has been sent!"}
          onRequestClose={() => {
            setModalSuccess(!modalSuccess);
          }}
          onPress={() => {
            setModalSuccess(false);
            navigation.goBack();
          }}
          onPressExit={() => {
            setModalSuccess(false);
            navigation.goBack();
          }}
        />

        <ModalSuccessFailed
          visible={modalFailed}
          type={"failed"}
          text={
            "Your Absence couldn't be sent out, check your signal and try again."
          }
          onRequestClose={() => {
            setModalFailed(!modalFailed);
          }}
          onPress={() => {
            setModalFailed(false);
          }}
          onPressExit={() => {
            setModalFailed(false);
          }}
        />

        <View style={{ marginVertical: 20 }}>
          <CustomText regular style={{ fontSize: 15 }}>
            Jenis Izin
          </CustomText>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.card}
            onPress={() => {
              setType("list");
              handlePresentModal();
            }}
          >
            <CustomText
              regular
              style={{ color: jenisSelected ? "black" : "grey" }}
            >
              {jenisSelected ? jenisSelected : "-- Pilih Jenis Izin --"}
            </CustomText>
            <AntDesign
              name="caretdown"
              size={10}
              color="black"
              style={{
                justifyContent: "center",
                alignSelf: "center",
              }}
            />
          </TouchableOpacity>
        </View>

        <View style={{ marginBottom: 20 }}>
          <CustomText regular style={{ fontSize: 15 }}>
            Tanggal Izin
          </CustomText>
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.card}
            onPress={() => setShowing()}
          >
            <CustomText
              regular
              style={{ color: jenisSelected ? "black" : "grey" }}
            >
              {moment(date).format("DD MMMM YYYY")}
            </CustomText>
            <AntDesign
              name="caretdown"
              size={10}
              color="black"
              style={{
                justifyContent: "center",
                alignSelf: "center",
              }}
            />
          </TouchableOpacity>
        </View>

        <View style={{ marginBottom: 20 }}>
          <CustomText regular style={{ fontSize: 15 }}>
            Keterangan
          </CustomText>
          <CustomTextInput
            editable={true}
            maxLength={200}
            multiline
            numberOfLines={6}
            onChangeText={(text) => setKeterangan(text)}
            value={keterangan}
            placeholder={"Alasan izin..."}
            textAlignVertical={"top"}
            keyboardType={"done"}
            style={[styles.card, { height: 150 }]}
          />
        </View>

        <View style={{ marginBottom: 20 }}>
          <CustomText regular style={{ fontSize: 15 }}>
            Ambil Foto
          </CustomText>
          <TouchableOpacity
            style={image ? styles.uploadIconViews : styles.uploadIconView}
            onPress={() =>
              navigation.navigate("Camera", {
                types: "Izin",
                onSelect,
              })
            }
          >
            {image ? (
              <Image
                source={{ uri: image }}
                style={{ height: 150, width: 200, borderRadius: 10 }}
              />
            ) : (
              <FontAwesome name="camera" size={35} color="black" />
            )}
          </TouchableOpacity>
        </View>

        {jenisSelected && keterangan && image && (
          <TouchableOpacity style={styles.btnSubmit} onPress={PostIjin}>
            <CustomText style={{ color: "white", fontSize: 15 }}>
              Submit
            </CustomText>
          </TouchableOpacity>
        )}

        {show && (
          <DateTimePicker
            value={date}
            mode="date"
            is24Hour={true}
            display="default"
            onChange={(event, selectedDate) => {
              const currentDate = selectedDate || date;
              setDate(currentDate);
              setShow(false);
            }}
          />
        )}

        <ButtomSheetModal
          bottomSheetRef={bottomSheetListRef}
          title={type === "list" ? "Pilih Jenis Izin" : "Pilih Tanggal Izin"}
          data={JENIS_IZIN}
          onDismiss={() => setOpen(false)}
          onPressClose={() => {
            bottomSheetListRef.current?.close();
            setOpen(false);
            setType("");
          }}
          onPressRender={(item) => {
            setJenisSelected(item);
            bottomSheetListRef.current?.close();
          }}
          date={date}
          type={type}
          onChangeDate={(event, selectedDate) => {
            const currentDate = selectedDate || date;
            settempDate(currentDate);
          }}
          onPressOk={() => {
            setDate(tempDate);
            bottomSheetListRef.current?.close();
          }}
        />
      </View>
    </TouchableWithoutFeedback>
  );
};

export default Entry;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    paddingHorizontal: 20,
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20,
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center",
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  uploadIconView: {
    height: 100,
    width: 100,
    borderWidth: 0.2,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderColor: "grey",
  },

  uploadIconViews: {
    height: 150,
    width: 200,
    borderWidth: 0.5,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  btnSubmit: {
    backgroundColor: COLORS.blue,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 15,
    borderRadius: 10,
    marginTop: 20,
  },
});
