import React, { useEffect, useState } from "react";
import {
  Alert,
  BackHandler,
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { FontAwesome, Feather } from "@expo/vector-icons";
import * as Location from "expo-location";
import * as geolib from "geolib";
import { LinearGradient } from "expo-linear-gradient";
import { useSafeAreaInsets } from "react-native-safe-area-context";

import {
  COLORS,
  GET_SENTENCE,
  OUTSIDE_RADIUS_SENTENCES,
} from "../../../services";
import {
  LoadingIndicator,
  MapsWebView,
  ModalNotInRadius,
  ModalSuccessFailed,
} from "../../../components";
import { useSelector } from "react-redux";

const { width, height } = Dimensions.get("window");

const ClockOut = ({ route, navigation }) => {
  const { latitude_rumah, logitude_rumah } = useSelector(
    (state) => state.ProfileReducer
  );
  const [modalRadius, setModalRadius] = useState(false);
  const [fake, setFake] = useState(false);
  const [modalFake, setModalFake] = useState(false);
  const [loading, setLoading] = useState(false);

  const {
    latitude_center,
    longitude_center,
    longitude1,
    latitude1,
    longitude2,
    latitude2,
    longitude3,
    latitude3,
    longitude4,
    latitude4,
  } = useSelector((state) => state.OfficeReducer);

  const insets = useSafeAreaInsets();
  // const map_ref = useRef();
  const [location, setlocation] = useState({
    latitude: 0,
    longitude: 0,
  });
  const [position, setPosition] = useState("");

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener("hardwareBackPress", backPressed);

    // set button refresh on header right
    navigation.setOptions({
      headerRight: () => (
        <TouchableOpacity onPress={() => onFabPress()}>
          <Feather name="refresh-cw" size={24} color="white" />
        </TouchableOpacity>
      ),
    });

    return () =>
      BackHandler.removeEventListener("hardwareBackPress", backPressed);
  }, []);

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        Alert.alert(
          "User location not detected",
          "You haven't granted permission to detect your location.",
          [{ text: "OK", onPress: () => console.log("OK Pressed") }]
        );
      }

      let locations = await Location.getCurrentPositionAsync({
        enableHighAccuracy: true,
      });

      // console.log("apakah pake fake gps ? ", locations.mocked);
      setFake(locations.mocked);

      setlocation({
        latitude: locations.coords.latitude,
        longitude: locations.coords.longitude,
      });

      currentPosition(locations.coords.latitude, locations.coords.longitude);
    })();
  }, []);

  const onFabPress = async () => {
    setLoading(true);

    let locations = await Location.getCurrentPositionAsync({
      enableHighAccuracy: true,
    });

    setFake(locations.mocked);

    setlocation({
      latitude: locations.coords.latitude,
      longitude: locations.coords.longitude,
    });

    currentPosition(locations.coords.latitude, locations.coords.longitude);
  };

  const currentPosition = (lat, long) => {
    // let homeRadius = geolib.isPointWithinRadius(
    //   { latitude: lat, longitude: long },
    //   { latitude: latitude_rumah, longitude: logitude_rumah },
    //   100
    // );

    let officeRadius = geolib.isPointWithinRadius(
      { latitude: lat, longitude: long },
      { latitude: latitude_center, longitude: longitude_center },
      100
    );

    // let officePolygon = geolib.isPointInPolygon(
    //   { latitude: lat, longitude: long },
    //   [
    //     { latitude: latitude1, longitude: longitude1 },
    //     { latitude: latitude2, longitude: longitude2 },
    //     { latitude: latitude3, longitude: longitude3 },
    //     { latitude: latitude4, longitude: longitude4 },
    //   ]
    // );

    if (officeRadius) {
      setPosition("KDK");
    }
    // else if (homeRadius) {
    //   setPosition("KDR");
    // } else {
    //   setPosition("unknown");
    // }
    setLoading(false);
  };

  const _goToMyPosition = (lat, lon) => {
    console.log(lat, " dan ", lon);
    map_ref.current.injectJavaScript(`
      map.setView([${lat}, ${lon}], 16)
      L.marker([${lat}, ${lon}]).addTo(map)
    `);
  };

  if ((location.latitude === 0 && location.longitude === 0) || loading) {
    return <LoadingIndicator />;
  }

  return (
    <View style={styles.container}>
      {/* Modal ketika tidak tersenyum */}
      <ModalSuccessFailed
        visible={modalFake}
        type={"failed"}
        text={"Oops, ketahuan nih pake Fake GPS!"}
        onRequestClose={() => {
          setModalFake(!modalFake);
        }}
        onPress={() => {
          setModalFake(false);
        }}
        onPressExit={() => {
          setModalFake(false);
        }}
      />

      <ModalNotInRadius
        visible={modalRadius}
        type={"failed"}
        text={GET_SENTENCE(OUTSIDE_RADIUS_SENTENCES)}
        onRequestClose={() => {
          setModalRadius(!modalRadius);
        }}
        onPress={() => {
          setModalRadius(false);
        }}
        onPressExit={() => {
          setModalRadius(false);
        }}
      />
      <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
        <MapsWebView
          // ref={map_ref}
          longitude={location.longitude}
          latitude={location.latitude}
          style={{ zIndex: 0 }}
        />
      </View>
      <TouchableOpacity
        style={styles.touchAble}
        onPress={() => {
          if (!fake) {
            if (position === "KDK") {
              navigation.navigate("Camera", {
                location,
                types: 4,
                from: route.params.from ? route.params.from : null,
              });
            } else {
              setModalRadius(true);
            }
          } else {
            setModalFake(true);
          }
        }}
        activeOpacity={0.7}
      >
        <LinearGradient
          colors={COLORS.gradient}
          style={[
            styles.linearGradientView,
            Platform.OS === "ios" && { paddingBottom: insets.bottom },
          ]}
        >
          <FontAwesome name="camera" size={24} color="white" />
          <Text
            style={{
              color: "white",
              fontSize: 15,
              fontWeight: "bold",
              marginLeft: 15,
            }}
          >
            Take a photo
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

export default ClockOut;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
  },
  touchAble: {
    backgroundColor: "transparent",
    zIndex: 1,
  },
  linearGradientView: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    // width: "100%",
    padding: 15,
  },
});
