import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StatusBar,
  Image
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import { MaterialIcons } from "@expo/vector-icons";
import AnimatedLottieView from "lottie-react-native";
import { COLORS } from "../../services";
import { CustomText } from "../../components";

const WelcomeAuth = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <AnimatedLottieView
          source={require("../../assets/lottie/welcome.json")}
          loop
          autoPlay
          style={{ height: 300 }}
        />
      </View>
      <Animatable.View style={styles.footer} animation="fadeInUpBig">
        <LinearGradient colors={COLORS.gradient} style={styles.footers}>
          <CustomText style={[styles.title, { color: "white" }]}>
            Welcome to BKI Time
          </CustomText>
          
          <TouchableOpacity
            style={styles.button}
            onPress={() => navigation.navigate("Login")}
          >
            <View style={styles.signIn}>
              <CustomText regular style={styles.textSign}>Get Started</CustomText>
              <MaterialIcons name="navigate-next" color="#fff" size={24} />
            </View>
          </TouchableOpacity>
        </LinearGradient>
      </Animatable.View>
    </View>
  );
};

export default WelcomeAuth;

const { height, width } = Dimensions.get("screen");
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  header: {
    flex: 1.5,
    justifyContent: "center",
    alignItems: "center"
  },
  footer: {
    flex: 1,
    backgroundColor: "#4c669f",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30
  },
  footers: {
    flex: 1,
    width: "100%",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    padding: 30
  },
  logo: {
    width: height_logo,
    height: height_logo
  },
  title: {
    color: "#05375a",
    fontSize: 25,
    fontWeight: "bold"
  },
  text: {
    color: "white",
    marginTop: 5
  },
  button: {
    alignItems: "flex-end",
    marginTop: 50
  },
  signIn: {
    justifyContent: "space-between",
    alignItems: "center",
    borderRadius: 20,
    flexDirection: "row",
    padding: 15,
    borderWidth: 1,
    borderColor: "white"
  },
  textSign: {
    color: "white",
    fontWeight: "bold"
  }
});
