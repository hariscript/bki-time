import AddHelpdesk from "./addHelpdesk";
import AnswerHelpdesk from "./answerHelpdesk";
import DetailHelpdesk from "./detailHelpdesk";

export { AddHelpdesk, AnswerHelpdesk, DetailHelpdesk };