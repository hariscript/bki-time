import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { CustomText, CustomTextInput } from "../../../components";
import { COLORS } from "../../../services";
import { Entypo } from "@expo/vector-icons";
import { FlatList } from "react-native";
import { color } from "react-native-reanimated";

const DetailHelpdesk = ({ navigation }) => {
  const [data, setData] = useState([
    {
      id: 1,
    },
    {
      id: 2,
    },
    {
      id: 3,
    },
    {
      id: 4,
    },
  ]);
  return (
    <View style={styles.container}>
      <CustomText style={styles.title}>No. Tiket</CustomText>
      <CustomText regular style={styles.description}>
        XXX/ICT?HD/12/22
      </CustomText>
      <CustomText style={styles.title}>Detail Permintaan</CustomText>
      <CustomText regular style={styles.description}>
        Lorem ipsum
      </CustomText>
      <CustomText style={styles.title}>Tanggal Permintaan</CustomText>
      <CustomText regular style={styles.description}>
        dd-MM-yyyy
      </CustomText>
      <CustomText style={styles.title}>Status Permintaan</CustomText>
      <CustomText
        style={{ marginLeft: 20, fontSize: 17, color: COLORS.greenCurrency }}
      >
        Open
      </CustomText>
      <TouchableOpacity
        style={styles.button}
        onPress={() => navigation.navigate("AnswerHelpdesk")}
      >
        <CustomText
          style={{ textAlign: "center", color: "#fff", fontSize: 18 }}
        >
          Jawab Permintaan
        </CustomText>
      </TouchableOpacity>
      <View
        style={{
          marginTop: 20,
          height: 2,
          backgroundColor: "#707070",
          flexDirection: "row",
          alignItems: "stretch",
        }}
      ></View>
      <CustomText style={styles.title}>Riwayat Tiket</CustomText>
        <View style={{ flexDirection: "row", margin: 20 }}>
          <View
            style={{
              height: 50,
              width: 90,
              backgroundColor: "#B4B4B4",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderWidth: 2,
              borderColor: "#707070",
            }}
          >
            <CustomText>Tanggal Update</CustomText>
          </View>
          <View
            style={{
              height: 50,
              width: 90,
              backgroundColor: "#B4B4B4",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderWidth: 2,
              borderColor: "#707070",
            }}
          >
            <CustomText>PIC</CustomText>
          </View>
          <View
            style={{
              height: 50,
              width: 90,
              backgroundColor: "#B4B4B4",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderWidth: 2,
              borderColor: "#707070",
            }}
          >
            <CustomText>Status</CustomText>
          </View>
          <View
            style={{
              height: 50,
              width: 90,
              backgroundColor: "#B4B4B4",
              flexDirection: "row",
              alignItems: "center",
              justifyContent: "center",
              borderWidth: 2,
              borderColor: "#707070",
            }}
          >
            <CustomText>Deskripsi</CustomText>
          </View>
      </View>
    </View>
  );
};

export default DetailHelpdesk;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  card: {
    backgroundColor: "white",
    // borderWidth: 1,
    marginHorizontal: 20,
    marginBottom: 15,
    padding: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
  },
  done: {
    color: "white",
    backgroundColor: "#007FFF",
    paddingVertical: 7,
    paddingHorizontal: 12,
    fontSize: 10,
    borderRadius: 12,
    marginTop: 10,
  },
  title: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 20,
    color: COLORS.boldText,
    marginBottom: 10,
  },
  description: {
    marginLeft: 20,
    fontSize: 17,
    color: COLORS.boldText,
  },
  button: {
    backgroundColor: COLORS.lightBlue,
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 30,
    justifyContent: "center",
    marginHorizontal: 50,
    marginTop: 30,
  },
});
