import React, { useCallback, useRef, useState } from "react";
import {
  Image,
  Keyboard,
  Platform,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { AntDesign, FontAwesome } from "@expo/vector-icons";
import { BottomSheetModal, BottomSheetFlatList } from "@gorhom/bottom-sheet";
import DateTimePicker from "@react-native-community/datetimepicker";

import {
  ButtomSheetModal,
  CustomText,
  CustomTextInput,
} from "../../../components";
import { COLORS, JENIS_IZIN } from "../../../services";
import moment from "moment";

const AddHelpdesk = ({ navigation }) => {
  const [jenisSelected, setJenisSelected] = useState("");
  const [date, setDate] = useState(new Date());
  const [tempDate, settempDate] = useState(new Date());
  const [open, setOpen] = useState(false);
  const [show, setShow] = useState(false);
  const [type, setType] = useState("");
  const [keterangan, setKeterangan] = useState("");
  const [image, setImage] = useState("");

  const bottomSheetListRef = useRef(null);

  const onSelect = (data) => {
    setImage(data);
  };

  const handlePresentModal = (e) => {
    bottomSheetListRef.current?.present();
    setTimeout(() => {
      setOpen(true);
    }, 100);
  };

  const setShowing = () => {
    if (Platform.OS === "ios") {
      setType("date");
      handlePresentModal();
    } else {
      setShow(true);
    }
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <ScrollView style={{ backgroundColor: "#fff" }}>
        <View style={{ backgroundColor: "#fff" }}>
          <CustomText
            style={{
              marginTop: 20,
              fontSize: 21,
              color: COLORS.boldText,
              marginBottom: 5,
              marginLeft: 20
            }}
          >
            Tambah Permintaan
          </CustomText>
          <View
            style={{
              marginTop: 5,
              height: 2,
              backgroundColor: "#707070",
              flexDirection: "row",
              alignItems: "stretch",
            }}
          ></View>
          <View style={[styles.container, { opacity: open ? 0.3 : null }]}>
            <View style={{ marginBottom: 20, marginTop: 20 }}>
              <CustomText style={{ color: COLORS.boldText, fontSize: 17 }}>
                Detail Permintaan
              </CustomText>
              <CustomTextInput
                editable={true}
                maxLength={200}
                multiline
                numberOfLines={6}
                onChangeText={(text) => setKeterangan(text)}
                value={keterangan}
                placeholder={"Penjelasan permintaan layanan..."}
                textAlignVertical={"top"}
                keyboardType={"done"}
                style={[styles.card, { height: 200 }, { fontSize: 17 }]}
              />
            </View>

            <View style={{ marginBottom: 20 }}>
              <CustomText style={{ color: COLORS.boldText, fontSize: 17 }}>
                Tanggal Permintaan
              </CustomText>
              <TouchableOpacity
                activeOpacity={0.7}
                style={styles.card}
                onPress={() => setShowing()}
              >
                <CustomText
                  regular
                  style={{ color: jenisSelected ? "black" : "grey" }}
                >
                  {moment(date).format("DD MMMM YYYY")}
                </CustomText>
                <AntDesign
                  name="caretdown"
                  size={10}
                  color="black"
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                  }}
                />
              </TouchableOpacity>
            </View>

            {show && (
              <DateTimePicker
                value={date}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={(event, selectedDate) => {
                  const currentDate = selectedDate || date;
                  setDate(currentDate);
                  setShow(false);
                }}
              />
            )}

            <ButtomSheetModal
              bottomSheetRef={bottomSheetListRef}
              title={
                type === "list" ? "Pilih Jenis Izin" : "Pilih Tanggal Izin"
              }
              data={JENIS_IZIN}
              onDismiss={() => setOpen(false)}
              onPressClose={() => {
                bottomSheetListRef.current?.close();
                setOpen(false);
                setType("");
              }}
              onPressRender={(item) => {
                setJenisSelected(item);
                bottomSheetListRef.current?.close();
              }}
              date={date}
              type={type}
              onChangeDate={(event, selectedDate) => {
                const currentDate = selectedDate || date;
                settempDate(currentDate);
              }}
              onPressOk={() => {
                setDate(tempDate);
                bottomSheetListRef.current?.close();
              }}
            />
          </View>
          <View
            style={{
              marginTop: 5,
              height: 2,
              backgroundColor: "#707070",
              flexDirection: "row",
              alignItems: "stretch",
            }}
          ></View>
          <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
            <TouchableOpacity
              onPress={() => navigation.navigate("Helpdesk")}
            >
              <CustomText style={styles.done}>Simpan</CustomText>
            </TouchableOpacity>
          </View>

          {jenisSelected && keterangan && image && (
            <TouchableOpacity style={styles.btnSubmit}>
              <CustomText style={{ color: "white", fontSize: 15 }}>
                Submit
              </CustomText>
            </TouchableOpacity>
          )}
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
};

export default AddHelpdesk;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    paddingHorizontal: 20,
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 5,
    padding: 16,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20,
  },
  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center",
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20,
  },
  uploadIconView: {
    height: 100,
    width: 100,
    borderWidth: 0.2,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    borderColor: "grey",
  },

  uploadIconViews: {
    height: 150,
    width: 200,
    borderWidth: 0.5,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
  },
  btnSubmit: {
    backgroundColor: COLORS.blue,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 20,
    marginBottom: 20,
  },
  done: {
    color: "white",
    backgroundColor: COLORS.lightBlue,
    paddingVertical: 7,
    paddingHorizontal: 30,
    fontSize: 17,
    borderRadius: 20,
    marginTop: 10,
    marginRight: 20,
  },
});
