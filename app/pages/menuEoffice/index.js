import DetailEoffice from "./detailEoffice";
import ListEoffice from "./listEoffice";
import Tembusan from "./tembusan";
import ApprovalEoffice from "./approval";
import Disposisi from "./disposisi";
import DisposisiKeluar from "./disposisiKeluar";
import SentItem from "./sentItem";

export {
  DetailEoffice,
  ListEoffice,
  Tembusan,
  ApprovalEoffice,
  Disposisi,
  DisposisiKeluar,
  SentItem
};
