import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ScrollView,
  BackHandler,
} from "react-native";
import moment from "moment";
import * as IntentLauncher from "expo-intent-launcher";
import { useFocusEffect } from "@react-navigation/native";

import { CustomText } from "../../../components";
import { CALL_API, COLORS, ENDPOINTS } from "../../../services";
import { useSelector } from "react-redux";

const DetailEoffice = ({ navigation, route }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);
  const { payload } = route.params;

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  const packageB = "id.bki.eoffice";

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener("hardwareBackPress", backPressed);

      dataEoffice();
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", backPressed);
      };
    }, [])
  );

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const dataEoffice = async () => {
    setLoading(true);
    const response = await CALL_API(
      ENDPOINTS.GetMetadataSurat,
      { id_surat: payload },
      "POST",
      nup,
      navigation
    );

    setData(response.finalResponse.data);
    setLoading(false);
  };

  // Fungsi untuk membuka aplikasi mobile B
  const openAppEoffice = async () => {
    try {
      await IntentLauncher.startActivityAsync(
        IntentLauncher.ActivityAction.APPLICATION_DETAILS_SETTINGS,
        {
          data: "package:" + packageB,
        }
      );
    } catch (error) {
      console.error("Error opening app B:", error);
    }
  };

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {data.map((item, index) => (
          <>
            <CustomText style={styles.title}>
              {String(item.key_field).replace(/_/g, " ")}
            </CustomText>
            <CustomText regular style={styles.description}>
              {item.nilai === null ? "-" : item.nilai}
            </CustomText>
          </>
        ))}

        <TouchableOpacity
          style={styles.button}
          onPress={() => openAppEoffice()}
        >
          <CustomText
            style={{ textAlign: "center", color: "#fff", fontSize: 15 }}
          >
            Open E-Office
          </CustomText>
        </TouchableOpacity>
      </ScrollView>
      {/* <CustomText style={styles.title}>Jenis Surat</CustomText>
      <CustomText regular style={styles.description}>
        (Nota Dinas / Surat Keluar / Dll)
      </CustomText>
      <CustomText style={styles.title}>Perihal</CustomText>
      <CustomText regular style={styles.description}>
        {payload.perihal}
      </CustomText>
      <CustomText style={styles.title}>Tanggal Kirim</CustomText>
      <CustomText regular style={styles.description}>
        {moment(payload.approved_date).format("DD MMMM YYYY, hh:mm:ss")}
      </CustomText> */}
      {/* <CustomText style={styles.title}>Pengirim</CustomText>
      <CustomText regular style={styles.description}>
        NLIP- Nama
      </CustomText>
      <CustomText regular style={styles.description}>
        Unit Pengirim
      </CustomText> */}
    </View>
  );
};

export default DetailEoffice;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  card: {
    backgroundColor: "white",
    // borderWidth: 1,
    marginHorizontal: 20,
    marginBottom: 15,
    padding: 15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
  },
  done: {
    color: "white",
    backgroundColor: "#007FFF",
    paddingVertical: 7,
    paddingHorizontal: 12,
    fontSize: 10,
    borderRadius: 12,
    marginTop: 10,
  },
  title: {
    marginTop: 20,
    marginLeft: 20,
    fontSize: 15,
    color: COLORS.boldText,
    marginBottom: 10,
    textTransform: "capitalize",
  },
  description: {
    marginLeft: 20,
    // fontSize: 17,
    color: COLORS.boldText,
  },
  button: {
    backgroundColor: COLORS.blue,
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 25,
    justifyContent: "center",
    marginHorizontal: 20,
    marginTop: 20,
  },
});
