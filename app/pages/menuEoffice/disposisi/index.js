import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  BackHandler,
} from "react-native";
import { useFocusEffect } from "@react-navigation/native";
import { useSelector } from "react-redux";
import moment from "moment";

import {
  CustomText,
  CustomTextInput,
  Empty,
  Loading,
} from "../../../components";
import { CALL_API, COLORS, ENDPOINTS } from "../../../services";

const Disposisi = ({ navigation }) => {
  const { nup } = useSelector((state) => state.ProfileReducer);

  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      BackHandler.addEventListener("hardwareBackPress", backPressed);

      dataEoffice();
      return () => {
        BackHandler.removeEventListener("hardwareBackPress", backPressed);
      };
    }, [])
  );

  const backPressed = () => {
    navigation.goBack();
    return true;
  };

  const dataEoffice = async () => {
    setLoading(true);
    const response = await CALL_API(
      ENDPOINTS.GetListDisposisi,
      null,
      "POST",
      nup,
      navigation
    );

    setData(response.finalResponse.data);
    setLoading(false);
  };

  const renderItem = ({ item, index }) => {
    return (
      <View style={styles.card}>
        <CustomText style={{ fontSize: 12 }}>
          No. Dokumen : {item.no_dokumen}
        </CustomText>
        <CustomText
          regular
          style={{ fontSize: 12, marginTop: 5, color: "#707070" }}
        >
          Pemberi disposisi : {item.pemberi_disposisi}
        </CustomText>
        <CustomText regular style={{ fontSize: 12, color: "#707070" }}>
          Tgl. Pengiriman :{" "}
          {moment(item.assigned_date).format("DD MMMM YYYY, hh:mm:ss")}
        </CustomText>

        <View style={styles.sparatorContent} />

        <TouchableOpacity
          style={styles.btnLihatSurat}
          onPress={() =>
            navigation.navigate("Detail Eoffice", {
              payload: item.id,
            })
          }
        >
          <CustomText style={styles.txtIzin}>Lihat Surat</CustomText>
        </TouchableOpacity>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <View style={styles.containerHeader}>
        <CustomText style={{ fontSize: 17 }}>Daftar Surat</CustomText>
      </View>

      <View style={styles.sparator} />

      {loading && <Loading />}

      {/* FlatList */}
      {!loading && data.length > 0 && (
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderItem}
        />
      )}

      {!loading && data.length < 1 && <Empty />}
    </View>
  );
};

export default Disposisi;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  card: {
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    elevation: 2,
    padding: 10,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  containerHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginVertical: 20,
  },
  sparator: {
    borderWidth: 0.3,
    borderColor: "#707070",
    marginHorizontal: 20,
    marginBottom: 15,
  },
  sparatorContent: {
    borderWidth: 0.5,
    borderColor: "#707070",
    marginVertical: 10,
  },
  btnLihatSurat: {
    paddingVertical: 5,
    paddingHorizontal: 20,
    backgroundColor: COLORS.blue,
    borderRadius: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignSelf: "flex-end",
  },
  txtIzin: {
    color: "white",
    fontSize: 12,
  },
});
