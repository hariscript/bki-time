import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Header } from "@react-navigation/native";
import { LinearGradient } from "expo-linear-gradient";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import { Feather } from "@expo/vector-icons";
import {
  Absence,
  AfterBreak,
  AuthLoading,
  Break,
  Camera,
  ClockIn,
  ClockOut,
  DetailPermission,
  EntryPermission,
  Faq,
  FormOnLeave,
  Home,
  ListPermission,
  Login,
  Management,
  OnLeave,
  Overtime,
  DetailOvertime,
  Personal,
  Eoffice,
  ListEoffice,
  DetailEoffice,
  Helpdesk,
  AddHelpdesk,
  AnswerHelpdesk,
  DetailHelpdesk,
  Salary,
  ManageAbsence,
  Summary,
  WelcomeAuth,
  ComingSoon,
  Tembusan,
  ApprovalEoffice,
  DisposisiKeluar,
  Disposisi,
  SentItem,
} from "../pages";
import TabBottom from "./bottomTab";
import { COLORS } from "../services";
import TopTabReportAbsence from "./topTabReportAbsence";
import TopTabManageAbsence from "./topTabManageAbsence";
const { width, height } = Dimensions.get("window");

const Stack = createNativeStackNavigator();

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="AuthLoading">
      <Stack.Screen
        name="AuthLoading"
        component={AuthLoading}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="WelcomeAuth"
        component={WelcomeAuth}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Coming Soon"
        component={ComingSoon}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="bottomTab"
        component={TabBottom}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Absence"
        component={Absence}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Camera"
        component={Camera}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Clock In"
        component={ClockIn}
        options={{
          animation: "fade",
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
          // headerRight: () => (
          //   <TouchableOpacity>
          //     <Feather name="refresh-cw" size={24} color="white" />
          //   </TouchableOpacity>
          // ),
        }}
      />

      <Stack.Screen
        name="Clock Out"
        component={ClockOut}
        options={{
          animation: "fade",
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Break"
        component={Break}
        options={{
          animation: "fade",
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="After Break"
        component={AfterBreak}
        options={{
          animation: "fade",
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Report"
        component={TopTabReportAbsence}
        options={{
          animation: "fade",
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Management"
        component={Management}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Personal"
        component={Personal}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="List Permission"
        component={ListPermission}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Izin",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Entry Permission"
        component={EntryPermission}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Buat",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Detail Permission"
        component={DetailPermission}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Detail",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />

      <Stack.Screen
        name="Salary"
        component={Salary}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Gaji",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Cuti"
        component={OnLeave}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Cuti",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Form Cuti"
        component={FormOnLeave}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Permohonan Cuti",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Lembur"
        component={Overtime}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Daftar Lembur",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Detail Lembur"
        component={DetailOvertime}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Detail Lembur",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Eoffice"
        component={Eoffice}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "E-Office",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="ListEoffice"
        component={ListEoffice}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "List Surat",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Tembusan"
        component={Tembusan}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Tembusan",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Approval E-Office"
        component={ApprovalEoffice}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Approval",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Sent Item"
        component={SentItem}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Approval",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Disposisi"
        component={Disposisi}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Disposisi",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Disposisi Keluar"
        component={DisposisiKeluar}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Disposisi Keluar",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Detail Eoffice"
        component={DetailEoffice}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Detail E-Office",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Helpdesk"
        component={Helpdesk}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Helpdesk",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="AddHelpdesk"
        component={AddHelpdesk}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Add Helpdesk",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="DetailHelpdesk"
        component={DetailHelpdesk}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Detail Helpdesk",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="AnswerHelpdesk"
        component={AnswerHelpdesk}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "AnswerHelpdesk",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="ManageAbsence"
        component={ManageAbsence}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Manage Absence",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Detail Absence"
        component={TopTabManageAbsence}
        options={{
          headerBackground: (props) => <GradientHeader {...props} />,
          headerTitleStyle: {
            color: "white",
          },
          headerTitle: "Detail Absensi",
          headerTintColor: "white",
          headerStyle: {
            backgroundColor: "transparent",
            height: Platform.OS === "ios" ? height * 20 : height * 0.07,
          },
          headerBackTitleVisible: false,
        }}
      />
    </Stack.Navigator>
  );
};

const GradientHeader = (props) => (
  // <View>
  <LinearGradient
    colors={COLORS.gradient}
    start={{ x: 0, y: 0 }}
    end={{ x: 1, y: 0 }}
    style={[
      { flex: 1, flexDirection: "row", alignItems: "center" },
      StyleSheet.absoluteFill,
    ]}
    // style={StyleSheet.absoluteFill}
    {...props}
  />
  // </View>
);

export default Router;
