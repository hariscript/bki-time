import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { Text, View } from "react-native";
import { DetailReport, Summary } from "../../pages";

const Tab = createMaterialTopTabNavigator();

const TopTabReportAbsence = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Summary"
        component={Summary}
        options={{ title: "Ringkasan" }}
      />
      <Tab.Screen
        name="Detail"
        component={DetailReport}
        options={{ title: "Detail" }}
      />
    </Tab.Navigator>
  );
};

export default TopTabReportAbsence;
