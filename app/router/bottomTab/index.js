import { StatusBar } from "expo-status-bar";
import React from "react";
import {
  Alert,
  Animated,
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useSafeAreaInsets } from "react-native-safe-area-context";

// Font Awesome Icons...
import {
  FontAwesome5,
  Ionicons,
  AntDesign,
  MaterialCommunityIcons,
  FontAwesome,
} from "@expo/vector-icons";
import { useRef } from "react";
import { LinearGradient } from "expo-linear-gradient";
import { ClockIn, ScanIcon } from "../../assets";
import { Faq, Home, Maps, Profile } from "../../pages";
import { COLORS } from "../../services";
import { CustomText } from "../../components";
import { useSelector } from "react-redux";
import moment from "moment";

const Tab = createBottomTabNavigator();

// Hiding Tab Names...
const TabBottom = ({ navigation }) => {
  const { clock_in, clock_out } = useSelector((state) => state.ProfileReducer);
  const insets = useSafeAreaInsets();
  // Animated Tab Indicator...
  const tabOffsetValue = useRef(new Animated.Value(0)).current;

  const currentTime = new Date();

  // set Clock In
  const clockIn = new Date();
  clockIn.setHours(7);
  clockIn.setMinutes(0);

  // set Break
  const breaks = new Date();
  breaks.setHours(11);
  breaks.setMinutes(0);

  // set After Break
  const afterBreak = new Date();
  afterBreak.setHours(13);
  afterBreak.setMinutes(0);

  // set After Break
  const clockOut = new Date();
  clockOut.setHours(16);
  clockOut.setMinutes(30);

  let currentPosition =
    currentTime >= clockIn && currentTime < breaks
      ? "Clock In"
      : currentTime >= breaks && currentTime < afterBreak
      ? "Break"
      : currentTime >= afterBreak && currentTime < clockOut
      ? "After Break"
      : "Clock Out";

  return (
    <>
      <Tab.Navigator
        screenOptions={{
          tabBarShowLabel: false,
          tabBarStyle: [
            {
              height: insets.bottom + 60,
            },
            null,
          ],
        }}
      >
        {
          // Tab Screens....
          // Tab ICons....
        }
        <Tab.Screen
          name={"Home"}
          component={Home}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View
                style={{
                  // centring Tab Button...
                  position: "absolute",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <FontAwesome5
                  name="home"
                  size={focused ? 20 : 18}
                  color={focused ? "#0b4ca4" : "gray"}
                ></FontAwesome5>
                <Text
                  style={{
                    color: focused ? "#0b4ca4" : "gray",
                    fontSize: focused ? 14 : 12,
                    marginTop: Platform.OS === "ios" ? 5 : 0,
                  }}
                >
                  Home
                </Text>
              </View>
            ),
          }}
          listeners={({ navigation, route }) => ({
            // Onpress Update....
            tabPress: (e) => {
              Animated.spring(tabOffsetValue, {
                toValue: 0,
                useNativeDriver: true,
              }).start();
            },
          })}
        ></Tab.Screen>

        <Tab.Screen
          name={"Faq"}
          component={Faq}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View
                style={{
                  // centring Tab Button...
                  position: "absolute",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <AntDesign
                  name="customerservice"
                  size={focused ? 23 : 21}
                  color={focused ? "#0b4ca4" : "gray"}
                ></AntDesign>
                <Text
                  style={{
                    color: focused ? "#0b4ca4" : "gray",
                    fontSize: focused ? 14 : 12,
                    marginTop: Platform.OS === "ios" ? 5 : 0,
                  }}
                >
                  Faq
                </Text>
              </View>
            ),
          }}
          listeners={({ navigation, route }) => ({
            // Onpress Update....
            tabPress: (e) => {
              Animated.spring(tabOffsetValue, {
                toValue: getWidth(),
                useNativeDriver: true,
              }).start();
            },
          })}
        ></Tab.Screen>

        {
          // Extra Tab Screen For Action Button..
        }

        <Tab.Screen
          name={"ActionButton"}
          component={EmptyScreen}
          options={{
            tabBarIcon: ({ focused }) => (
              <TouchableOpacity
                onPress={() => {
                  if (
                    clock_in &&
                    moment(clock_in).format("DD MMMM YYYY") ==
                      moment(new Date()).format("DD MMMM YYYY") &&
                    currentPosition === "Clock In"
                  ) {
                    Alert.alert(
                      "Info",
                      "Anda sudah absen Clock In hari ini, apakah Anda ingin mengulang?",
                      [
                        {
                          text: "Cancel",
                          onPress: () => console.log("Cancel Pressed"),
                          style: "cancel",
                        },
                        {
                          text: "OK",
                          onPress: () =>
                            navigation.navigate(currentPosition, {
                              from: "bottomTab",
                            }),
                        },
                      ],
                      {
                        cancelable: false,
                      }
                    );
                  } else if (
                    clock_out &&
                    moment(clock_out).format("DD MMMM YYYY") ==
                      moment(new Date()).format("DD MMMM YYYY") &&
                    currentPosition === "Clock Out"
                  ) {
                    Alert.alert(
                      "Info",
                      "Anda sudah absen Clock Out hari ini, apakah Anda ingin mengulang?",
                      [
                        {
                          text: "Cancel",
                          onPress: () => console.log("Cancel Pressed"),
                          style: "cancel",
                        },
                        {
                          text: "OK",
                          onPress: () =>
                            navigation.navigate(currentPosition, {
                              from: "bottomTab",
                            }),
                        },
                      ],
                      {
                        cancelable: false,
                      }
                    );
                  } else {
                    navigation.navigate(currentPosition, {
                      from: "bottomTab",
                    });
                  }
                }}
              >
                <LinearGradient
                  colors={COLORS.gradient}
                  style={{
                    width: 80,
                    height: 80,
                    borderRadius: 50,
                    justifyContent: "center",
                    alignItems: "center",
                    borderWidth: 5,
                    borderColor: "white",
                    marginBottom: Platform.OS == "android" ? 50 : 30,
                  }}
                >
                  <Image
                    source={ClockIn}
                    style={{
                      width: 35,
                      height: 35,
                      tintColor: "white",
                    }}
                  ></Image>
                  <CustomText style={{ fontSize: 8, color: "white" }}>
                    {currentPosition}
                  </CustomText>
                </LinearGradient>
              </TouchableOpacity>
            ),
          }}
        ></Tab.Screen>

        <Tab.Screen
          name={"Maps"}
          component={Maps}
          options={{
            tabBarIcon: ({ focused }) => (
              <View
                style={{
                  // centring Tab Button...
                  position: "absolute",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <FontAwesome
                  name="map-o"
                  size={focused ? 20 : 18}
                  color={focused ? "#0b4ca4" : "gray"}
                />
                <Text
                  style={{
                    color: focused ? "#0b4ca4" : "gray",
                    fontSize: focused ? 14 : 12,
                    marginTop: Platform.OS === "ios" ? 5 : 0,
                  }}
                >
                  Maps
                </Text>
              </View>
            ),
          }}
          listeners={({ navigation, route }) => ({
            // Onpress Update....
            tabPress: (e) => {
              Animated.spring(tabOffsetValue, {
                toValue: getWidth() * 3,
                useNativeDriver: true,
              }).start();
            },
          })}
        ></Tab.Screen>

        <Tab.Screen
          name={"Profile"}
          component={Profile}
          options={{
            headerShown: false,
            tabBarIcon: ({ focused }) => (
              <View
                style={{
                  // centring Tab Button...
                  position: "absolute",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <FontAwesome5
                  name="user-alt"
                  size={focused ? 20 : 18}
                  color={focused ? "#0b4ca4" : "gray"}
                ></FontAwesome5>
                <Text
                  style={{
                    color: focused ? "#0b4ca4" : "grey",
                    fontSize: focused ? 14 : 12,
                    marginTop: Platform.OS === "ios" ? 5 : 0,
                  }}
                >
                  Profile
                </Text>
              </View>
            ),
          }}
          listeners={({ navigation, route }) => ({
            // Onpress Update....
            tabPress: (e) => {
              Animated.spring(tabOffsetValue, {
                toValue: getWidth() * 4,
                useNativeDriver: true,
              }).start();
            },
          })}
        ></Tab.Screen>
      </Tab.Navigator>

      <Animated.View
        style={{
          width: Platform.OS === "ios" ? getWidth() - 5 : getWidth() - 20,
          position: "absolute",
          bottom: insets.bottom + 60,
          // Horizontal Padding = 20...
          left: Platform.OS === "ios" ? 0 : 10,
          transform: [{ translateX: tabOffsetValue }],
        }}
      >
        <LinearGradient
          colors={COLORS.gradient}
          style={{
            height: 5,
            borderRadius: Platform.OS === "ios" ? 5 : 20,
          }}
        ></LinearGradient>
      </Animated.View>
    </>
  );
};

export default TabBottom;

function getWidth() {
  let width = Dimensions.get("window").width;

  // Total five Tabs...
  return width / 5;
}

function EmptyScreen() {
  return (
    <View
      style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
    ></View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
