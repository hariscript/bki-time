import React from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import { Text, View } from "react-native";
import { ManageDetail, ManageSummary } from "../../pages";

const Tab = createMaterialTopTabNavigator();

const TopTabManageAbsence = ({ route }) => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Summary"
        component={ManageSummary}
        options={{ title: "Ringkasan" }}
        // initialParams={{ route }}
        initialParams={{ nup_personil: route.params.nup_personil }}
      />
      <Tab.Screen
        name="Detail"
        component={ManageDetail}
        options={{ title: "Detail" }}
        initialParams={{ nup_personil: route.params.nup_personil }}
      />
    </Tab.Navigator>
  );
};

export default TopTabManageAbsence;
