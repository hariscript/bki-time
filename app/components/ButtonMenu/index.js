import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  Dimensions
} from "react-native";
import {
  AntDesign,
  Entypo,
  EvilIcons,
  Feather,
  FontAwesome,
  FontAwesome5,
  Fontisto,
  Foundation,
  Ionicons,
  MaterialCommunityIcons,
  MaterialIcons,
  Octicons,
  SimpleLineIcons,
  Zocial
} from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import CustomText from "../Text";

const ButtonMenu = ({
  onPress,
  type,
  name,
  text,
  size,
  style,
  styleText,
  color,
  gradient,
  gambar
}) => {
  return gradient ? (
    <TouchableOpacity
      style={[styles.menuRowButton, style]}
      onPress={onPress}
      activeOpacity={0.8}
    >
      <LinearGradient
        colors={["#4c669f", "#3b5998", "#192f6a"]}
        style={[styles.menuRowButton, style]}
      >
        {type == "AntDesign" ? (
          <AntDesign
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Entypo" ? (
          <Entypo name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "EvilIcons" ? (
          <EvilIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Feather" ? (
          <Feather name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "FontAwesome" ? (
          <FontAwesome
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "FontAwesome5" ? (
          <FontAwesome5
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Fontisto" ? (
          <Fontisto name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "Foundation" ? (
          <Foundation
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "MaterialCommunityIcons" ? (
          <MaterialCommunityIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "MaterialIcons" ? (
          <MaterialIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Octicons" ? (
          <Octicons name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "SimpleLineIcons" ? (
          <SimpleLineIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Ionicons" ? (
          <Ionicons name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : (
          <Zocial name={name} size={size} color={color ? color : "#0b4ca4"} />
        )}
        <CustomText style={[styles.text, styleText]}>{text}</CustomText>
      </LinearGradient>
    </TouchableOpacity>
  ) : (
    <>
      <TouchableOpacity style={[style]} onPress={onPress} activeOpacity={0.8}>
        <Image
          source={gambar}
          style={{ height: 60, width: 60, alignSelf: "center" }}
          resizeMode="contain"
        />
        <CustomText style={[styles.text, styleText]} regular>
          {text}
        </CustomText>

        {/* Second Option */}
        
        {/* <Image
          source={gambar}
          style={{ height: 95, width: 95, alignSelf: "center" }}
          resizeMode="contain"
        />
        <CustomText style={[styles.text, styleText]} regular>
          {text}
        </CustomText> */}
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    marginTop: 5,
    fontSize: 10
  },
  viewIcon: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default ButtonMenu;
