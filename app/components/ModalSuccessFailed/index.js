import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import {
  Dimensions,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from "react-native";
import { Entypo } from "@expo/vector-icons";
import AnimatedLottieView from "lottie-react-native";

const windowWidth = Dimensions.get("window").width;
const windowHeight = Dimensions.get("window").height;

const ModalSuccessFailed = ({
  type,
  visible,
  onRequestClose,
  onPress,
  onPressExit,
  text
}) => {
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={visible}
      onRequestClose={onRequestClose}
    >
      <View style={styles.centeredView}>
        <View style={[styles.modalView, {}]}>
          <View style={styles.exitButtonView}>
            <TouchableOpacity onPress={onPressExit}>
              <Entypo name="circle-with-cross" size={30} color="black" />
            </TouchableOpacity>
          </View>
          <View style={styles.contentView}>
            <View
              style={[
                styles.contentIcon
                // { borderColor: type == "success" ? "green" : "red" }
              ]}
            >
              <AnimatedLottieView
                source={
                  type == "success"
                    ? require("../../assets/lottie/success.json")
                    : require("../../assets/lottie/error.json")
                }
                loop={false}
                autoPlay
                duration={1500}
                style={{ height: type == "success" ? 150: 250 }}
              />
            </View>
            <Text
              style={[
                styles.contentTitle,
                {
                  color: type == "success" ? "green" : "red",
                  fontSize: type == "success" ? 25 : 20
                }
              ]}
            >
              {type == "success" ? "Yeay!" : "Oops!"}
            </Text>
            <View
              style={[
                styles.contentMessage,
                { borderColor: type == "success" ? "green" : "red" }
              ]}
            />
            {type == "success" ? (
              <Text style={{ textAlign: "center", marginTop: 30 }}>{text}</Text>
            ) : (
              <Text style={{ textAlign: "center", marginTop: 30 }}>{text}</Text>
            )}
          </View>

          <TouchableOpacity
            style={[styles.touchAble, { marginTop: 30 }]}
            onPress={onPress}
          >
            <LinearGradient
              colors={["#4c669f", "#3b5998", "#192f6a"]}
              style={styles.linearGradientViews}
            >
              <Text
                style={{ color: "white", fontSize: 15, fontWeight: "bold" }}
              >
                OK
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default ModalSuccessFailed;

const styles = StyleSheet.create({
  touchAble: {
    borderRadius: 15
  },
  linearGradientViews: {
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    padding: 10,
    borderRadius: 10
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    width: windowWidth - 100,
    // height: windowHeight / 2,
    margin: 20,
    padding: 20,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  exitButtonView: {
    alignItems: "flex-end",
    marginBottom: 30,
    marginTop: -30,
    marginRight: -30
  },
  contentView: {
    justifyContent: "center",
    alignItems: "center"
  },
  contentIcon: {
    justifyContent: "center",
    alignItems: "center",
    height: 100,
    width: 100,
    // borderRadius: 100 / 2,
    // borderWidth: 1
  },
  contentTitle: {
    marginTop: 10,
    fontWeight: "bold"
  },
  contentMessage: {
    height: 1,
    borderWidth: 1,
    width: 100,
    marginTop: 15
  }
});
