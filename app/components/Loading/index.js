import AnimatedLottieView from "lottie-react-native";
import React, { useEffect, useState } from "react";
import { Image, StyleSheet, View } from "react-native";

const Loading = ({ isLoading }) => {
  return (
    <View
      style={[
        styles.container,
        { justifyContent: "center", alignItems: "center" }
      ]}
    >
      <AnimatedLottieView
        source={require("../../assets/lottie/loading.json")}
        loop
        autoPlay
        // style={{ width: 200, height: 200 }}
      />
      {/* <Image
        source={require("../../assets/lottie/loading.json")}
        style={{ width: 100, height: 100 }}
      /> */}
    </View>
  );
};

export default Loading;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});
