import React, { useEffect, useRef } from "react";
import { Animated, StyleSheet, View } from "react-native";

const LoadingNews = ({ style }) => {
  const circleAnimatedValue = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    circleAnimated();

    return () => {};
  }, []);

  const circleAnimated = () => {
    circleAnimatedValue.setValue(0);
    Animated.timing(circleAnimatedValue, {
      toValue: 1,
      duration: 350,
      useNativeDriver: false
    }).start(() => {
      setTimeout(() => {
        circleAnimated();
      }, 1000);
    });
  };

  const translateX3 = circleAnimatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [-10, 400]
  });
  return (
    <View style={[styles.card, style]}>
      <View
        style={[
          styles.contentCardSquare,
          { backgroundColor: "#ECEFF1", overflow: "hidden" }
        ]}
      >
        <Animated.View
          style={{
            width: "100%",
            height: 100,
            backgroundColor: "white",
            opacity: 0.5,
            transform: [{ translateX: translateX3 }]
          }}
        ></Animated.View>
      </View>
      <View
        style={{
          backgroundColor: "#ECEFF1",
          width: "80%",
          height: 25,
          margin: 10
        }}
      >
        <Animated.View
          style={{
            width: "20%",
            height: "100%",
            backgroundColor: "white",
            opacity: 0.2,
            transform: [{ translateX: translateX3 }]
          }}
        ></Animated.View>
      </View>
      <View
        style={{
          backgroundColor: "#ECEFF1",
          width: "40%",
          height: 15,
          marginHorizontal: 10,
          marginBottom: 5
        }}
      >
        <Animated.View
          style={{
            width: "20%",
            height: "100%",
            backgroundColor: "white",
            opacity: 0.2,
            transform: [{ translateX: translateX3 }]
          }}
        ></Animated.View>
      </View>
      <View
        style={{
          backgroundColor: "#ECEFF1",
          width: "30%",
          height: 15,
          margin: 10,
          alignSelf: "flex-end"
        }}
      >
        <Animated.View
          style={{
            width: "100%",
            height: 100,
            backgroundColor: "white",
            opacity: 0.2,
            transform: [{ translateX: translateX3 }]
          }}
        ></Animated.View>
      </View>
    </View>
  );
};

export default LoadingNews;

const styles = StyleSheet.create({
  card: {
    marginTop: 15,
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    // flexDirection: "row",
    elevation: 5,
    width: 300
  },
  readmore: {
    // backgroundColor: "blue",
    paddingVertical: 3,
    width: "40%",
    borderRadius: 10,
    marginTop: 5,
    alignItems: "center"
  },
  contentCardLeft: {
    flex: 1,
    justifyContent: "center",
    alignItems: "flex-start"
  },
  contentCardSquare: {
    // backgroundColor: "green",
    width: "100%",
    height: 100,
    borderRadius: 10,
    justifyContent: "center"
  }
});
