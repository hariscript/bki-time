import React from "react";
import { Dimensions, View } from "react-native";
import CustomText from "../Text";

const BarCharts = ({ value, colors, title, horizontal, vertical }) => {
  const percent = String((Number(value) / 20) * 100);

  if (vertical) {
    return (
      <View style={{ width: Dimensions.get("window").width / 11 }}>
        <CustomText style={{ textAlign: "center", marginBottom: 5 }}>
          {value}
        </CustomText>
        <View
          style={{
            height: 150,
            backgroundColor: "#eeeeee",
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
            justifyContent: "flex-end"
          }}
        >
          <View
            style={{
              height: percent + "%",
              backgroundColor: colors,
              borderTopLeftRadius: 10,
              borderTopRightRadius: 10
            }}
          />
        </View>
        <CustomText style={{ textAlign: "center", fontSize: 8, marginTop: 10 }}>
          {title}
        </CustomText>
      </View>
    );
  }

  if (horizontal) {
    return (
      <View
        style={{
          flexDirection: "row",
          marginTop: 20,
          marginHorizontal: 20,
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            flex: 0.8,
            justifyContent: "flex-end",
            flexDirection: "row"
          }}
        >
          <CustomText regular style={{fontSize: 11}}>{title}</CustomText>
        </View>
        <View
          style={{
            flex: 2,
            height: 30,
            backgroundColor: "#eeeeee",
            marginHorizontal: 10,
            borderTopRightRadius: 8,
            borderBottomRightRadius: 8
          }}
        >
          <View
            style={{
              width: percent + "%",
              height: 30,
              backgroundColor: colors,
              // marginHorizontal: 10,
              borderTopRightRadius: 8,
              borderBottomRightRadius: 8
            }}
          />
        </View>
        <View
          style={{
            flex: 0.2,
            justifyContent: "flex-end",
            flexDirection: "row"
            // backgroundColor: "yellow"
          }}
        >
          <CustomText regular>{value}</CustomText>
        </View>
      </View>
    );
  }
};

export default BarCharts;
