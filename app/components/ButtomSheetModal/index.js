import React, { useCallback } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { AntDesign, Entypo } from "@expo/vector-icons";
import { BottomSheetModal, BottomSheetFlatList } from "@gorhom/bottom-sheet";

import CustomText from "../Text";
import DateTimePicker from "@react-native-community/datetimepicker";
import { useSafeAreaInsets } from "react-native-safe-area-context";

const ButtomSheetModal = ({
  bottomSheetRef,
  onDismiss,
  title,
  onPressRender,
  onPressClose,
  data,
  date,
  onChangeDate,
  type,
  onPressOk,
  renderItems,
  trigger
}) => {
  const insets = useSafeAreaInsets();
  const snapPoints = ["50%"];

  const renderItem = useCallback(
    ({ item }) => (
      <TouchableOpacity onPress={() => onPressRender(item)}>
        <View style={styles.itemContainer}>
          <CustomText regular>{item}</CustomText>
        </View>
      </TouchableOpacity>
    ),
    []
  );
  
  return (
    <View>
      <BottomSheetModal
        ref={bottomSheetRef}
        snapPoints={snapPoints}
        backgroundStyle={styles.bottomSheetBackground}
        onDismiss={onDismiss}
      >
        <View style={{ flex: 1, paddingHorizontal: 15 }}>
          <View style={styles.bottomSheetHeader}>
            <TouchableOpacity
              onPress={onPressClose}
              style={{ marginRight: 10 }}
            >
              <Entypo name="cross" size={20} color="grey" />
            </TouchableOpacity>
            <CustomText style={{ fontSize: 15 }}>{title}</CustomText>
          </View>

          <View style={styles.sparator} />

          {type === "list" && (
            <BottomSheetFlatList
              data={data}
              keyExtractor={
                renderItems && trigger === 0 ? (i) => i.id : (i) => i
              }
              renderItem={
                renderItems && trigger === 0 ? renderItems : renderItem
              }
              showsVerticalScrollIndicator={false}
              style={{ marginBottom: insets.bottom }}
            />
          )}

          {type === "date" && (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode="date"
                is24Hour={true}
                display="spinner"
                onChange={onChangeDate}
              />

              <TouchableOpacity style={styles.btnOk} onPress={onPressOk}>
                <CustomText style={{ color: "white" }}>Ok</CustomText>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </BottomSheetModal>
    </View>
  );
};

export default ButtomSheetModal;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  bottomSheetBackground: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    borderRadius: 20
  },

  bottomSheetHeader: {
    flexDirection: "row",
    marginHorizontal: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  sparator: {
    borderWidth: 1,
    borderColor: "#eee",
    marginBottom: 10
  },
  itemContainer: {
    padding: 6,
    marginVertical: 6,
    marginHorizontal: 20
  },
  btnOk: {
    justifyContent: "center",
    alignItems: "center",
    width: "60%",
    backgroundColor: "#1EACFF",
    paddingVertical: 10,
    borderRadius: 10,
    marginTop: 10
  }
});
