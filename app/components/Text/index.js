import React from "react";
import { StyleSheet, Text } from "react-native";

const CustomText = ({ children, style, regular, bold }) => {
  return (
    <Text style={regular ? [styles.regular, style] : [styles.bold, style]}>
      {children}
    </Text>
  );
};

export default CustomText;
const styles = StyleSheet.create({
  regular: {
    fontFamily: "Montserrat-Regular"
  },
  bold: {
    fontFamily: "Montserrat-Bold"
  }
});
