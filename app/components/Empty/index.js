import React from "react";
import { Image, StyleSheet, View } from "react-native";
import { Empty } from "../../assets";
import CustomText from "../Text";

const DataEmpty = ({ isLoading }) => {
  return (
    <View
      style={[
        styles.container,
        { justifyContent: "center", alignItems: "center" }
      ]}
    >
      <Image source={Empty} style={{ width: 300, height: 300 }} />
    </View>
  );
};

export default DataEmpty;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});
