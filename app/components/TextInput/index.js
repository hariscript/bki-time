import React from "react";
import { Platform, StyleSheet, Text, TextInput, View } from "react-native";

const TextInputCustom = ({
  judul,
  editable,
  maxLength,
  multiline,
  numberOfLines,
  onChangeText,
  value,
  placeholder,
  textAlignVertical,
  style,
  required,
  keyboardType
}) => {
  return judul == undefined ? (
    <TextInput
      editable={editable}
      maxLength={maxLength}
      multiline={multiline}
      numberOfLines={numberOfLines}
      onChangeText={onChangeText}
      value={value}
      placeholder={placeholder}
      textAlignVertical={textAlignVertical}
      style={Platform.OS === "ios" ? [style, { paddingBottom: 10 }] : style}
      keyboardType={keyboardType}
    />
  ) : required ? (
    <View>
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.text}>{judul}</Text>
        <Text style={{ color: "red", marginLeft: 3 }}>*</Text>
      </View>
      <TextInput
        editable={editable}
        maxLength={maxLength}
        multiline={multiline}
        numberOfLines={numberOfLines}
        onChangeText={onChangeText}
        value={value}
        placeholder={placeholder}
        textAlignVertical={textAlignVertical}
        style={Platform.OS === "ios" ? [style, { paddingBottom: 10 }] : style}
        keyboardType={keyboardType}
      />
    </View>
  ) : (
    <View>
      <Text style={styles.text}>{judul}</Text>
      <TextInput
        editable={editable}
        maxLength={maxLength}
        multiline={multiline}
        numberOfLines={numberOfLines}
        onChangeText={onChangeText}
        value={value}
        placeholder={placeholder}
        textAlignVertical={textAlignVertical}
        style={Platform.OS === "ios" ? [style, { paddingBottom: 10 }] : style}
        keyboardType={keyboardType}
      />
    </View>
  );
};

export default TextInputCustom;
const styles = StyleSheet.create({
  text: {
    fontSize: 15,
    fontWeight: "bold",
    marginBottom: Platform.OS === "ios" ? 7 : 5
  }
});
