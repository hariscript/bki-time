import React from "react";
import { ActivityIndicator } from "react-native";

const LoadingIndicator = ({ style }) => {
  return (
    <ActivityIndicator color={"blue"} size={"large"} style={style} />
    // </View>
  );
};

export default LoadingIndicator;
