import React, { useRef } from "react";
import { ActivityIndicator, Dimensions, SafeAreaView } from "react-native";
import { WebView } from "react-native-webview";
import { useSelector } from "react-redux";

const { width, height } = Dimensions.get("window");

const MapsWebView = ({ latitude, longitude, style }) => {
  const { latitude_center, longitude_center } = useSelector(
    (state) => state.OfficeReducer
  );
  const ref = useRef();

  const loadingView = () => {
    return (
      // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ActivityIndicator
        color={"blue"}
        size={"large"}
        style={{
          // flex: 1,
          height: "100%",
          width: "100%",
          position: "absolute",
          justifyContent: "center",
          alignItems: "center"
        }}
      />
      // </View>
    );
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <WebView
        ref={ref}
        originWhitelist={["*"]}
        source={{
          uri:
            "https://dev-apibkitime.bki.co.id/maps.php?lat=" +
            latitude +
            "&long=" +
            longitude +
            "&lat_office=" +
            latitude_center +
            "&long_office=" +
            longitude_center
        }}
        style={[style, { width: 800, height }]}
        scrollEnabled={true}
        geolocationEnabled={true}
        automaticallyAdjustContentInsets={true}
        renderLoading={loadingView}
        startInLoadingState={true}
        // allowFileAccess={true}
        // scalesPageToFit={true}
      />
    </SafeAreaView>
  );
};

export default MapsWebView;
