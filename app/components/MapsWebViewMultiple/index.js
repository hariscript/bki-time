import React, { useRef } from "react";
import { ActivityIndicator, Dimensions, SafeAreaView } from "react-native";
import { WebView } from "react-native-webview";
import { useSelector } from "react-redux";

const { width, height } = Dimensions.get("window");

const MapsWebViewMultiple = ({ user_loc, office_loc, style }) => {
  const { latitude_center, longitude_center } = useSelector(
    (state) => state.OfficeReducer
  );
  const ref = useRef();

  const loadingView = () => {
    return (
      // <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
      <ActivityIndicator
        color={"blue"}
        size={"large"}
        style={{
          // flex: 1,
          height: "100%",
          width: "100%",
          position: "absolute",
          justifyContent: "center",
          alignItems: "center"
        }}
      />
      // </View>
    );
  };

  // const locations = {
  //   office_loc: office_loc,
  //   user: user_loc
  // };

  const locations = {
    // current_loc: [latitude, longitude],
    office_loc: [latitude_center, longitude_center],
    user: user_loc
  };

  // Mengubah array locations menjadi string JSON yang akan dikirimkan melalui URL
  const locationsJSON = JSON.stringify(locations);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <WebView
        ref={ref}
        originWhitelist={["*"]}
        source={{
          uri: `https://dev-apibkitime.bki.co.id/maps_multiple_marker.php?locations=${encodeURIComponent(
            locationsJSON
          )}`
        }}
        style={[style, { width: 700, height, alignSelf: 'center' }]}
        scrollEnabled={true}
        geolocationEnabled={true}
        automaticallyAdjustContentInsets={true}
        renderLoading={loadingView}
        startInLoadingState={true}
        // allowFileAccess={true}
        // scalesPageToFit={true}
      />
    </SafeAreaView>
  );
};

export default MapsWebViewMultiple;
