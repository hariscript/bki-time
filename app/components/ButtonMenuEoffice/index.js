import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  View,
  Dimensions,
  ActivityIndicator
} from "react-native";
import {
  AntDesign,
  Entypo,
  EvilIcons,
  Feather,
  FontAwesome,
  FontAwesome5,
  Fontisto,
  Foundation,
  Ionicons,
  MaterialCommunityIcons,
  MaterialIcons,
  Octicons,
  SimpleLineIcons,
  Zocial
} from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import CustomText from "../Text";

const ButtonMenuEoffice = ({
  onPress,
  type,
  name,
  text,
  size,
  style,
  styleText,
  color,
  gradient,
  gambar,
  count,
  loading
}) => {
  return gradient ? (
    <TouchableOpacity
      style={[styles.menuRowButton, style]}
      onPress={onPress}
      activeOpacity={0.8}
    >
      <LinearGradient
        colors={["#4c669f", "#3b5998", "#192f6a"]}
        style={[styles.menuRowButton, style]}
      >
        {type == "AntDesign" ? (
          <AntDesign
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Entypo" ? (
          <Entypo name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "EvilIcons" ? (
          <EvilIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Feather" ? (
          <Feather name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "FontAwesome" ? (
          <FontAwesome
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "FontAwesome5" ? (
          <FontAwesome5
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Fontisto" ? (
          <Fontisto name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "Foundation" ? (
          <Foundation
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "MaterialCommunityIcons" ? (
          <MaterialCommunityIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "MaterialIcons" ? (
          <MaterialIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Octicons" ? (
          <Octicons name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : type == "SimpleLineIcons" ? (
          <SimpleLineIcons
            name={name}
            size={size}
            color={color ? color : "#0b4ca4"}
          />
        ) : type == "Ionicons" ? (
          <Ionicons name={name} size={size} color={color ? color : "#0b4ca4"} />
        ) : (
          <Zocial name={name} size={size} color={color ? color : "#0b4ca4"} />
        )}
        <CustomText style={[styles.text, styleText]}>{text}</CustomText>
      </LinearGradient>
    </TouchableOpacity>
  ) : (
    <>
      <TouchableOpacity style={[style]} onPress={onPress} activeOpacity={0.8}>
        <View
          style={{
            alignItems: "flex-end",
            marginBottom: -15,
            marginRight: 15,
            zIndex: 1
          }}
        >
          {loading ? (
            <View
              style={{
                backgroundColor: "red",
                padding: 3,
                borderRadius: 15
              }}
            >
              <ActivityIndicator color={"white"} />
            </View>
          ) : (
            <CustomText
              style={[
                styles.textBadges,
                count > 0 && {
                  backgroundColor: "red"
                }
              ]}
            >
              {count}
            </CustomText>
          )}
        </View>
        <Image
          source={gambar}
          style={{ height: 60, width: 60, alignSelf: "center", zIndex: 0 }}
          resizeMode="contain"
        />
        <CustomText style={[styles.text, styleText]} regular>
          {text}
        </CustomText>

        {/* Second Option */}

        {/* <Image
          source={gambar}
          style={{ height: 95, width: 95, alignSelf: "center" }}
          resizeMode="contain"
        />
        <CustomText style={[styles.text, styleText]} regular>
          {text}
        </CustomText> */}
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    marginTop: 5,
    fontSize: 10
  },
  textBadges: {
    fontSize: 10,
    padding: 5,
    borderRadius: 15,
    textAlign: "center",
    color: "white"
  },
  viewIcon: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    justifyContent: "center",
    alignItems: "center"
  }
});

export default ButtonMenuEoffice;
