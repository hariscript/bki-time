import { Persistor, Store } from "./store";
import {
  SET_LOGGED,
  SET_KEY,
  SET_EMAIL,
  SET_JABATAN,
  SET_LATITUDE_RUMAH,
  SET_LONGITUDE_RUMAH,
  SET_NAME,
  SET_NUP,
  SET_PHOTO,
  SET_UNIT_KERJA,
  SET_CLOCK_IN,
  SET_CLOCK_OUT,
  SET_LONG_LAT_OFFICE,
  SET_TIME_WORKED,
  SET_TRIGGER_COLOR,
} from "./action";

export {
  SET_LOGGED,
  SET_KEY,
  SET_EMAIL,
  SET_JABATAN,
  SET_LATITUDE_RUMAH,
  SET_LONGITUDE_RUMAH,
  SET_NAME,
  SET_NUP,
  SET_PHOTO,
  SET_UNIT_KERJA,
  SET_CLOCK_IN,
  SET_CLOCK_OUT,
  SET_LONG_LAT_OFFICE,
  SET_TIME_WORKED,
  SET_TRIGGER_COLOR,
  Store,
  Persistor,
};
