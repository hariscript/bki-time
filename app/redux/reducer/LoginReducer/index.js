const initialStateHome = {
  isLogged: false,
  appID: "14e463aa21a847a729ca399899ea6961",
  key: ""
};

// reducer
export const LoginReducer = (state = initialStateHome, action) => {
  switch (action.type) {
    case "SET_LOGGED":
      return {
        ...state,
        isLogged: action.inputValue
      };

    case "SET_KEY":
      return {
        ...state,
        key: action.inputValue
      };

    default:
      return state;
  }
};
