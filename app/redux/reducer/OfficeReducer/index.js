const initialStateHome = {
  latitude_center: 0,
  longitude_center: 0,
  longitude1: 0,
  latitude1: 0,
  longitude2: 0,
  latitude2: 0,
  longitude3: 0,
  latitude3: 0,
  longitude4: 0,
  latitude4: 0
};

// reducer
export const OfficeReducer = (state = initialStateHome, action) => {
  switch (action.type) {
    case "SET_LONG_LAT_OFFICE":
      return {
        ...state,
        latitude_center: action.inputValue.latitude_center,
        longitude_center: action.inputValue.longitude_center,
        longitude1: action.inputValue.longitude1,
        latitude1: action.inputValue.latitude1,
        longitude2: action.inputValue.longitude2,
        latitude2: action.inputValue.latitude2,
        longitude3: action.inputValue.longitude3,
        latitude3: action.inputValue.latitude3,
        longitude4: action.inputValue.longitude4,
        latitude4: action.inputValue.latitude4
      };

    default:
      return state;
  }
};
