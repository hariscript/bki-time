const initialStateHome = {
  nup: "",
  nama: "",
  unit_kerja: "",
  jabatan: "",
  latitude_rumah: "",
  logitude_rumah: "",
  photo: "",
  email: "",
  clock_in: null,
  clock_out: null,
  time_worked: null,
  trigger_color: "black",
};

// reducer
export const ProfileReducer = (state = initialStateHome, action) => {
  switch (action.type) {
    case "SET_NUP":
      return {
        ...state,
        nup: action.inputValue,
      };

    case "SET_NAME":
      return {
        ...state,
        nama: action.inputValue,
      };

    case "SET_DEVICE_ID":
      return {
        ...state,
        device_id: action.inputValue,
      };

    case "SET_UNIT_KERJA":
      return {
        ...state,
        unit_kerja: action.inputValue,
      };

    case "SET_JABATAN":
      return {
        ...state,
        jabatan: action.inputValue,
      };

    case "SET_LATITUDE_RUMAH":
      return {
        ...state,
        latitude_rumah: action.inputValue,
      };

    case "SET_LONGITUDE_RUMAH":
      return {
        ...state,
        logitude_rumah: action.inputValue,
      };

    case "SET_PHOTO":
      return {
        ...state,
        photo: action.inputValue,
      };

    case "SET_EMAIL":
      return {
        ...state,
        email: action.inputValue,
      };
    case "SET_CLOCK_IN":
      return {
        ...state,
        clock_in: action.inputValue,
      };
    case "SET_CLOCK_OUT":
      return {
        ...state,
        clock_out: action.inputValue,
      };
    case "SET_TIME_WORKED":
      return {
        ...state,
        time_worked: action.inputValue,
      };

    case "SET_TRIGGER_COLOR":
      return {
        ...state,
        trigger_color: action.inputValue,
      };

    default:
      return state;
  }
};
