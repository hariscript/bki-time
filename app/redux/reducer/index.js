import { combineReducers } from "redux";
import { LoginReducer } from "./LoginReducer";
import { ProfileReducer } from "./ProfileReducer";
import { OfficeReducer } from "./OfficeReducer";

const reducer = combineReducers({
  LoginReducer,
  ProfileReducer,
  OfficeReducer
});

export default reducer;
