const SET_LONG_LAT_OFFICE = (value) => {
  return { type: "SET_LONG_LAT_OFFICE", inputValue: value };
};

export { SET_LONG_LAT_OFFICE };
