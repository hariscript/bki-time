const SET_LOGGED = (value) => {
  return { type: "SET_LOGGED", inputValue: value };
};

const SET_KEY = (value) => {
  return { type: "SET_KEY", inputValue: value };
};

export { SET_LOGGED, SET_KEY };
