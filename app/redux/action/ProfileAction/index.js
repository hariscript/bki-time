const SET_NUP = (value) => {
  return { type: "SET_NUP", inputValue: value };
};

const SET_NAME = (value) => {
  return { type: "SET_NAME", inputValue: value };
};

const SET_UNIT_KERJA = (value) => {
  return { type: "SET_UNIT_KERJA", inputValue: value };
};

const SET_JABATAN = (value) => {
  return { type: "SET_JABATAN", inputValue: value };
};

const SET_LATITUDE_RUMAH = (value) => {
  return { type: "SET_LATITUDE_RUMAH", inputValue: value };
};

const SET_LONGITUDE_RUMAH = (value) => {
  return { type: "SET_LONGITUDE_RUMAH", inputValue: value };
};

const SET_PHOTO = (value) => {
  return { type: "SET_PHOTO", inputValue: value };
};

const SET_EMAIL = (value) => {
  return { type: "SET_EMAIL", inputValue: value };
};
const SET_CLOCK_IN = (value) => {
  return { type: "SET_CLOCK_IN", inputValue: value };
};
const SET_CLOCK_OUT = (value) => {
  return { type: "SET_CLOCK_OUT", inputValue: value };
};

const SET_TIME_WORKED = (value) => {
  return { type: "SET_TIME_WORKED", inputValue: value };
};

const SET_TRIGGER_COLOR = (value) => {
  // console.log(value);
  return { type: "SET_TRIGGER_COLOR", inputValue: value };
};

export {
  SET_NUP,
  SET_NAME,
  SET_EMAIL,
  SET_JABATAN,
  SET_LATITUDE_RUMAH,
  SET_LONGITUDE_RUMAH,
  SET_PHOTO,
  SET_UNIT_KERJA,
  SET_CLOCK_IN,
  SET_CLOCK_OUT,
  SET_TIME_WORKED,
  SET_TRIGGER_COLOR,
};
